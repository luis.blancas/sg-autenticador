/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.journaldev.prime.faces.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author luisa
 */

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class BasculaController  implements Serializable
{
    List<Items> items;
    private Items editar;
    public List<Items> getItems() 
    {
        this.items=new ArrayList();
        this.items.add(new Items(1,"Luis","luis@teniente.com"));
        this.items.add(new Items(2,"Adrian","adrian@teniente.com"));
        this.items.add(new Items(3,"Pedro","pedro_987@teniente.com"));
        this.items.add(new Items(4,"Juan","juan_98765482ng@teniente.com"));
        return items;
    }

    public void setItems(List<Items> items) {
        this.items = items;
    }

    public Items getEditar() {
        return editar;
    }

    public void setEditar(Items editar) {
        this.editar = editar;
    }
    
    public String loadCustomer(Items i)
    {
        System.out.println(i.toString());
        return "pm:nuevo";
    }
    public String nuevo()
    {
        return "pm:editar";
    }
}
