/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.org.citicos.controller;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.List;
import org.primefaces.event.RowEditEvent;
import mx.org.citricos.entity.Car;
import mx.org.citicos.service.CarService;
/**
 *
 * @author luisa
 */
@ManagedBean(name="dtEditView")
@ViewScoped
public class AddRowView implements Serializable {
 
    private List<Car> cars1;
 
    @ManagedProperty("#{carService}")
    private CarService service;
 
    @PostConstruct
    public void init() {
        cars1 = service.createCars(30);
    }
 
    public List<Car> getCars1() {
        return cars1;
    }
 
    public List<String> getBrands() {
        return service.getBrands();
    }
 
    public List<String> getColors() {
        return service.getColors();
    }
 
    public void setService(CarService service) {
        this.service = service;
    }
 
    public void onRowEdit(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Edicion de Detalle", ((Car) event.getObject()).getId());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
 
    public void onRowCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Edición cancelado", ((Car) event.getObject()).getId());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
 
    public void onAddNew() {
        // Add one new car to the table:
        Car car2Add = service.createCars(1).get(0);
        cars1.add(car2Add);
        FacesMessage msg = new FacesMessage("Nuevo Detalle", car2Add.getId());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
 
 
}