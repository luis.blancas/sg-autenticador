/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.org.citicos.controller;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import mx.org.citricos.dao.Conexion;
import mx.org.citricos.entity.Corrida;
import mx.org.citricos.entity.CorridaFolio;
/**
 *
 * @author luisa
 */
public class CorridasController  extends Conexion 
{
    String querySelectCo ="  SELECT F.ID,F.STATUS, F.FOLIO,F.FECHA,F.ID_PRODUCTOR,F.PESO_BRUTO,F.PESO_TARA,F.PESO_NETO,\n" +
                        " F.NO_REJAS,F.ID_REJAS,F.ID_TLIMON,F.ID_AGRONOMO,F.DEJO,F.OBSERVACIONES, \n" +
                        " F.SEGUNDAS,F.TERCERAS,F.TORREON,F.COLEADA,F.JAPON, \n" +
                        " (SELECT A.DESCRIPCION FROM CC_AGRONOMO A WHERE F.ID_AGRONOMO  = A.ID)AGRONOMO,\n" +
                        " P.NOMBRE PRODUCTOR,\n" +
                        " (SELECT R.DESCRIPCION FROM CC_CAT_REJA R WHERE F.ID_REJAS     = R.ID)TIPO_REJAS,\n" +
                        " (SELECT L.DESCRIPCION FROM CC_CAT_TAMANO_LIMON L WHERE F.ID_TLIMON    = L.ID)TIPO_LIMON\n" +
                        " FROM CC_FOLIOS F, PRODUCTORES P  \n" +
                        " WHERE F.ID_PRODUCTOR = P.ID " +
                        " AND F.ID_ACTIVO =1 AND STATUS = ";
    String querySelect="SELECT ID,"
            + " VERDE_JAPON,VERDE_110,VERDE_150,VERDE_175,VERDE_200,VERDE_230,VERDE_250, " 
            + " EMPAQUE_110,EMPAQUE_150,EMPAQUE_175,EMPAQUE_200,EMPAQUE_230,EMPAQUE_250,	" 
            + " SEGUNDAS,TERCERAS,TORREON,COLEADA,"
            + " COMPRADOR,FACTURAR,TIPO, "
            + " IDCOMPRADOR,IDFACTURAR,IDTIPO   "
            + " FROM CC_CORRIDAS"
            + " WHERE ID_FOLIO = "; 
    String queryMax="SELECT MAX(ID) ID FROM CC_CORRIDAS"; 
    String queryCreate="INSERT INTO CC_CORRIDAS ( "
            + " VERDE_JAPON,VERDE_110,VERDE_150,VERDE_175,VERDE_200,VERDE_230,VERDE_250, " 
            + " EMPAQUE_110,EMPAQUE_150,EMPAQUE_175,EMPAQUE_200,EMPAQUE_230,EMPAQUE_250,	" 
            + " SEGUNDAS,TERCERAS,TORREON,COLEADA,"
            + "COMPRADOR,FACTURAR,TIPO, " 
            + "IDCOMPRADOR,IDFACTURAR,IDTIPO,ID_FOLIO)  VALUES( ?,"
            + "?,?,?,?,?,?,?,"
            + "?,?,?,?,?,?,"
            + "?,?,?,?,"
            + "?,?,?,?,?,?)";
    String queryUpdate="UPDATE CC_CORRIDAS SET"
            + " VERDE_JAPON= ?,VERDE_110= ?,VERDE_150= ?,VERDE_175= ?,VERDE_200= ?,VERDE_230= ?,VERDE_250= ?, " 
            + " EMPAQUE_110= ?,EMPAQUE_150= ?,EMPAQUE_175= ?,EMPAQUE_200= ?,EMPAQUE_230= ?,EMPAQUE_250= ?,	" 
            + " SEGUNDAS= ?,TERCERAS= ?,TORREON= ?,COLEADA= ?,"
            + "COMPRADOR= ?,FACTURAR= ?,TIPO= ?, " 
            + "IDCOMPRADOR= ?,IDFACTURAR= ?,IDTIPO= ? WHERE ID =  ? ";
    public Corrida getMax()
    {
        System.out.println("getMax()");
        Corrida bean=new Corrida();
        Connection connection=null;
        try 
        {
            connection=get_connection();
            Statement st=connection.createStatement();
            ResultSet rs=st.executeQuery(queryMax);
            if(rs.next())
            { 
                bean = getOne(rs.getInt("ID"));
            }
            rs.close();
        }
        catch (Exception e) 
        {
            System.out.println(e);
        }
        finally
        {
            if(connection!=null)
            {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TransportistaController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return bean;
    }
    public Corrida getOne(int i)
    {
        System.out.println("getOne("+i+")");
        Corrida bean=null;
        Connection connection=null;
        try 
        {
            connection=get_connection();
            Statement st=connection.createStatement();
            ResultSet rs=st.executeQuery(querySelect+i);
            if(rs.next())
            {
                bean=new Corrida();
                bean.setId(rs.getInt("ID"));
                bean.setFolio(i);
                
                bean.setVerde_japon(rs.getDouble("VERDE_JAPON"));
                bean.setVerde_110(rs.getDouble("VERDE_110"));
                bean.setVerde_150(rs.getDouble("VERDE_150"));
                bean.setVerde_175(rs.getDouble("VERDE_175"));
                bean.setVerde_200(rs.getDouble("VERDE_200"));
                bean.setVerde_230(rs.getDouble("VERDE_230"));
                bean.setVerde_250(rs.getDouble("VERDE_250")); 
        
        
                bean.setEmpaque_110(rs.getDouble("EMPAQUE_110"));
                bean.setEmpaque_150(rs.getDouble("EMPAQUE_150"));
                bean.setEmpaque_175(rs.getDouble("EMPAQUE_175"));
                bean.setEmpaque_200(rs.getDouble("EMPAQUE_200"));
                bean.setEmpaque_230(rs.getDouble("EMPAQUE_230"));
                bean.setEmpaque_250(rs.getDouble("EMPAQUE_250")); 
                
                bean.setSegundas(rs.getDouble("SEGUNDAS"));
                bean.setTerceras(rs.getDouble("TERCERAS"));
                bean.setTorreon(rs.getDouble("TORREON"));
                bean.setColeada(rs.getDouble("COLEADA"));
                
                bean.setComprador(rs.getString("COMPRADOR"));
                bean.setFacturar(rs.getString("FACTURAR"));
                bean.setTipo(rs.getString("TIPO"));
                bean.setIdcomprador(rs.getInt("IDCOMPRADOR"));
                bean.setIdfacturar(rs.getInt("IDFACTURAR"));
                bean.setIdtipo(rs.getInt("IDTIPO"));
            }
            rs.close();
        }
        catch (Exception e) 
        {
            System.out.println("Problemas "+e.getMessage());
        }
        finally
        {
            if(connection!=null)
            {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TransportistaController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        System.out.println("BEAN: "+bean.toString());
        return bean;
    }
    public Corrida insertaDatoVacio(int i)
    {
            return insertRecord(
                0d,0d,0d,0d,0d,0d,0d,
                0d,0d,0d,0d,0d,0d,
                0d,0d,0d,0d,
                "","","",0,0,0,i);
    }
    /**
     * Proceso para insertar una corrida
     * @param verde_japon
     * @param verde_110
     * @param verde_150
     * @param verde_175
     * @param verde_200
     * @param verde_230
     * @param verde_250
     * @param empaque_110
     * @param empaque_150
     * @param empaque_175
     * @param empaque_200
     * @param empaque_230
     * @param empaque_250
     * @param segundas
     * @param terceras
     * @param torreon
     * @param coleada
     * @param comprador
     * @param facturar
     * @param tipo
     * @param idcomprador
     * @param idfacturar
     * @param idtipo
     * @param id
     * @return una corrida
     */
    public Corrida  insertRecord(
            double verde_japon,
            double verde_110,
            double verde_150,
            double verde_175,
            double verde_200,
            double verde_230,
            double verde_250,
            double empaque_110,
            double empaque_150,
            double empaque_175,
            double empaque_200,
            double empaque_230,
            double empaque_250,	
            double segundas,
            double terceras ,
            double torreon,
            double coleada,
            String comprador,
            String facturar,
            String tipo, 
            int idcomprador,
            int idfacturar,
            int idtipo,
            int id
            )
    {
        System.out.println("insertRecord("+id+")");
        int insertCont=0;
        Corrida bean=null;
        Connection connection=null;
        try 
        { 
            connection=get_connection();
            PreparedStatement ps=connection.prepareStatement(queryCreate);
            ps.setDouble(1,  verde_japon);
            ps.setDouble(2,  verde_110);
            ps.setDouble(3,  verde_150);
            ps.setDouble(4,  verde_175);
            ps.setDouble(5,  verde_200);
            ps.setDouble(6,  verde_230);
            ps.setDouble(7,  verde_250);

            ps.setDouble(8,  empaque_110);
            ps.setDouble(9,  empaque_150);
            ps.setDouble(10, empaque_175);
            ps.setDouble(11, empaque_200);
            ps.setDouble(12, empaque_230);
            ps.setDouble(13, empaque_250);	

            ps.setDouble(14, segundas);
            ps.setDouble(15, terceras);
            ps.setDouble(16, torreon);
            ps.setDouble(17, coleada);
            
            ps.setString(18, comprador);
            ps.setString(19, facturar);
            ps.setString(20, tipo);
            ps.setInt   (21, idcomprador);
            ps.setInt   (22, idfacturar);
            ps.setInt   (23, idtipo);
            ps.setInt   (24, id);
            insertCont=ps.executeUpdate();
            if(insertCont==1)
            {
                bean  =  getMax();
            }
            ps.close();
        }
        catch (Exception e) 
        {
            System.out.println(e);
        }
        finally
        {
            if(connection!=null)
            {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TransportistaController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return bean;
    }
    
    public ArrayList<CorridaFolio> getCorridas(int id)
    {
        ArrayList<CorridaFolio> l=new ArrayList<>();
        Connection connection=null;
        try 
        {
            connection=get_connection();
            Statement st=connection.createStatement();
            try (ResultSet rs = st.executeQuery(querySelectCo+id+" order by F.FOLIO desc ")) {
                while(rs.next())
                {
                    CorridaFolio bean=new CorridaFolio();
                    bean.setId(rs.getInt("ID"));
                    bean.setFolio(rs.getString("FOLIO"));
                    bean.setEstatus(rs.getInt("STATUS"));
                    bean.setCodigo(getToken(bean.getFolio()));
                    bean.setFecha(rs.getString("FECHA"));
                    bean.setId_productor(rs.getInt("ID_PRODUCTOR"));
                    bean.setPeso_bruto(rs.getInt("PESO_BRUTO"));
                    bean.setPeso_tara(rs.getInt("PESO_TARA"));
                    bean.setPeso_neto(rs.getInt("PESO_NETO"));
                    bean.setNo_rejas(rs.getInt("NO_REJAS"));
                    bean.setTipo_rejas(rs.getInt("ID_REJAS"));
                    bean.setTipo_limon(rs.getInt("ID_TLIMON"));
                    bean.setId_agronomo(rs.getInt("ID_AGRONOMO"));
                    bean.setNo_rejas(rs.getInt("NO_REJAS"));
                    bean.setDejo(rs.getInt("DEJO"));
                    if(bean.getDejo() == 1)
                        bean.setIsdejo(true);
                    else
                        bean.setIsdejo(false);
                    bean.setObservaciones(rs.getString("OBSERVACIONES"));
                    bean.setSegundas(rs.getInt("SEGUNDAS"));
                    bean.setTerceras(rs.getInt("TERCERAS"));
                    bean.setTorreon(rs.getInt("TORREON"));
                    bean.setColeada(rs.getInt("COLEADA"));
                    bean.setJapon(rs.getInt("JAPON"));
                    bean.setProductor(rs.getString("PRODUCTOR"));
                    bean.setAgronomo(rs.getString("AGRONOMO"));
                    bean.setTipos_rejas(rs.getString("TIPO_REJAS"));
                    bean.setTipos_limones(rs.getString("TIPO_LIMON"));
                    CorridasController corridasCTR=new CorridasController();
                    Corrida corrida = corridasCTR.getOne(bean.getId());
                    bean.setIdcorrida(corrida.getId());
                    bean.setVerde_japon(corrida.getVerde_japon());
                    bean.setVerde_110(corrida.getVerde_110());
                    bean.setVerde_150(corrida.getVerde_150());
                    bean.setVerde_175(corrida.getVerde_175());
                    bean.setVerde_200(corrida.getVerde_200());
                    bean.setVerde_230(corrida.getVerde_230());
                    bean.setVerde_250(corrida.getVerde_250()); 
        
                    bean.setEmpaque_110(corrida.getEmpaque_110());
                    bean.setEmpaque_150(corrida.getEmpaque_150());
                    bean.setEmpaque_175(corrida.getEmpaque_175());
                    bean.setEmpaque_200(corrida.getEmpaque_200());
                    bean.setEmpaque_230(corrida.getEmpaque_230());
                    bean.setEmpaque_250(corrida.getEmpaque_250()); 

                    bean.setSig_segundas(corrida.getSegundas());
                    bean.setSig_terceras(corrida.getTerceras());
                    bean.setSig_torreon(corrida.getTorreon());
                    bean.setSig_coleada(corrida.getColeada());

                    bean.setComprador(corrida.getComprador());
                    bean.setFacturar(corrida.getFacturar());
                    bean.setTipo(corrida.getTipo());
                    bean.setIdcomprador(corrida.getIdcomprador());
                    bean.setIdfacturar(corrida.getIdfacturar());
                    bean.setIdtipo(corrida.getIdtipo());
                    bean.setIdcorrida(corrida.getId());
                    bean.setVerde_suma(getVerdesuma(corrida));
                    bean.setEmpaque_suma(getEmpaquesuma(corrida));
                    bean.setCorrida_suma(getCorridasuma(corrida));
                    l.add(bean);
                }
            }
        }
        catch (Exception e) 
        {
            System.out.println(e);
        }
        finally
        {
            if(connection!=null)
            {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TransportistaController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return l;
    }
    
    public Double getVerdesuma(Corrida corrida)
    {
        double verde_suma =
            corrida.getVerde_japon()+
            corrida.getVerde_110()+
            corrida.getVerde_150()+
            corrida.getVerde_175()+
            corrida.getVerde_200()+
            corrida.getVerde_230()+
            corrida.getVerde_250();
        return verde_suma;
    }
    
    public Double getEmpaquesuma(Corrida corrida)
    {
        double empaque_suma =
            corrida.getEmpaque_110()+
            corrida.getEmpaque_150()+
            corrida.getEmpaque_175()+
            corrida.getEmpaque_200()+
            corrida.getEmpaque_230()+
            corrida.getEmpaque_250();
        return empaque_suma;
    }
    public Double getCorridasuma(Corrida corrida)
    {
        double corrida_suma =
            corrida.getSegundas()+
            corrida.getTerceras()+
            corrida.getTorreon()+
            corrida.getColeada();
        return corrida_suma;
    } 
    /**
     * Proceso para modificar una corrida
     * @param verde_japon
     * @param verde_110
     * @param verde_150
     * @param verde_175
     * @param verde_200
     * @param verde_230
     * @param verde_250
     * @param empaque_110
     * @param empaque_150
     * @param empaque_175
     * @param empaque_200
     * @param empaque_230
     * @param empaque_250
     * @param segundas
     * @param terceras
     * @param torreon
     * @param coleada
     * @param comprador
     * @param facturar
     * @param tipo
     * @param idcomprador
     * @param idfacturar
     * @param idtipo
     * @param id
     */
    public void UpdateRecord(
            double verde_japon,
            double verde_110,
            double verde_150,
            double verde_175,
            double verde_200,
            double verde_230,
            double verde_250,
            double empaque_110,
            double empaque_150,
            double empaque_175,
            double empaque_200,
            double empaque_230,
            double empaque_250,	
            double segundas,
            double terceras ,
            double torreon,
            double coleada,
            String comprador,
            String facturar,
            String tipo, 
            int idcomprador,
            int idfacturar,
            int idtipo,
            int id
            )
    {
        System.out.println("UpdateRecord()");
        Connection connection=null;
        try 
        { 
            connection=get_connection();
            PreparedStatement ps=connection.prepareStatement(queryUpdate);
            ps.setDouble(1,  verde_japon);
            ps.setDouble(2,  verde_110);
            ps.setDouble(3,  verde_150);
            ps.setDouble(4,  verde_175);
            ps.setDouble(5,  verde_200);
            ps.setDouble(6,  verde_230);
            ps.setDouble(7,  verde_250);

            ps.setDouble(8,  empaque_110);
            ps.setDouble(9,  empaque_150);
            ps.setDouble(10, empaque_175);
            ps.setDouble(11, empaque_200);
            ps.setDouble(12, empaque_230);
            ps.setDouble(13, empaque_250);	

            ps.setDouble(14, segundas);
            ps.setDouble(15, terceras);
            ps.setDouble(16, torreon);
            ps.setDouble(17, coleada);
            
            ps.setString(18, comprador);
            ps.setString(19, facturar);
            ps.setString(20, tipo);
            ps.setInt   (21, idcomprador);
            ps.setInt   (22, idfacturar);
            ps.setInt   (23, idtipo);
            ps.setInt   (24, id);
            int updet=ps.executeUpdate();
            ps.close();
            
            String queryUpdate="UPDATE CC_CORRIDAS SET"
            + " VERDE_JAPON= ?,VERDE_110= ?,VERDE_150= ?,VERDE_175= ?,VERDE_200= ?,VERDE_230= ?,VERDE_250= ?, " 
            + " EMPAQUE_110= ?,EMPAQUE_150= ?,EMPAQUE_175= ?,EMPAQUE_200= ?,EMPAQUE_230= ?,EMPAQUE_250= ?,	" 
            + " SEGUNDAS= ?,TERCERAS= ?,TORREON= ?,COLEADA= ?,"
            + "COMPRADOR= ?,FACTURAR= ?,TIPO= ?, " 
            + "IDCOMPRADOR= ?,IDFACTURAR= ?,IDTIPO= ? WHERE ID =  ? ";
        }
        catch (Exception e) 
        {
            System.out.println(e);
        }
        finally
        {
            if(connection!=null)
            {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TransportistaController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    /**
     * Proceso para modificar una corrida
     * @param estatus
     * @param id
     */
    public void UpdateRecord(int id, int estatus)
    {
        System.out.println("UpdateRecord a Estatus <<<< "+ estatus +">>>>( "  +id+ ")");
        Connection connection=null;
        try 
        { 
            connection=get_connection();
            String queryUpdate="UPDATE CC_FOLIOS SET  STATUS= ?  WHERE ID =  (select  id_folio  from cc_corridas where id = ?) ";
            PreparedStatement ps=connection.prepareStatement(queryUpdate);
            ps.setInt   (1, estatus);
            ps.setInt   (2, id);
            int updet=ps.executeUpdate();
            ps.close();
            
        }
        catch (Exception e) 
        {
            System.out.println(e);
        }
        finally
        {
            if(connection!=null)
            {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TransportistaController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    private String getToken(String cadena)
    {
      cadena =  cadena.substring(3,15);
      String pww="";
      if(cadena.length()%2 == 0)
      {
          for(int i=0;i<(cadena.length()/2);i++)
          {
              String tok=cadena.substring((2*i),(2*(i+1)));
              pww= pww + (sexty(tok));
          }
      }
      return pww;
    }
    private String  sexty(String to)
    {
      int cod=(new Integer(to));
      switch(cod)
      {
          case 0: return "0";
          case 1: return "1";
          case 2: return "2";
          case 3: return "3";
          case 4: return "4";
          case 5: return "5";
          case 6: return "6";
          case 7: return "7";
          case 8: return "8";
          case 9: return "9";
          
          case 10: return "A";
          case 11: return "B";
          case 12: return "C";
          case 13: return "D";
          case 14: return "E";
          case 15: return "F";
          case 16: return "G";
          case 17: return "H";
          case 18: return "I";
          case 19: return "J";
          
          case 20: return "K";
          case 21: return "L";
          case 22: return "M";
          case 23: return "N";
          case 24: return "O";
          case 25: return "P";
          case 26: return "Q";
          case 27: return "R";
          case 28: return "S";
          case 29: return "T";
          
          case 30: return "U";
          case 31: return "V";
          case 32: return "W";
          case 33: return "X";
          case 34: return "Y";
          case 35: return "Z";
          case 36: return "*";
          case 37: return "#";
          case 38: return "$";
          case 39: return "%";
          
          
          case 40: return "&";
          case 41: return "/";
          case 42: return "(";
          case 43: return ")";
          case 44: return "=";
          case 45: return "!";
          case 46: return "+";
          case 47: return "{";
          case 48: return "}";
          case 49: return "[";
          
          
          case 50: return "]";
          case 51: return "-";
          case 52: return ".";
          case 53: return ",";
          case 54: return ";";
          case 55: return ":";
          case 56: return "<";
          case 57: return ">";
          case 58: return "@";
          case 59: return "?";
      }
      return " ";
    }
}
