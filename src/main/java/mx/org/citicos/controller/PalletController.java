/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.org.citicos.controller;
import mx.org.citricos.dao.Conexion;
import mx.org.citricos.entity.Pallet;
import java.util.ArrayList;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import mx.org.citricos.entity.Marca;
import mx.org.citricos.entity.Tarima;
import mx.org.citricos.entity.Transporte;
/**
 *
 * @author BID
 */
public class PalletController extends Conexion
{
    TarimaController     tarimaC     = new TarimaController();
    MarcaController      marcaC      = new MarcaController();
    TransporteController transporteC = new TransporteController();
    private final String querySelect="SELECT  ID,QR,NUMERO,CAJAS,ID_ACTIVO,ID_CAT_TARIMA,ID_CAT_MARCA,ID_TRANSPORTE FROM CC_TARIMA  WHERE ID_ACTIVO =1  ORDER BY ID DESC";
    private final String querySelectTR="SELECT  ID,QR,NUMERO,CAJAS,ID_ACTIVO,ID_CAT_TARIMA,ID_CAT_MARCA,ID_TRANSPORTE FROM CC_TARIMA";
    private final String queryMax="SELECT MAX(ID) ID FROM CC_TARIMA";
    //private final String queryPalletDesc="SELECT MAX(ID) ID FROM CC_TARIMA";
    private final String queryPalletDescFinal="SELECT ID_TARIMA FROM CC_TARIMA_DESC WHERE ID =";
    private final String querySelectOne=" SELECT  C.ID,C.QR,C.NUMERO,C.CAJAS,C.ID_ACTIVO,C.ID_CAT_TARIMA,C.ID_CAT_MARCA,C.ID_TRANSPORTE,M.DESCRIPCION MARCAX, T.DESCRIPCION TARIMAX "
            +                           " FROM CC_TARIMA C , CC_CAT_MARCA M, CC_CAT_TARIMA T "
            +                           " WHERE C.ID_ACTIVO =1 "
            +                           " AND C.ID_CAT_TARIMA =T.ID "
            +                           " AND  C.ID_CAT_MARCA =  M.ID"
            +                           " AND   C.ID =  ";
    private final String queryUpdate="UPDATE CC_TARIMA SET CAJAS=?,ID_CAT_TARIMA=?,ID_CAT_MARCA=?, MODIFICADOPOR=?, MODIFICADO=NOW() WHERE ID = ? ";
    private final String queryUpdateTransporte="UPDATE CC_TARIMA SET  ID_TRANSPORTE=?,MODIFICADOPOR=? , MODIFICADO=NOW() WHERE ID = ? ";
    //private final String queryUpdateQR="UPDATE CC_TARIMA SET QR =? , MODIFICADO=NOW() WHERE ID = ? ";
    private final String queryUpdateNumber="UPDATE CC_TARIMA SET  NUMERO = CONCAT(CONCAT('PLL',DATE_FORMAT(NOW(),'%Y%m%d')),LPAD(ID,9,'0')) , MODIFICADO=NOW() WHERE ID = ? ";
    private final String queryDeleteUser="UPDATE CC_TARIMA SET ID_ACTIVO = 3 , MODIFICADOPOR =?, MODIFICADO=CURDATE() WHERE ID = ?  ";
    private final String queryCreateUser="INSERT INTO CC_TARIMA  (CAJAS,ID_CAT_TARIMA,ID_CAT_MARCA,CREADOPOR,MODIFICADOPOR,CREADO,MODIFICADO) VALUES(?,?,?,?,?,NOW(),NOW())";
    private final String queryUpdateRecordBox="UPDATE CC_TARIMA  SET CAJAS = ?   WHERE ID = ?  ";
    
    public static void main(String [] args)
    {
        int reg=22;
        PalletController ctr=new PalletController();
        ArrayList< Pallet> l=ctr.getAll();
        Iterator it=l.iterator();
        while(it.hasNext())
        {
            Pallet  bean=(Pallet)it.next(); 
            System.out.println(">"+bean.toString());
            
        }
        //public int updateRecord(int cajas,int tarima,int marca,int transporte,int id)
        int modificado=ctr.updateRecord(20,3,3,2,reg);
        System.out.println("Se modifico >"+modificado+" registro");
        Pallet modificado5=ctr.getOne(reg);        
        System.out.println(">"+modificado5.toString());
        int trans=ctr.updateRecordTransporte(22,3,reg);
        System.out.println(">"+modificado5.toString());
        int borrado=ctr.deleteRecord(reg,2);
        System.out.println("Se borro  >"+modificado+" registro ["+borrado+"]");
        Pallet insertReg= ctr.insertRecord(51,4,3,10,10);
        System.out.println("Registro nuevo >"+insertReg.toString());
    }
    
    /**
     * Proceso para el borrado de registro.
     * @param id
     * @param cajas
     * @return numero de registros dados de alta.
     */
    public int updateRecordBox(int id,int cajas)
    {
        System.out.println("updateRecordBox("+id+","+cajas+")");
        int deleteCont=0;
        Connection connection=null;
        try 
        {
            connection=get_connection();
            PreparedStatement ps=connection.prepareStatement(queryUpdateRecordBox);
            ps.setInt(1, cajas);
            ps.setInt(2, id); 
            deleteCont=ps.executeUpdate();
            ps.close();
        }
        catch (Exception e)
        {
            System.out.println(e);
        }
        finally
        {
            if(connection!=null)
            {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TransportistaController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return deleteCont;   
    }
    private Pallet getMax()
    {
        System.out.println("getMax()");
        Pallet bean=new Pallet();
        Connection connection=null;
        try 
        {
            connection=get_connection();
            Statement st=connection.createStatement();
            ResultSet rs=st.executeQuery(queryMax);
            if(rs.next())
            { 
                bean = getOne(rs.getInt("ID"));
            }
            rs.close();
        }
        catch (Exception e) 
        {
            System.out.println(e);
        }
        finally
        {
            if(connection!=null)
            {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TransportistaController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return bean;
    }
    public int getIdPallet(int idPalletDesc)
    {
        System.out.println("getIdPallet("+ idPalletDesc+")");
        int idPallet=0;
        Connection connection=null;
        try 
        {
            connection=get_connection();
            Statement st=connection.createStatement();
            ResultSet rs=st.executeQuery(queryPalletDescFinal + idPalletDesc);
            if(rs.next())
            { 
                idPallet = rs.getInt("ID");
            }
            rs.close();
        }
        catch (Exception e) 
        {
            System.out.println(e);
        }
        finally
        {
            if(connection!=null)
            {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TransportistaController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return idPallet;
    }
    /**
     * Proceso para la consulta de un cliente
     * @param id
     * @return 
     */
    public Pallet getOne(int id)
    {
        System.out.println("getOne("+id+")");
        Pallet bean=new Pallet();
        Connection connection=null;
        try 
        {
            connection=get_connection();
            Statement st=connection.createStatement();
            ResultSet rs=st.executeQuery(querySelectOne+id);
            if(rs.next())
            { 
                bean.setId(rs.getInt("ID"));
                bean.setQr(rs.getString("QR"));
                bean.setCajas(rs.getInt("CAJAS"));
                bean.setIdactivo(rs.getInt("ID_ACTIVO"));
                bean.setIdtarima(rs.getInt("ID_CAT_TARIMA"));
                bean.setIdmarca(rs.getInt("ID_CAT_MARCA"));
                bean.setIdtransporte(rs.getInt("ID_TRANSPORTE"));
                bean.setNumero(rs.getString("NUMERO"));
                bean.setMarca(rs.getString("MARCAX"));
                bean.setTarima(rs.getString("TARIMAX"));
            }
            rs.close();
        }
        catch (Exception e) 
        {
            System.out.println(e);
        }
        finally
        {
            if(connection!=null)
            {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TransportistaController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return bean;
    }
    public ArrayList<Pallet> getAll()
    {
        System.out.println("getAll()");
        ArrayList<Pallet> l=new ArrayList<Pallet>();
        Connection connection=null;
        try 
        {
            connection=get_connection();
            Statement st=connection.createStatement();
            ResultSet rs=st.executeQuery(querySelect);
            while(rs.next())
            {
                Pallet bean=new Pallet();
                bean.setId(rs.getInt("ID"));
                bean.setQr(rs.getString("QR"));
                bean.setCajas(rs.getInt("CAJAS"));
                bean.setIdactivo(rs.getInt("ID_ACTIVO"));
                bean.setIdtarima(rs.getInt("ID_CAT_TARIMA"));
                bean.setIdmarca(rs.getInt("ID_CAT_MARCA"));
                bean.setIdtransporte(rs.getInt("ID_TRANSPORTE"));
                bean.setNumero(rs.getString("NUMERO"));
                bean.setTarima(((Tarima)tarimaC.getOne(bean.getIdtarima())).getNombre());
                bean.setMarca(((Marca)marcaC.getOne(bean.getIdmarca())).getNombre());
                bean.setTransporte(((Transporte)transporteC.getOne(bean.getIdtransporte())).getNumero());
                PalletDescController detController=new PalletDescController(rs.getInt("ID"));
                bean.setDetalle(detController.getAll());
        
                l.add(bean);
            }
            rs.close();
        }
        catch (Exception e) 
        {
            System.out.println(e);
        }
        finally
        {
            if(connection!=null)
            {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TransportistaController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return l;
    }
    public ArrayList<Pallet> getAll(HashMap m)
    {
        System.out.println("getAll()");
        ArrayList<Pallet> l=new ArrayList<Pallet>();
        Connection connection=null;
        try 
        {
            connection=get_connection();
            Statement st=connection.createStatement();
            ResultSet rs=st.executeQuery(querySelectTR);
            while(rs.next())
            {
                Pallet bean=new Pallet();
                bean.setId(rs.getInt("ID"));
                bean.setQr(rs.getString("QR"));
                bean.setCajas(rs.getInt("CAJAS"));
                bean.setIdactivo(rs.getInt("ID_ACTIVO"));
                bean.setIdtarima(rs.getInt("ID_CAT_TARIMA"));
                bean.setIdmarca(rs.getInt("ID_CAT_MARCA"));
                bean.setIdtransporte(rs.getInt("ID_TRANSPORTE"));
                bean.setNumero(rs.getString("NUMERO"));
                bean.setActivos((String) m.get((new Integer(rs.getInt("ID_ACTIVO")))));
                bean.setTarima(((Tarima)tarimaC.getOne(bean.getIdtarima())).getNombre());
                bean.setMarca(((Marca)marcaC.getOne(bean.getIdmarca())).getNombre());
                bean.setTransporte(((Transporte)transporteC.getOne(bean.getIdtransporte())).getNumero());
                l.add(bean);
            }
            rs.close();
        }
        catch (Exception e) 
        {
            System.out.println(e);
        }
        finally
        {
            if(connection!=null)
            {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TransportistaController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return l;
    }
    /**
     * Proceso para la modificación de registro.  
     * @param transporte
     * @param usuario
     * @param id
     * @return numero de registros dados de alta.
     */
    public int updateRecordTransporte(int transporte,int usuario,int id)
     {   
        System.out.println("updateRecord("+transporte+","+usuario+","+id+")");
        int updeteCont=0;
        Connection connection=null;
        try 
        { 
            connection=get_connection();
            PreparedStatement ps=connection.prepareStatement(queryUpdateTransporte);
            ps.setInt(1, transporte);
            ps.setInt(2, usuario);
            ps.setInt(3, id); 
            updeteCont = ps.executeUpdate();
            ps.close();
        }
        catch (Exception e)
        {
            System.out.println(e);
        }
        finally
        {
            if(connection!=null)
            {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TransportistaController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return updeteCont;
    }
    /**
     * Proceso para la modificación de registro. 
     * @param cajas
     * @param tarima
     * @param marca
     * @param usuariom
     * @param id
     * @return numero de registros dados de alta.
     */
    public int updateRecord(int cajas,int tarima,int marca,int usuariom,int id)
     {   
        System.out.println("updateRecord("+cajas+","+tarima+","+marca+","+id+")");
        int updeteCont=0;
        Connection connection=null;
        try 
        { 
            //UPDATE CC_TARIMA SET CAJAS=?,ID_CAT_TARIMA=?,ID_CAT_MARCA=?, MODIFICADOPOR=? WHERE ID = ?
            connection=get_connection();
            PreparedStatement ps=connection.prepareStatement(queryUpdate);
            ps.setInt(1, cajas);
            ps.setInt(2, tarima);
            ps.setInt(3, marca); 
            ps.setInt(4, usuariom);            
            ps.setInt(5, id);
            updeteCont = ps.executeUpdate();
            ps.close();
        }
        catch (Exception e)
        {
            System.out.println(e);
        }
        finally
        {
            if(connection!=null)
            {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TransportistaController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return updeteCont;
    }
    /**
     * Proceso para la modificación de registro. 
     * @param id
     * @return numero de registros dados de alta.
     */
    private  int updateRecord(int id)
     {   
        System.out.println("updateRecord("+ id+")");
        int updeteCont=0;
        Connection connection=null;
        try 
        { 
            connection=get_connection();
            PreparedStatement ps=connection.prepareStatement(queryUpdateNumber); 
            ps.setInt(1, id);
            updeteCont = ps.executeUpdate();
            ps.close();
        }
        catch (Exception e)
        {
            System.out.println(e);
        }
        finally
        {
            if(connection!=null)
            {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TransportistaController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return updeteCont;
    }
    /**
     * Proceso para el borrado de registro.
     * @param id
     * @param id_user
     * @return numero de registros dados de alta.
     */
    public int deleteRecord(int id,int id_user)
    {
        System.out.println("deleteRecord("+id+")");
        int deleteCont=0;
        Connection connection=null;
        try 
        {
            connection=get_connection();
            PreparedStatement ps=connection.prepareStatement(queryDeleteUser);
            ps.setInt(1, id_user);
            ps.setInt(2, id); 
            deleteCont=ps.executeUpdate();
            ps.close();
        }
        catch (Exception e)
        {
            System.out.println(e);
        }
        finally
        {
            if(connection!=null)
            {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TransportistaController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return deleteCont;   
    }
    /**
     * Proceso para el guardado de registro.
     * @param cajas
     * @param tarima
     * @param marca
     * @return numero de registros dados de alta.
     * CAJAS,ID_CAT_TARIMA,ID_CAT_MARCA
     */
    public Pallet  insertRecord(int cajas,int tarima,int marca,int idUsuarioi, int idUsuariom)
    {
        //CAJAS,ID_CAT_TARIMA,ID_CAT_MARCA,CREADOPOR,MODIFICADOPOR
        System.out.println("insertRecord("+cajas+","+tarima+","+marca+","+idUsuarioi+","+idUsuariom+")");
        int insertCont=0;
        Pallet bean=null;
        Connection connection=null;
        try 
        {
            connection=get_connection();
            PreparedStatement ps=connection.prepareStatement(queryCreateUser);
            ps.setInt(1, cajas);
            ps.setInt(2, tarima);
            ps.setInt(3, marca);
            ps.setInt(4, idUsuarioi);
            ps.setInt(5, idUsuariom); 
            insertCont=ps.executeUpdate();
            if(insertCont==1)
            {
                bean  =  getMax();
            }
            ps.close();
            updateRecord(bean.getId());
        }
        catch (Exception e) 
        {
            System.out.println(e);
        }
        finally
        {
            if(connection!=null)
            {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TransportistaController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return bean;
    }
}
