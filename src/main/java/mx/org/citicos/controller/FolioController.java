/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.org.citicos.controller;
import mx.org.citricos.dao.Conexion;
import mx.org.citricos.entity.Folios; 
import java.util.ArrayList;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger ;
import mx.org.citricos.entity.Corrida;
import mx.org.citricos.entity.CorridaFolio;
/**
 *
 * @author Luis Adrian Blancas Bahena
 */
 
public class FolioController  extends Conexion 
{
    String fecha="  select DATE_FORMAT(NOW(),'%a %d de %M del %Y %h:%m.%s') FECHA ";
    String querySelect ="  SELECT F.ID,F.FOLIO,F.FECHA,F.ID_PRODUCTOR,F.PESO_BRUTO,F.PESO_TARA,F.PESO_NETO,\n" +
                        " F.NO_REJAS,F.ID_REJAS,F.ID_TLIMON,F.ID_AGRONOMO,F.DEJO,F.OBSERVACIONES, \n" +
                        " F.SEGUNDAS,F.TERCERAS,F.TORREON,F.COLEADA,F.JAPON, \n" +
                        " (SELECT A.DESCRIPCION FROM CC_AGRONOMO A WHERE F.ID_AGRONOMO  = A.ID)AGRONOMO,\n" +
                        " P.NOMBRE PRODUCTOR,\n" +
                        " (SELECT R.DESCRIPCION FROM CC_CAT_REJA R WHERE F.ID_REJAS     = R.ID)TIPO_REJAS,\n" +
                        " (SELECT L.DESCRIPCION FROM CC_CAT_TAMANO_LIMON L WHERE F.ID_TLIMON    = L.ID)TIPO_LIMON\n" +
                        " FROM CC_FOLIOS F, PRODUCTORES P  \n" +
                        " WHERE F.ID_PRODUCTOR = P.ID " +
                        " AND F.ID_ACTIVO =1 AND STATUS =";
    String queryMax="SELECT MAX(ID) ID FROM CC_FOLIOS";
    String queryUpdateNumber="UPDATE CC_FOLIOS SET  FOLIO = CONCAT(CONCAT('FOL',DATE_FORMAT(NOW(),'%y%m%d%h%m%s')),LPAD(ID,5,'0')) WHERE ID = ? ";
    String querySelectOne="SELECT F.STATUS, F.ID,F.FOLIO,F.FECHA,F.ID_PRODUCTOR,F.PESO_BRUTO,F.PESO_TARA,F.PESO_NETO,\n" +
                        "  F.NO_REJAS,F.ID_REJAS,F.ID_TLIMON,F.ID_AGRONOMO,F.DEJO,F.OBSERVACIONES, \n" +
                        "  F.SEGUNDAS,F.TERCERAS,F.TORREON,F.COLEADA,F.JAPON, \n" +
                        "  (SELECT A.DESCRIPCION FROM CC_AGRONOMO A WHERE F.ID_AGRONOMO  = A.ID)AGRONOMO,\n" +
                        "  P.NOMBRE PRODUCTOR,\n" +
                        "  (SELECT R.DESCRIPCION FROM CC_CAT_REJA R WHERE F.ID_REJAS     = R.ID)TIPO_REJAS,\n" +
                        "  (SELECT L.DESCRIPCION FROM CC_CAT_TAMANO_LIMON L WHERE F.ID_TLIMON    = L.ID)TIPO_LIMON\n" +
                        "  FROM CC_FOLIOS F, PRODUCTORES P  \n" +
                        "  WHERE F.ID_PRODUCTOR = P.ID " +
                        "  and   F.ID          = " ;
    String queryUpdate1="UPDATE CC_FOLIOS SET STATUS=?, ID_PRODUCTOR=?,PESO_BRUTO=?,PESO_TARA=?,PESO_NETO=? "
            + "WHERE ID = ?";
    String queryUpdateMvl1="UPDATE CC_FOLIOS SET STATUS=?, ID_PRODUCTOR=?,PESO_BRUTO=?,PESO_TARA=?,PESO_NETO=?, MODIFICADOPOR=? "
            + "WHERE ID = ?";
    String queryUpdate2="UPDATE CC_FOLIOS SET STATUS=?, NO_REJAS=?,ID_REJAS=?,ID_TLIMON=?,ID_AGRONOMO=?,DEJO=?,OBSERVACIONES=? "
            + "WHERE ID = ?";
    String queryUpdateMvl2="UPDATE CC_FOLIOS SET STATUS=?, NO_REJAS=?,ID_REJAS=?,ID_TLIMON=?,ID_AGRONOMO=?,DEJO=?,OBSERVACIONES=?,MODIFICADOPOR=? "
            + "WHERE ID = ?";
    String queryUpdate2_1="UPDATE CC_FOLIOS SET STATUS=?, NO_REJAS=?,ID_REJAS=?,ID_TLIMON=?,DEJO=?,OBSERVACIONES=? "
            + "WHERE ID = ?";
    String queryUpdate3="UPDATE CC_FOLIOS SET SEGUNDAS=?,TERCERAS=?,TORREON=?,COLEADA=?,JAPON=? "
            + "WHERE ID = ?";
    String queryUpdate4="UPDATE CC_FOLIOS SET "
            + "PESO_BRUTO=?,PESO_TARA=?,PESO_NETO=?,"
            + "JAPON=?,SEGUNDAS=?,TERCERAS=?,TORREON=?,COLEADA=?,"
            + "STATUS=? WHERE ID = ?";
    String queryUpdateMov4="UPDATE CC_FOLIOS SET "
            + "PESO_BRUTO=?,PESO_TARA=?,PESO_NETO=?,"
            + "JAPON=?,SEGUNDAS=?,TERCERAS=?,TORREON=?,COLEADA=?,"
            + "STATUS=?,MODIFICADOPOR=? WHERE ID = ?";
    String queryrechazarActivo = "UPDATE CC_FOLIOS SET STATUS=0  WHERE ID = ?";
    String queryrechazar = "UPDATE CC_FOLIOS SET STATUS=?  WHERE ID = ?";
    String queryDelete="DELETE FROM CC_FOLIOS WHERE ID = ? ";
    String queryCreate="INSERT INTO CC_FOLIOS (ID_PRODUCTOR,PESO_BRUTO,PESO_TARA,PESO_NETO)VALUES (?,?,?,?)";
    String queryCreateMvl="INSERT INTO CC_FOLIOS (ID_PRODUCTOR,PESO_BRUTO,PESO_TARA,PESO_NETO,CREADOPOR,MODIFICADOPOR)VALUES (?,?,?,?,?,?)";
    
    public void updateRecord(String fecha,int opc,int id)
    {  
        String update_basc ="UPDATE CC_FOLIOS SET fecha_basc=? WHERE ID = ?";
        String update_rece ="UPDATE CC_FOLIOS SET fecha_rece=? WHERE ID = ?";
        String update_conf ="UPDATE CC_FOLIOS SET fecha_conf=? WHERE ID = ?";
        String update ="";
        Connection connection=null;
        switch(opc)
        {
            case 1:  update  = update_basc;break;
            case 2:  update  = update_rece;break;
            case 3:  update  = update_conf;break;
            default: update  = update_basc;break;
        }
        try 
        { 
            connection=get_connection();
            PreparedStatement ps=connection.prepareStatement(update);
            ps.setString(1, fecha);
            ps.setInt   (2, id);
            ps.executeUpdate();
            ps.close();
        }
        catch (Exception e)
        {
            System.out.println(e);
        }
        finally
        {
            if(connection!=null)
            {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TransportistaController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public String getFecha()
    {
        String fechaCadena="";
        Connection connection=null;
        try 
        {
            connection=get_connection();
            Statement st=connection.createStatement();
            ResultSet rs=st.executeQuery(fecha);
            if(rs.next())
            {
                fechaCadena = rs.getString("FECHA");
            }
            rs.close();
        }
        catch (Exception e) 
        {
            System.out.println(e);
        }
        finally
        {
            if(connection!=null)
            {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TransportistaController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return fechaCadena;
    }
    public Folios getMax()
    {
        //System.out.println("getMax()");
        Folios bean=new Folios();
        Connection connection=null;
        try 
        {
            connection=get_connection();
            Statement st=connection.createStatement();
            ResultSet rs=st.executeQuery(queryMax);
            if(rs.next())
            { 
                bean = getOne(rs.getInt("ID"));
            }
            rs.close();
        }
        catch (Exception e) 
        {
            System.out.println(e);
        }
        finally
        {
            if(connection!=null)
            {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TransportistaController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return bean;
    }
    
    public Folios getOne(int i)
    {
        //System.out.println("getOne("+i+")");
        Folios bean=new Folios();
        Connection connection=null;
        try 
        {
            connection=get_connection();
            Statement st=connection.createStatement();
            ResultSet rs=st.executeQuery(querySelectOne+i);
            if(rs.next())
            {
                bean.setId(rs.getInt("ID"));
                bean.setFolio(rs.getString("FOLIO"));
                bean.setEstatus(rs.getInt("STATUS"));
                if(bean.getFolio()!=null)
                    if(bean.getFolio().length()>19)
                        bean.setCodigo(getToken(bean.getFolio()));
                bean.setFecha(rs.getString("FECHA"));
                bean.setId_productor(rs.getInt("ID_PRODUCTOR"));
                bean.setPeso_bruto(rs.getInt("PESO_BRUTO"));
                bean.setPeso_tara(rs.getInt("PESO_TARA"));
                bean.setPeso_neto(rs.getInt("PESO_NETO"));
                bean.setNo_rejas(rs.getInt("NO_REJAS"));
                bean.setTipo_rejas(rs.getInt("ID_REJAS"));
                bean.setTipo_limon(rs.getInt("ID_TLIMON"));
                bean.setId_agronomo(rs.getInt("ID_AGRONOMO"));
                bean.setNo_rejas(rs.getInt("NO_REJAS"));
                bean.setDejo(rs.getInt("DEJO"));
                bean.setObservaciones(rs.getString("OBSERVACIONES"));
                bean.setSegundas(rs.getInt("SEGUNDAS"));
                bean.setTerceras(rs.getInt("TERCERAS"));
                bean.setTorreon(rs.getInt("TORREON"));
                bean.setColeada(rs.getInt("COLEADA"));
                bean.setJapon(rs.getInt("JAPON"));
                bean.setProductor(rs.getString("PRODUCTOR"));
                bean.setAgronomo(rs.getString("AGRONOMO"));
                bean.setTipos_rejas(rs.getString("TIPO_REJAS"));
                bean.setTipos_limones(rs.getString("TIPO_LIMON"));
            }
            rs.close();
        }
        catch (Exception e) 
        {
            System.out.println(e);
        }
        finally
        {
            if(connection!=null)
            {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TransportistaController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return bean;
    }
    public ArrayList<CorridaFolio> getCorridas()
    {
        ArrayList<CorridaFolio> l=new ArrayList<>();
        Connection connection=null;
        try 
        {
            connection=get_connection();
            Statement st=connection.createStatement();
            try (ResultSet rs = st.executeQuery(querySelect+3+" order by F.FOLIO desc ")) {
                while(rs.next())
                {
                    CorridaFolio bean=new CorridaFolio();
                    bean.setId(rs.getInt("ID"));
                    bean.setFolio(rs.getString("FOLIO"));
                    bean.setCodigo(getToken(bean.getFolio()));
                    bean.setFecha(rs.getString("FECHA"));
                    bean.setId_productor(rs.getInt("ID_PRODUCTOR"));
                    bean.setPeso_bruto(rs.getInt("PESO_BRUTO"));
                    bean.setPeso_tara(rs.getInt("PESO_TARA"));
                    bean.setPeso_neto(rs.getInt("PESO_NETO"));
                    bean.setNo_rejas(rs.getInt("NO_REJAS"));
                    bean.setTipo_rejas(rs.getInt("ID_REJAS"));
                    bean.setTipo_limon(rs.getInt("ID_TLIMON"));
                    bean.setId_agronomo(rs.getInt("ID_AGRONOMO"));
                    bean.setNo_rejas(rs.getInt("NO_REJAS"));
                    bean.setDejo(rs.getInt("DEJO"));
                    if(bean.getDejo() == 1)
                        bean.setIsdejo(true);
                    else
                        bean.setIsdejo(false);
                    bean.setObservaciones(rs.getString("OBSERVACIONES"));
                    bean.setSegundas(rs.getInt("SEGUNDAS"));
                    bean.setTerceras(rs.getInt("TERCERAS"));
                    bean.setTorreon(rs.getInt("TORREON"));
                    bean.setColeada(rs.getInt("COLEADA"));
                    bean.setJapon(rs.getInt("JAPON"));
                    bean.setProductor(rs.getString("PRODUCTOR"));
                    bean.setAgronomo(rs.getString("AGRONOMO"));
                    bean.setTipos_rejas(rs.getString("TIPO_REJAS"));
                    bean.setTipos_limones(rs.getString("TIPO_LIMON"));
                    CorridasController corridasCTR=new CorridasController();
                    Corrida corrida = corridasCTR.getOne(bean.getId());
                    
                    bean.setId(corrida.getId());
                
                    bean.setVerde_japon(corrida.getVerde_japon());
                    bean.setVerde_110(corrida.getVerde_110());
                    bean.setVerde_150(corrida.getVerde_150());
                    bean.setVerde_175(corrida.getVerde_175());
                    bean.setVerde_200(corrida.getVerde_200());
                    bean.setVerde_230(corrida.getVerde_230());
                    bean.setVerde_250(corrida.getVerde_250()); 
        
                    bean.setEmpaque_110(corrida.getEmpaque_110());
                    bean.setEmpaque_150(corrida.getEmpaque_150());
                    bean.setEmpaque_175(corrida.getEmpaque_175());
                    bean.setEmpaque_200(corrida.getEmpaque_200());
                    bean.setEmpaque_230(corrida.getEmpaque_230());
                    bean.setEmpaque_250(corrida.getEmpaque_250()); 

                    bean.setSig_segundas(corrida.getSegundas());
                    bean.setSig_terceras(corrida.getTerceras());
                    bean.setSig_torreon(corrida.getTorreon());
                    bean.setSig_coleada(corrida.getColeada());

                    bean.setComprador(corrida.getComprador());
                    bean.setFacturar(corrida.getFacturar());
                    bean.setTipo(corrida.getTipo());
                    bean.setIdcomprador(corrida.getIdcomprador());
                    bean.setIdfacturar(corrida.getIdfacturar());
                    bean.setIdtipo(corrida.getIdtipo());
                    bean.setIdcorrida(corrida.getId());
                    l.add(bean);
                }
            }
        }
        catch (Exception e) 
        {
            System.out.println(e);
        }
        finally
        {
            if(connection!=null)
            {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TransportistaController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return l;
    }
    public ArrayList<Folios> getAll()
    {
        ArrayList<Folios> l=new ArrayList<>();
        Connection connection=null;
        try 
        {
            connection=get_connection();
            Statement st=connection.createStatement();
            try (ResultSet rs = st.executeQuery(querySelect+3+" order by F.FOLIO desc ")) {
                while(rs.next())
                {
                    Folios bean=new Folios();
                    bean.setId(rs.getInt("ID"));
                    bean.setFolio(rs.getString("FOLIO"));
                    bean.setCodigo(getToken(bean.getFolio()));
                    bean.setFecha(rs.getString("FECHA"));
                    bean.setId_productor(rs.getInt("ID_PRODUCTOR"));
                    bean.setPeso_bruto(rs.getInt("PESO_BRUTO"));
                    bean.setPeso_tara(rs.getInt("PESO_TARA"));
                    bean.setPeso_neto(rs.getInt("PESO_NETO"));
                    bean.setNo_rejas(rs.getInt("NO_REJAS"));
                    bean.setTipo_rejas(rs.getInt("ID_REJAS"));
                    bean.setTipo_limon(rs.getInt("ID_TLIMON"));
                    bean.setId_agronomo(rs.getInt("ID_AGRONOMO"));
                    bean.setNo_rejas(rs.getInt("NO_REJAS"));
                    bean.setDejo(rs.getInt("DEJO"));
                    if(bean.getDejo() == 1)
                        bean.setIsdejo(true);
                    else
                        bean.setIsdejo(false);
                    bean.setObservaciones(rs.getString("OBSERVACIONES"));
                    bean.setSegundas(rs.getInt("SEGUNDAS"));
                    bean.setTerceras(rs.getInt("TERCERAS"));
                    bean.setTorreon(rs.getInt("TORREON"));
                    bean.setColeada(rs.getInt("COLEADA"));
                    bean.setJapon(rs.getInt("JAPON"));
                    bean.setProductor(rs.getString("PRODUCTOR"));
                    bean.setAgronomo(rs.getString("AGRONOMO"));
                    bean.setTipos_rejas(rs.getString("TIPO_REJAS"));
                    bean.setTipos_limones(rs.getString("TIPO_LIMON"));
                    CorridasController corridasCTR=new CorridasController();
                    bean.setCorrida(corridasCTR.getOne(bean.getId()));
                    l.add(bean);
                }
            }
        }
        catch (Exception e) 
        {
            System.out.println(e);
        }
        finally
        {
            if(connection!=null)
            {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TransportistaController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return l;
    }
    public ArrayList<Folios> getAll(int i)
    {
        ArrayList<Folios> l=new ArrayList<>();
        Connection connection=null;
        try 
        {
            connection=get_connection();
            Statement st=connection.createStatement();
            try (ResultSet rs = st.executeQuery(querySelect+i+" order by F.FOLIO desc ")) {
                while(rs.next())
                {
                    Folios bean=new Folios();
                    bean.setId(rs.getInt("ID"));
                    bean.setFolio(rs.getString("FOLIO"));
                    bean.setCodigo(getToken(bean.getFolio()));
                    bean.setFecha(rs.getString("FECHA"));
                    bean.setId_productor(rs.getInt("ID_PRODUCTOR"));
                    bean.setPeso_bruto(rs.getInt("PESO_BRUTO"));
                    bean.setPeso_tara(rs.getInt("PESO_TARA"));
                    bean.setPeso_neto(rs.getInt("PESO_NETO"));
                    bean.setNo_rejas(rs.getInt("NO_REJAS"));
                    bean.setTipo_rejas(rs.getInt("ID_REJAS"));
                    bean.setTipo_limon(rs.getInt("ID_TLIMON"));
                    bean.setId_agronomo(rs.getInt("ID_AGRONOMO"));
                    bean.setNo_rejas(rs.getInt("NO_REJAS"));
                    bean.setDejo(rs.getInt("DEJO"));
                    if(bean.getDejo() == 1)
                        bean.setIsdejo(true);
                    else
                        bean.setIsdejo(false);
                    bean.setObservaciones(rs.getString("OBSERVACIONES"));
                    bean.setSegundas(rs.getInt("SEGUNDAS"));
                    bean.setTerceras(rs.getInt("TERCERAS"));
                    bean.setTorreon(rs.getInt("TORREON"));
                    bean.setColeada(rs.getInt("COLEADA"));
                    bean.setJapon(rs.getInt("JAPON"));
                    bean.setProductor(rs.getString("PRODUCTOR"));
                    bean.setAgronomo(rs.getString("AGRONOMO"));
                    bean.setTipos_rejas(rs.getString("TIPO_REJAS"));
                    bean.setTipos_limones(rs.getString("TIPO_LIMON"));
                    l.add(bean);
                }
            }
        }
        catch (Exception e) 
        {
            System.out.println(e);
        }
        finally
        {
            if(connection!=null)
            {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TransportistaController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return l;
    }
    /**
     * Opciono para rechazar el registro
     * @param id
     * @param opcion
     * @return 
     */
    public int rechazar(int id, int opcion)
    {
        int updeteCont=0;
        Connection connection=null;
        try 
        { 
            connection=get_connection();
            PreparedStatement ps=connection.prepareStatement(queryrechazar);
            ps.setInt   (1, opcion);
            ps.setInt   (2, id);
            updeteCont = ps.executeUpdate();
            ps.close();
            updeteCont=1;
        }
        catch (Exception e)
        {
            System.out.println(e);
        }
        finally
        {
            if(connection!=null)
            {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TransportistaController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return updeteCont;
    }
    /**
     * Modificar registro
     * @param id_productor
     * @param peso_bruto
     * @param peso_tara
     * @param peso_neto
     * @param id
     * @param opc
     * @return 
     */
    public int updateRecordFase1(int id_productor,int peso_bruto,int peso_tara,int peso_neto,int id,int opc)
    {  
        int updeteCont=0;
        Connection connection=null;
        try 
        { 
            connection=get_connection();
            PreparedStatement ps=connection.prepareStatement(queryUpdate1);
            ps.setInt   (1, opc);
            ps.setInt   (2, id_productor);
            ps.setInt   (3, peso_bruto);
            ps.setInt   (4, peso_tara);
            ps.setInt   (5, peso_neto);
            ps.setInt   (6, id);
            updeteCont = ps.executeUpdate();
            ps.close();
        }
        catch (Exception e)
        {
            System.out.println(e);
        }
        finally
        {
            if(connection!=null)
            {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TransportistaController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return updeteCont;
    }
    /**
     * Modificar registro
     * @param id_productor
     * @param peso_bruto
     * @param peso_tara
     * @param peso_neto
     * @param id
     * @param opc
     * @return 
     */
    public int updateRecordFase1(int id_productor,int peso_bruto,int peso_tara,int peso_neto,int id,int opc,int usuario)
    {  
        int updeteCont=0;
        Connection connection=null;
        try 
        { 
            connection=get_connection();
            PreparedStatement ps=connection.prepareStatement(queryUpdateMvl1);
            ps.setInt   (1, opc);
            ps.setInt   (2, id_productor);
            ps.setInt   (3, peso_bruto);
            ps.setInt   (4, peso_tara);
            ps.setInt   (5, peso_neto);
            ps.setInt   (6, usuario);
            ps.setInt   (7, id);
            updeteCont = ps.executeUpdate();
            ps.close();
        }
        catch (Exception e)
        {
            System.out.println(e);
        }
        finally
        {
            if(connection!=null)
            {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TransportistaController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return updeteCont;
    }
     
    /**
     * Modifcar Registro
     * @param no_rejas
     * @param id_rejas
     * @param id_tlimon
     * @param id_agronomo
     * @param dejo
     * @param observaciones
     * @param id
     * @return 
     */
    public int updateRecord(int no_rejas,int id_rejas,int id_tlimon,int id_agronomo,int dejo,String observaciones,int id,int opc)
    {  
        int updeteCont=0;
        Connection connection=null;
        try 
        { 
            connection=get_connection();
            PreparedStatement ps=connection.prepareStatement(queryUpdate2);
            ps.setInt   (1, opc);
            ps.setInt   (2, no_rejas);
            ps.setInt   (3, id_rejas);
            ps.setInt   (4, id_tlimon);
            ps.setInt   (5, id_agronomo);
            ps.setInt   (6, dejo);
            ps.setString(7, observaciones);
            ps.setInt   (8, id);
            updeteCont = ps.executeUpdate();
            ps.close();
        }
        catch (Exception e)
        {
            System.out.println(e);
        }
        finally
        {
            if(connection!=null)
            {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TransportistaController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return updeteCont;
    }
    /**
     * Modifcar Registro
     * @param no_rejas
     * @param id_rejas
     * @param id_tlimon
     * @param id_agronomo
     * @param dejo
     * @param observaciones
     * @param id
     * @return 
     */
    public int updateRecord(int no_rejas,int id_rejas,int id_tlimon,int id_agronomo,int dejo,String observaciones,int id,int opc,int usuario)
    {  
        int updeteCont=0;
        Connection connection=null;
        try 
        { 
            connection=get_connection();
            PreparedStatement ps=connection.prepareStatement(queryUpdateMvl2);
            ps.setInt   (1, opc);
            ps.setInt   (2, no_rejas);
            ps.setInt   (3, id_rejas);
            ps.setInt   (4, id_tlimon);
            ps.setInt   (5, id_agronomo);
            ps.setInt   (6, dejo);
            ps.setString(7, observaciones);
            ps.setInt   (8, usuario);
            ps.setInt   (9, id);
            updeteCont = ps.executeUpdate();
            ps.close();
        }
        catch (Exception e)
        {
            System.out.println(e);
        }
        finally
        {
            if(connection!=null)
            {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TransportistaController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return updeteCont;
    }
    
    /**
     * Modifcar Registro
     * @param no_rejas
     * @param id_rejas
     * @param id_tlimon
     * @param dejo
     * @param observaciones
     * @param id
     * @return 
     */
    public int updateRecordSinAgr(int no_rejas,int id_rejas,int id_tlimon,int dejo,String observaciones,int id,int opc)
    {  
        int updeteCont=0;
        Connection connection=null;
        try 
        { 
            connection=get_connection();
            PreparedStatement ps=connection.prepareStatement(queryUpdate2_1);
            ps.setInt   (1, opc);
            ps.setInt   (2, no_rejas);
            ps.setInt   (3, id_rejas);
            ps.setInt   (4, id_tlimon);
            ps.setInt   (5, dejo);
            ps.setString(6, observaciones);
            ps.setInt   (7, id);
            updeteCont = ps.executeUpdate();
            ps.close();
        }
        catch (Exception e)
        {
            System.out.println(e);
        }
        finally
        {
            if(connection!=null)
            {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TransportistaController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return updeteCont;
    }
    /**
     * Modificar Registro
     * @param segundas
     * @param terceras
     * @param torreon
     * @param coleada
     * @param japon
     * @param id
     * @return 
     */
    public int updateRecord(int segundas,int terceras,int torreon,int coleada,int japon,int id)
    {  
        int updeteCont=0;
        Connection connection=null;
        try 
        { 
            connection=get_connection();
            PreparedStatement ps=connection.prepareStatement(queryUpdate3);
            ps.setInt   (1, segundas);
            ps.setInt   (2, terceras);
            ps.setInt   (3, torreon);
            ps.setInt   (4, coleada);
            ps.setInt   (5, japon);
            ps.setInt   (6, id);
            updeteCont = ps.executeUpdate();
            ps.close();
        }
        catch (Exception e)
        {
            System.out.println(e);
        }
        finally
        {
            if(connection!=null)
            {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TransportistaController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return updeteCont;
    }
    
    /**
     * Modificar Registro
     * @param bruto
     * @param tara
     * @param neto
     * @param japon
     * @param segundas
     * @param terceras
     * @param torreon
     * @param coleada
     * @param id
     * @param opc
     * @return 
     */
    public int updateRecord(
            int bruto,int tara,int neto,
            int japon,int segundas,int terceras,int torreon,int coleada,
            int id,int opc)
    {  
        int updeteCont=0;
        Connection connection=null;
        try 
        { 
            connection=get_connection();
            PreparedStatement ps=connection.prepareStatement(queryUpdate4);
            ps.setInt   (1, bruto);
            ps.setInt   (2, tara);
            ps.setInt   (3, neto);
            ps.setInt   (4, japon);
            ps.setInt   (5, segundas);
            ps.setInt   (6, terceras);
            ps.setInt   (7, torreon);
            ps.setInt   (8, coleada);
            ps.setInt   (9, opc);
            ps.setInt   (10,id);
            updeteCont = ps.executeUpdate();
            ps.close();
        }
        catch (Exception e)
        {
            System.out.println(e);
        }
        finally
        {
            if(connection!=null)
            {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TransportistaController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return updeteCont;
    }
    /**
     * Modificar Registro
     * @param bruto
     * @param tara
     * @param neto
     * @param japon
     * @param segundas
     * @param terceras
     * @param torreon
     * @param coleada
     * @param id
     * @param opc
     * @param usuario
     * @return 
     */
    public int updateRecord(
            int bruto,int tara,int neto,
            int japon,int segundas,int terceras,int torreon,int coleada,
            int id,int opc,int usuario)
    {  
        int updeteCont=0;
        Connection connection=null;
        try 
        { 
            connection=get_connection();
            PreparedStatement ps=connection.prepareStatement(queryUpdateMov4);
            ps.setInt   (1, bruto);
            ps.setInt   (2, tara);
            ps.setInt   (3, neto);
            ps.setInt   (4, japon);
            ps.setInt   (5, segundas);
            ps.setInt   (6, terceras);
            ps.setInt   (7, torreon);
            ps.setInt   (8, coleada);
            ps.setInt   (9, opc);
            ps.setInt   (10,usuario);
            ps.setInt   (11,id);
            updeteCont = ps.executeUpdate();
            ps.close();
            if(opc==3)
            {
                CorridasController ctr=new CorridasController();
                ctr.insertaDatoVacio(id);
            }            
        }
        catch (Exception e)
        {
            System.out.println(e);
        }
        finally
        {
            if(connection!=null)
            {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TransportistaController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return updeteCont;
    }
    
    /**
     * Proceso para el borrado de registro.
     * @param id
     * @return 
     */
    public int rechazarActivo(int id)
    {
        //System.out.println("rechazarActivo("+id+")");
        int deleteCont=0;
        Connection connection=null;
        try 
        {
            connection=get_connection();
            PreparedStatement ps=connection.prepareStatement(queryrechazarActivo);
            ps.setInt(1, id);
            deleteCont=ps.executeUpdate();
            ps.close();
        }
        catch (Exception e)
        {
            System.out.println(e);
        }
        finally
        {
            if(connection!=null)
            {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TransportistaController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return deleteCont;   
    }
    /**
     * Proceso para el borrado de registro.
     * @param id
     * @return 
     */
    public int deleteRecord(int id)
    {
        //System.out.println("deleteRecord("+id+")");
        int deleteCont=0;
        Connection connection=null;
        try 
        {
            connection=get_connection();
            PreparedStatement ps=connection.prepareStatement(queryDelete);
            ps.setInt(1, id);
            deleteCont=ps.executeUpdate();
            ps.close();
        }
        catch (Exception e)
        {
            System.out.println(e);
        }
        finally
        {
            if(connection!=null)
            {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TransportistaController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return deleteCont;   
    }
    /**
     * Proceso para la modificación de registro. 
     * @param id
     * @return numero de registros dados de alta.
     */
    private  int updateRecord(int id)
    {
        //System.out.println("updateRecord("+ id+")");
        int updeteCont=0;
        Connection connection=null;
        try 
        { 
            connection=get_connection();
            PreparedStatement ps=connection.prepareStatement(queryUpdateNumber); 
            ps.setInt(1, id);
            updeteCont = ps.executeUpdate();
            ps.close();
        }
        catch (Exception e)
        {
            System.out.println(e);
        }
        finally
        {
            if(connection!=null)
            {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TransporteController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return updeteCont;
    }
    /**
     * Insertar Registro
     * @param id_productor
     * @param peso_bruto
     * @param peso_tara
     * @param peso_neto
     * @return 
     */
    public Folios  insertRecord(int id_productor,int peso_bruto,int peso_tara,int peso_neto)
    {
        //System.out.println("insertRecord()");
        int insertCont=0;
        Folios bean=null;
        Connection connection=null;
        try 
        {
            connection=get_connection();
            PreparedStatement ps=connection.prepareStatement(queryCreate);
            ps.setInt (1, id_productor);
            ps.setInt (2, peso_bruto);
            ps.setInt (3, peso_tara);
            ps.setInt (4, peso_neto);
            insertCont=ps.executeUpdate();
            if(insertCont==1)
            {
                bean  =  getMax();
                updateRecord(bean.getId());
            }
            ps.close();
        }
        catch (Exception e) 
        {
            System.out.println(e);
        }
        finally
        {
            if(connection!=null)
            {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TransportistaController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return bean;
    }
    
    /**
     * Insertar Registro
     * @param id_productor
     * @param peso_bruto
     * @param peso_tara
     * @param peso_neto
     * @return 
     */
    public Folios  insertRecord(int id_productor,int peso_bruto,int peso_tara,int peso_neto,int usuario)
    {
        //System.out.println("insertRecord()");
        int insertCont=0;
        Folios bean=null;
        Connection connection=null;
        try 
        {
            connection=get_connection();
            //INSERT INTO CC_FOLIOS 
            //(ID_PRODUCTOR,PESO_BRUTO,PESO_TARA,PESO_NETO,CREADOPOR,MODIFICADOPOR)VALUES (?,?,?,?,?,?)
            PreparedStatement ps=connection.prepareStatement(queryCreateMvl);
            ps.setInt (1, id_productor);
            ps.setInt (2, peso_bruto);
            ps.setInt (3, peso_tara);
            ps.setInt (4, peso_neto);
            ps.setInt (5, usuario);
            ps.setInt (6, usuario);
            insertCont=ps.executeUpdate();
            if(insertCont==1)
            {
                bean  =  getMax();
                updateRecord(bean.getId());
                bean  =  getOne(bean.getId());
            }
            ps.close();
        }
        catch (Exception e) 
        {
            System.out.println(e);
        }
        finally
        {
            if(connection!=null)
            {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TransportistaController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return bean;
    }
    
    private String getToken(String cadena)
    {
      cadena =  cadena.substring(3,15);
      String pww="";
      if(cadena.length()%2 == 0)
      {
          for(int i=0;i<(cadena.length()/2);i++)
          {
              String tok=cadena.substring((2*i),(2*(i+1)));
              pww= pww + (sexty(tok));
          }
      }
      return pww;
    }
    private String  sexty(String to)
    {
      int cod=(new Integer(to));
      switch(cod)
      {
          case 0: return "0";
          case 1: return "1";
          case 2: return "2";
          case 3: return "3";
          case 4: return "4";
          case 5: return "5";
          case 6: return "6";
          case 7: return "7";
          case 8: return "8";
          case 9: return "9";
          
          case 10: return "A";
          case 11: return "B";
          case 12: return "C";
          case 13: return "D";
          case 14: return "E";
          case 15: return "F";
          case 16: return "G";
          case 17: return "H";
          case 18: return "I";
          case 19: return "J";
          
          case 20: return "K";
          case 21: return "L";
          case 22: return "M";
          case 23: return "N";
          case 24: return "O";
          case 25: return "P";
          case 26: return "Q";
          case 27: return "R";
          case 28: return "S";
          case 29: return "T";
          
          case 30: return "U";
          case 31: return "V";
          case 32: return "W";
          case 33: return "X";
          case 34: return "Y";
          case 35: return "Z";
          case 36: return "*";
          case 37: return "#";
          case 38: return "$";
          case 39: return "%";
          
          
          case 40: return "&";
          case 41: return "/";
          case 42: return "(";
          case 43: return ")";
          case 44: return "=";
          case 45: return "!";
          case 46: return "+";
          case 47: return "{";
          case 48: return "}";
          case 49: return "[";
          
          
          case 50: return "]";
          case 51: return "-";
          case 52: return ".";
          case 53: return ",";
          case 54: return ";";
          case 55: return ":";
          case 56: return "<";
          case 57: return ">";
          case 58: return "@";
          case 59: return "?";
      }
      return " ";
    }
}
