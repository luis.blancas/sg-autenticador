/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.org.citicos.service;
/**
 *
 * @author luisa
 */
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import mx.org.citicos.controller.ActivoController;
import mx.org.citicos.controller.CalibreController;
import mx.org.citicos.controller.CalidadController;
import mx.org.citicos.controller.PalletController;
import mx.org.citicos.controller.PalletDescController;
import mx.org.citricos.entity.Activo;
import mx.org.citricos.entity.Calibre;
import mx.org.citricos.entity.Calidad;
import mx.org.citricos.entity.Pallet_desc;
 
@ManagedBean(name = "detalleService")
@ApplicationScoped
public class PalletDetalleService implements Serializable
{
    private int idPalet=0; 
    private Pallet_desc one;
    private List<Pallet_desc> todos;    
    private List<String> calibre=null;
    private List<String> calidad=null;
    private List<String> activo=null;
    PalletDescController controller=null;
    public List<Pallet_desc> getTodos() 
    {
        controller=new PalletDescController(this.idPalet);
        todos=controller.getAll(this.idPalet);
        System.out.println("getTodos() >> "+todos.size());
        return todos;
    }

    public void setTodos(List<Pallet_desc> todos) {
        this.todos = todos;
    }

    public List<String> getCalibre() 
    {
        CalibreController controller_Calibre=new CalibreController();
        List<Calibre> calibreRY=controller_Calibre.getAll();
        calibre=new ArrayList<String>();
        for(int x=0;x<calibreRY.size();x++)
        {
            Calibre clb=calibreRY.get(x);
            String cadena = (clb.getId()+".-"+ clb.getNombre());
            calibre.add(cadena);
        }
        return calibre;
    }
    public List<String> getActivo() 
    {
        ActivoController controller_Activo=new ActivoController();
        List<Activo> activoRY=controller_Activo.getAll();
        activo=new ArrayList<String>();
        for(int x=0;x<activoRY.size();x++)
        {
            Activo clb=activoRY.get(x);
            activo.add(clb.getId()+".-"+ clb.getNombre());
        }
        return activo;
    }

    public void setCalibre(List<String> calibre) {
        this.calibre = calibre;
    }

    public List<String> getCalidad() 
    {
        CalidadController controller_Calidad=new CalidadController();
        List<Calidad> calidadRY=controller_Calidad.getAll();
        calidad=new ArrayList<String>();
        for(int x=0;x<calidadRY.size();x++)
        {
            Calidad clb=calidadRY.get(x);
            calidad.add(clb.getId()+".-"+ clb.getNombre());
        }
        return calidad;
    }

    public void setCalidad(List<String> calidad) {
        this.calidad = calidad;
    }
    public int create(int cajas,int calibre,int calidad ,String cdi) 
    { 
        int detalle=controller.insertRecord(cajas, calibre, calidad, cdi, this.idPalet, 1);
        return detalle;
    }

    public Pallet_desc getOneNew() 
    {
        PalletDescController controller=new PalletDescController(this.idPalet);
        one=controller.creaNuevo(this.idPalet);
        return one;
    }
 
    public void setOne(Pallet_desc one) {
        this.one = one;
    }

    public int getIdPalet() {
        return idPalet;
    }

    public void setIdPalet(int idPalet) { 
        this.idPalet = idPalet;
    }
    
    public void setIdPaletFinal(int idPalletDesc) {
        PalletController controller=new PalletController();
        this.idPalet =controller.getIdPallet(idPalletDesc);
        setIdPalet(this.idPalet);
    } 

    public String modificaDetale(Integer id, Integer cajas, String cdi, String calidad, String calibre,String activo,int usuario) 
    {    
        String msj="";
        String tokensE[]   =  calibre.split(".-");
        int calibreInt =  new Integer (tokensE[0]);
        
        String tokensD[]   =  calidad.split(".-");
        int calidadInt =  new Integer (tokensD[0]);
        
        String tokensA[]   =  activo.split(".-");
        int activoInt =  new Integer (tokensA[0]);
        
        
        int numero = controller.updateRecor(id.intValue(), cajas.intValue(), calibreInt, calidadInt, cdi,activoInt, usuario);
        
        if(numero>0)
            msj = "Se modifico con exito ["+id+"]";
        else
            msj ="Error al modificar el dealle ["+id+"]";
        return msj;
    }

    public void setActivo(List<String> activo) {
        this.activo = activo;
    }
    
    
}
