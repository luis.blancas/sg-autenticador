/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.org.citicos.service;

import java.io.Serializable; 
import javax.faces.context.FacesContext;  
/**
 *
 * @author luisa
 */
public class  SessionUtils implements Serializable {
    
    public void add(String key, Object value) 
    {
        FacesContext.getCurrentInstance().getAttributes().put(key, value);
    }

    public Object get(String key) 
    {
        return FacesContext.getCurrentInstance().getAttributes().get(key);
    }
}
