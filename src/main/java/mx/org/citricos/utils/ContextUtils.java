/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.org.citricos.utils; 
import java.io.IOException;
import java.io.Serializable; 
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession; 
import mx.org.citricos.mb.Login;
/**
 *
 * @author luisa
 */
public class ContextUtils  implements Serializable{
    /**
     * Representa el valor inicial de la versiÃ³n del serial
     */
    private static final long serialVersionUID = 1L;
    public FacesContext getConext()
    {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return facesContext;
    }
    public void logout()
    { 
        try 
        {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
            session.setAttribute("usuario", null);
            session.invalidate();
            facesContext.getExternalContext().invalidateSession();
            facesContext.addMessage(null, new FacesMessage("Logout", "Se cerro con exito la sesion"));
            facesContext.getExternalContext().redirect("../../../Login.xhtml?faces-redirect=true");
        }
        catch (IOException ex) 
        {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String crearAtributo(String key,Object value)
    {
        try
        {
            FacesContext facesContext = FacesContext.getCurrentInstance(); 
            if (facesContext == null) 
            {
                return (String)
                        ("Se intentá acceder al FacesContext fuera del contexto "
                                + "de Java Server Faces, se regresará¡ una instancia nula.FacesContext");
            }
            HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
            if(session==null)
            {
                return (String)
                        ("Se intentá acceder al FacesContext fuera del contexto "
                                + "de Java Server Faces, se regresará¡ una instancia nula.Session Nula(1)");
            }
            session.setAttribute(key,value);
            return "OK";
        }
        catch(Exception ed)
        {
            return "Error."+ed.getMessage();
        }
    }
    public Object leerAtributo(String key)
    {
        try
        {
            FacesContext facesContext = FacesContext.getCurrentInstance(); 
            if (facesContext == null) 
            {
                return (String)
                        ("Se intentá acceder al FacesContext fuera del contexto "
                                + "de Java Server Faces, se regresará¡ una instancia nula.FacesContext");
            }
            HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
            if(session==null)
            {
                return (String)
                        ("Se intentá acceder al FacesContext fuera del contexto "
                                + "de Java Server Faces, se regresará¡ una instancia nula.Session Nula(1)");
            }
            return session.getAttribute(key); 
        }
        catch(Exception ed)
        {
            return "Error."+ed.getMessage();
        }
    }
}
