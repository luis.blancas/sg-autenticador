/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.org.citricos.utils;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.apache.tomcat.util.codec.binary.Base64;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
/**
 *
 * @author luisa
 */
public class GeratorQRUtil {
    public static String getImageQR(String text, String ext, int width, int height) {
            String encodedfile = null;
            try {
                    QRCodeWriter qrCodeWriter = new QRCodeWriter();
                BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, width, height);
                ByteArrayOutputStream pngOutputStream = new ByteArrayOutputStream();
                MatrixToImageWriter.writeToStream(bitMatrix, ext, pngOutputStream);
                byte[] pngData = pngOutputStream.toByteArray(); 
                encodedfile = new String(Base64.encodeBase64(pngData), "UTF-8");
            } catch(WriterException wex) {
                    System.out.println(wex.getMessage());
            } catch(IOException ioe) {
                    System.out.println(ioe.getMessage());
            } 
            return encodedfile;
    }
    public static byte[] decodeBase64byte(String base64String)
    {
            return Base64.decodeBase64(base64String);
    }
    public static void writeFile(String ruta,byte[] fileArray)
    {
        try
        {
            FileOutputStream fileOuputStream = new FileOutputStream(ruta);
            fileOuputStream.write(fileArray);
            fileOuputStream.close();
        }catch(Exception e)
        {
            System.out.println(e.getMessage());
        } 
    }
 
    public static byte[] foundByte( ) throws FileNotFoundException, IOException  
    {
        File file=new File("/resources/images/citricos.png");
        ByteArrayOutputStream ous = null;
        InputStream ios = null;
        try 
        {
            byte[] buffer = new byte[4096];
            ous = new ByteArrayOutputStream();
            ios = new FileInputStream(file);
            int read = 0;
            while ((read = ios.read(buffer)) != -1) {
                ous.write(buffer, 0, read);
            }
        }
        finally 
        {
            try {
                if (ous != null)
                    ous.close();
            } catch (IOException e) {
            }

            try {
                if (ios != null)
                    ios.close();
            } catch (IOException e) {
            }
        }
        return ous.toByteArray();
    }
}
