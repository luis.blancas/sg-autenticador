/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.org.citricos.utils;

/**
 *
 * @author luisa
 */

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Clase utilitaria que proveÃ© algunos mÃ©todos para interactuar con las clases de IO.
*/
public final class FileUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileUtils.class);

    /**
     * Encoding usado en el aplicaciÃ³n: UTF-8
     */
    public static final String DEFAULT_ENCONDING = "UTF-8";
    
    private FileUtils() {
    }

    /**
     * Cierra de forma segura el FileInputStream
     *
     * @param fis
     */
    public static void close(InputStream fis) {
        if (fis == null) {
            LOGGER.warn("Se intentÃ³ cerrar un inputStream nulo");
        } else {
            try {
                fis.close();
            } catch (Exception ex) {
                LOGGER.error("Error al cerrar el inputStrema", ex);
            }
        }
    }

    /**
     * Cierra de forma segura el FileReader
     *
     * @param fr
     */
    public static void close(FileReader fr) {
        if (fr == null) {
            LOGGER.warn("Se intentÃ³ cerrar un fileReader nulo");
        } else {
            try {
                fr.close();
            } catch (Exception ex) {
                LOGGER.error("Error al cerrar el fileReader", ex);
            }
        }
    }

    /**
     * Cierra de forma segura el InputStreamReader
     *
     * @param isr
     */
    public static void close(InputStreamReader isr) {
        if (isr == null) {
            LOGGER.warn("Se intentÃ³ cerrar un InputStreamReader nulo");
        } else {
            try {
                isr.close();
            } catch (Exception ex) {
                LOGGER.error("Error al cerrar el InputStreamReader", ex);
            }
        }
    }
    
    /**
     * Recupera un InputStreamReader a partir de la ruta dada. Intenta usar la codificaciÃ³n
     * UFT-8, en caso de error se usa la default del sistema.
     * 
     * @param filePath
     * @return 
     * @throws FileNotFoundException 
     */
    public static InputStreamReader getInputStream(String filePath) throws FileNotFoundException {
        FileInputStream fis = new FileInputStream(filePath);
        try {
            return new InputStreamReader(fis, DEFAULT_ENCONDING);
        } catch (UnsupportedEncodingException ex) {
            LOGGER.warn("No fue posible usar la codificaciÃ³n UTF-8, se usarÃ¡ la default del sistema", ex);
            return new InputStreamReader(fis, Charset.defaultCharset());
        }
    }
}