/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.org.citricos.entity;

/**
 *
 * @author luisa
 */
public class Corrida 
{
    private Integer id;
    private Integer folio;
    private Double  verde_japon=0d;
    private Double  verde_110=0d;
    private Double  verde_150=0d;
    private Double  verde_175=0d;
    private Double  verde_200=0d;
    private Double  verde_230=0d;
    private Double  verde_250=0d;
    private Double  empaque_110=0d;
    private Double  empaque_150=0d;
    private Double  empaque_175=0d;
    private Double  empaque_200=0d;
    private Double  empaque_230=0d;
    private Double  empaque_250=0d;	
    private Double  segundas=0d;
    private Double  terceras=0d;
    private Double  torreon=0d;
    private Double  coleada=0d;
    private String  comprador; 
    private String  facturar;
    private String  tipo;
    private Integer idcomprador; 
    private Integer idfacturar;
    private Integer idtipo;

    private Double  verde_suma=0d;
    private Double  empaque_suma=0d;
    private Double  corrida_suma=0d;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getVerde_japon() {
        return verde_japon;
    }

    public void setVerde_japon(Double verde_japon) {
        this.verde_japon = verde_japon;
    }

    public Double getVerde_110() {
        return verde_110;
    }

    public void setVerde_110(Double verde_110) {
        this.verde_110 = verde_110;
    }

    public Double getVerde_150() {
        return verde_150;
    }

    public void setVerde_150(Double verde_150) {
        this.verde_150 = verde_150;
    }

    public Double getVerde_175() {
        return verde_175;
    }

    public void setVerde_175(Double verde_175) {
        this.verde_175 = verde_175;
    }

    public Double getVerde_200() {
        return verde_200;
    }

    public void setVerde_200(Double verde_200) {
        this.verde_200 = verde_200;
    }

    public Double getVerde_230() {
        return verde_230;
    }

    public void setVerde_230(Double verde_230) {
        this.verde_230 = verde_230;
    }

    public Double getVerde_250() {
        return verde_250;
    }

    public void setVerde_250(Double verde_250) {
        this.verde_250 = verde_250;
    }

    public Double getEmpaque_110() {
        return empaque_110;
    }

    public void setEmpaque_110(Double empaque_110) {
        this.empaque_110 = empaque_110;
    }

    public Double getEmpaque_150() {
        return empaque_150;
    }

    public void setEmpaque_150(Double empaque_150) {
        this.empaque_150 = empaque_150;
    }

    public Double getEmpaque_175() {
        return empaque_175;
    }

    public void setEmpaque_175(Double empaque_175) {
        this.empaque_175 = empaque_175;
    }

    public Double getEmpaque_200() {
        return empaque_200;
    }

    public void setEmpaque_200(Double empaque_200) {
        this.empaque_200 = empaque_200;
    }

    public Double getEmpaque_230() {
        return empaque_230;
    }

    public void setEmpaque_230(Double empaque_230) {
        this.empaque_230 = empaque_230;
    }

    public Double getEmpaque_250() {
        return empaque_250;
    }

    public void setEmpaque_250(Double empaque_250) {
        this.empaque_250 = empaque_250;
    }

    public Double getSegundas() {
        return segundas;
    }

    public void setSegundas(Double segundas) {
        this.segundas = segundas;
    }

    public Double getTerceras() {
        return terceras;
    }

    public void setTerceras(Double terceras) {
        this.terceras = terceras;
    }

    public Double getTorreon() {
        return torreon;
    }

    public void setTorreon(Double torreon) {
        this.torreon = torreon;
    }

    public Double getColeada() {
        return coleada;
    }

    public void setColeada(Double coleada) {
        this.coleada = coleada;
    }

    public String getComprador() {
        return comprador;
    }

    public void setComprador(String comprador) {
        this.comprador = comprador;
    }

    public String getFacturar() {
        return facturar;
    }

    public void setFacturar(String facturar) {
        this.facturar = facturar;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Integer getIdcomprador() {
        return idcomprador;
    }

    public void setIdcomprador(Integer idcomprador) {
        this.idcomprador = idcomprador;
    }

    public Integer getIdfacturar() {
        return idfacturar;
    }

    public void setIdfacturar(Integer idfacturar) {
        this.idfacturar = idfacturar;
    }

    public Integer getIdtipo() {
        return idtipo;
    }

    public void setIdtipo(Integer idtipo) {
        this.idtipo = idtipo;
    }

    public Integer getFolio() {
        return folio;
    }

    public void setFolio(Integer folio) {
        this.folio = folio;
    }

    public Double getVerde_suma() {
        this.verde_suma =
            this.verde_japon+
            this.verde_110+
            this.verde_150+
            this.verde_175+
            this.verde_200+
            this.verde_230+
            this.verde_250;
        return verde_suma;
    }

    public void setVerde_suma(Double verde_suma) {
        this.verde_suma = verde_suma;
    }

    public Double getEmpaque_suma() {
        this.empaque_suma =
            this.empaque_110+
            this.empaque_150+
            this.empaque_175+
            this.empaque_200+
            this.empaque_230+
            this.empaque_250;
        return empaque_suma;
    }

    public void setEmpaque_suma(Double empaque_suma) {
        this.empaque_suma = empaque_suma;
    }

    public Double getCorrida_suma() {
        this.corrida_suma =
            this.segundas+
            this.terceras+
            this.torreon+
            this.coleada;
        return corrida_suma;
    }

    public void setCorrida_suma(Double corrida_suma) {
        this.corrida_suma = corrida_suma;
    }

    public String sumaverdes()
    {
        this.verde_suma =
            this.verde_japon+
            this.verde_110+
            this.verde_150+
            this.verde_175+
            this.verde_200+
            this.verde_230+
            this.verde_250;
        return "";
    }
    
    public String empaquesuma()
    {
        this.empaque_suma =
            this.empaque_110+
            this.empaque_150+
            this.empaque_175+
            this.empaque_200+
            this.empaque_230+
            this.empaque_250;
        return "";
    }
    public String corridasuma()
    {
        this.corrida_suma =
            this.segundas+
            this.terceras+
            this.torreon+
            this.coleada;
        return "";
    }
    
    @Override
    public String toString() {
        return "Corrida {" + "id=" + id + ", folio=" + folio + "\n, "
                + "[ verde_japon=" + verde_japon + ", verde_110=" + verde_110 + ", verde_150=" + verde_150 + ", verde_175=" + verde_175 + ", verde_200=" + verde_200 + ", verde_230=" + verde_230 + ", verde_250=" + verde_250 +
                "],\n [empaque_110=" + empaque_110 + ", empaque_150=" + empaque_150 + ", empaque_175=" + empaque_175 + ", empaque_200=" + empaque_200 + ", empaque_230=" + empaque_230 + ", empaque_250=" + empaque_250 + 
                "],\n [segundas=" + segundas + ", terceras=" + terceras + ", torreon=" + torreon + ", coleada=" + coleada + ", comprador=" + comprador + ", facturar=" + facturar + ", tipo=" + tipo + ", idcomprador=" + idcomprador + "]"
              + "],[ idfacturar=" + idfacturar + ", idtipo=" + idtipo + '}';
    }

    
    
}
