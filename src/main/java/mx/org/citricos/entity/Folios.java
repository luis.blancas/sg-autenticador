/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.org.citricos.entity;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author luisa
 */
public class Folios 
{
    private Integer id;
    private String  folio;
    private String  fecha;
    private Integer id_productor;
    private Integer peso_bruto=0;
    private Integer peso_tara=0;
    private Integer peso_neto=0;
    //---------------------------------------
    private Integer no_rejas;
    private Integer tipo_rejas;
    private Integer tipo_limon;
    private Integer id_agronomo;
    private Integer dejo;
    private boolean isdejo;
    private String observaciones;
            
    //---------------------------------------
    private Integer segundas;
    private Integer terceras;
    private Integer torreon;
    private Integer coleada;
    private Integer japon;
    //---------------------------------------
    //---------------------------------------
    private String productor;
    private String agronomo;
    private String tipos_rejas;
    private String tipos_limones;
    
    private String codigo="";
    private Integer estatus=0;
    private Corrida corrida;
            
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getFecha() 
    {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date d=new Date();
        String fechaN=format.format(d);
        this.fecha = fechaN;
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Integer getId_productor() {
        return id_productor;
    }

    public void setId_productor(Integer id_productor) {
        this.id_productor = id_productor;
    }

    public Integer getPeso_bruto() {
        return peso_bruto;
    }

    public void setPeso_bruto(Integer peso_bruto) {
        this.peso_bruto = peso_bruto;
    }

    public Integer getPeso_tara() {
        return peso_tara;
    }

    public void setPeso_tara(Integer peso_tara) {
        this.peso_tara = peso_tara;
    }

    public Integer getPeso_neto() {
        return peso_neto;
    }

    public void setPeso_neto(Integer peso_neto) {
        this.peso_neto = peso_neto;
    }

    public Integer getNo_rejas() {
        return no_rejas;
    }

    public void setNo_rejas(Integer no_rejas) {
        this.no_rejas = no_rejas;
    }

    public Integer getTipo_rejas() {
        return tipo_rejas;
    }

    public void setTipo_rejas(Integer tipo_rejas) {
        this.tipo_rejas = tipo_rejas;
    }

    public Integer getTipo_limon() {
        return tipo_limon;
    }

    public void setTipo_limon(Integer tipo_limon) {
        this.tipo_limon = tipo_limon;
    }

    public Integer getId_agronomo() {
        return id_agronomo;
    }

    public void setId_agronomo(Integer id_agronomo) {
        this.id_agronomo = id_agronomo;
    }

    public Integer getDejo() {
        return dejo;
    }

    public void setDejo(Integer dejo) {
        this.dejo = dejo;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Integer getSegundas() {
        return segundas;
    }

    public void setSegundas(Integer segundas) {
        this.segundas = segundas;
    }

    public Integer getTerceras() {
        return terceras;
    }

    public void setTerceras(Integer terceras) {
        this.terceras = terceras;
    }

    public Integer getTorreon() {
        return torreon;
    }

    public void setTorreon(Integer torreon) {
        this.torreon = torreon;
    }

    public Integer getColeada() {
        return coleada;
    }

    public void setColeada(Integer coleada) {
        this.coleada = coleada;
    }

    public Integer getJapon() {
        return japon;
    }

    public void setJapon(Integer japon) {
        this.japon = japon;
    }

    public String getProductor() {
        return productor;
    }

    public void setProductor(String productor) {
        this.productor = productor;
    }

    public String getAgronomo() {
        return agronomo;
    }

    public void setAgronomo(String agronomo) {
        this.agronomo = agronomo;
    }

    public String getTipos_rejas() {
        return tipos_rejas;
    }

    public void setTipos_rejas(String tipos_rejas) {
        this.tipos_rejas = tipos_rejas;
    }

    public String getTipos_limones() {
        return tipos_limones;
    }

    public void setTipos_limones(String tipos_limones) {
        this.tipos_limones = tipos_limones;
    }

    public boolean isIsdejo() {
        return isdejo;
    }

    public void setIsdejo(boolean isdejo) {
        this.isdejo = isdejo;
    }  

    @Override
    public String toString() {
        return "Folios{" + "id=" + id + ", folio=" + folio + ", fecha=" + fecha + ", id_productor=" + id_productor + 
                ", peso_bruto=" + peso_bruto + ", peso_tara=" + peso_tara + ", peso_neto=" + peso_neto + 
                ", no_rejas=" + no_rejas + ", tipo_rejas=" + tipo_rejas + ", tipo_limon=" + tipo_limon + 
                ", id_agronomo=" + id_agronomo + ", dejo=" + dejo + ", isdejo=" + isdejo + ", observaciones=" + observaciones + 
                ", segundas=" + segundas + ", terceras=" + terceras + ", torreon=" + torreon + ", coleada=" + coleada + ", "
                + "japon=" + japon + ", productor=" + productor + ", agronomo=" + agronomo + ", tipos_rejas=" + tipos_rejas + 
                ", tipos_limones=" + tipos_limones + '}';
    }

    public String getCodigo() 
    { 
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Corrida getCorrida() {
        return corrida;
    }

    public void setCorrida(Corrida corrida) {
        this.corrida = corrida;
    }

    public Integer getEstatus() {
        return estatus;
    }

    public void setEstatus(Integer estatus) {
        this.estatus = estatus;
    }
    
    
    
}
