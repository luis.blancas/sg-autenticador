/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.org.citricos.mb;

import java.io.IOException;
import mx.org.citicos.controller.UsuarioController;
import mx.org.citicos.controller.UtilidadesController;
import mx.org.citricos.entity.Usuario;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext; 
import javax.servlet.http.HttpSession;
@ManagedBean (name = "login")
@RequestScoped
public class Login implements Serializable
{ 
    private String usuariom="";
    private String password="";
    private Usuario user;
    
    @PostConstruct
    public void init() {
        user=new Usuario();
        usuariom=new String();
        password=new String();
        
    }

    public String LoginControl()
    { 
        FacesContext facesContext = FacesContext.getCurrentInstance();
        UsuarioController controller = new UsuarioController();
        UtilidadesController utilidades =new UtilidadesController();
        this.user = controller.findControl(usuariom, password);
        if(this.user != null)
        {
            if(this.user.getMsg().toUpperCase().equals("OK"))
            { 
                if (facesContext == null) 
                {
                    System.out.println
                            ("Se intentá acceder al FacesContext fuera del contexto "
                                    + "de Java Server Faces, se regresará¡ una instancia nula.FacesContext");
                    facesContext.addMessage(null, new FacesMessage("Error", 
                    "Se intentá acceder al FacesContext fuera del contexto "
                                    + "de Java Server Faces, se regresará¡ una instancia nula.: FacesContext"));
                    facesContext.addMessage(null, new FacesMessage("Acceso denegado: FacesContext", user.getMsg()));
                    return "";
                }
                HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
                session.setMaxInactiveInterval(5*60);
                if(session==null)
                {
                    System.out.println
                            ("Se intentá acceder al FacesContext fuera del contexto "
                                    + "de Java Server Faces, se regresará¡ una instancia nula.Session Nula(1)");
                    facesContext.addMessage(null, new FacesMessage("Error", 
                    "Se intentá acceder al FacesContext fuera del contexto "
                                    + "de Java Server Faces, se regresará¡ una instancia nula.: FacesContext"));
                    facesContext.addMessage(null, new FacesMessage("Acceso denegado: FacesContext", user.getMsg()));
                    return "";   
                }
                session.setAttribute("usuario", this.user);  
                if(this.user.getPagina().indexOf("function")>=0)
                    return utilidades.function(this.user.getPagina());
                return this.user.getPagina(); 
            }
          else
            {
                facesContext.addMessage(null, new FacesMessage("Error", (new StringBuilder()).append("Error de acceso con usuario [").append(usuariom).append("]").toString()));
                facesContext.addMessage(null, new FacesMessage("Acceso denegado", user.getMsg()));
                return "";
            }
        } else
        {
            facesContext.addMessage(null, new FacesMessage("Error", (new StringBuilder()).append("Error de acceso con usuario [").append(usuariom).append("]").toString()));
            facesContext.addMessage(null, new FacesMessage("Acceso denegado", "Usuario o contrase\361a incorrecta"));
            return "";
        }
    }
 
    public void logout()
    {
        try 
        {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
            session.setAttribute("usuario", null);
            session.invalidate();
            facesContext.getExternalContext().invalidateSession();
            facesContext.addMessage(null, new FacesMessage("Logout", "Se cerro con exito la sesion"));
            facesContext.getExternalContext().redirect("../../../Login.xhtml?faces-redirect=true");
        }
        catch (IOException ex) 
        {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getUsuariom() {
        return usuariom;
    }

    public void setUsuariom(String usuariom) {
        this.usuariom = usuariom;
    }
    

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Usuario getUser() {
        return user;
    }

    public void setUser(Usuario user) {
        this.user = user;
    }
     
}