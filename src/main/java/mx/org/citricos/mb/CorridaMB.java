/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.org.citricos.mb;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.MalformedURLException;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.servlet.ServletContext;
import mx.org.citicos.controller.CorridasController;
import mx.org.citicos.controller.FolioController;
import mx.org.citicos.controller.PreciosController;
import mx.org.citricos.entity.Corrida;
import mx.org.citricos.entity.CorridaFolio;
import mx.org.citricos.entity.Folios;
import mx.org.citricos.entity.Precios;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.printing.PDFPageable;

/**
 *
 * @author luis Adrian
 */ 
@ManagedBean (name = "corridaMB")
@RequestScoped
public class CorridaMB  implements Serializable
{
    private final FacesContext context = FacesContext.getCurrentInstance();
    private final float[] columnWidths6 = new float[]{84f,84f,84f,84f,84f,84f};
    private final float[] columnWidths5 = new float[]{84f,84f,84f,84f,84f};
    private final float[] columnWidths15 = new float[]{84f,420f};
    private final float[] columnWidths4 = new float[]{99f,99f,99f,99f};
    private final float[] columnWidths24 = new float[]{108,396f};
    private final float[] columnWidths2 = new float[]{84f,84f};
    private final float[] columnWidths1 = new float[]{84f};
    private final float[] columnWidths004 = new float[]{84f,84f,84f,84f};
    private final float[] columnWidths024 = new float[]{168,336};
    private final float[] columnWidths003 = new float[]{168,168,168};
    
    private final float[] columnWidths007 = new float[]{84f,420f};
    private final float[] columnWidths008 = new float[]{396f};
    
    Font fontH_11 = new Font( Font.NORMAL, 11, Font.NORMAL);
    Font fontH_10 = new Font( Font.NORMAL, 10, Font.NORMAL);
    Font fontH_09 = new Font( Font.NORMAL,  9, Font.NORMAL);
    Font fontH_08 = new Font( Font.NORMAL,  8, Font.NORMAL);
    Font fontH_07 = new Font( Font.NORMAL,  7, Font.NORMAL);
    Font fontH_06 = new Font( Font.NORMAL,  6, Font.NORMAL);
    
    CorridasController controller=new CorridasController();
    private List<CorridaFolio> activos; 
    private CorridaFolio activoSeleccionado;
    private CorridaFolio activoEditar =new CorridaFolio();
    private CorridaFolio activoAnexar =new CorridaFolio();
    private CorridaFolio activoBorrado=new CorridaFolio();  
    private CorridaFolio activo       =new CorridaFolio();  
    private double verdes_cantidad=0;
    private double verdes_porcentaje=0;
    private double verdes_precio=0;
    private double verdes_total=0;
    
    private double empaques_cantidad=0;
    private double empaques_porcentaje=0;
    private double empaques_precio=0;
    private double empaques_total=0;
    
    private double desechos_cantidad=0;
    private double desechos_porcentaje=0;
    private double desechos_precio=0;
    private double desechos_total=0;
    
    private double _cantidad=0;
    private double _porcentaje=0;
    private double _precio=0;
    private double _total=0;
    
    @PostConstruct
    public void init() 
    {
        this.activoSeleccionado=new CorridaFolio();
        
    }
    public List<CorridaFolio> getActivos() 
    {
        this.activos= controller.getCorridas(3);
        return activos;
    }
    public String seleccionEditar(CorridaFolio editar)
    {
        this.activoEditar=new CorridaFolio();
        this.activoSeleccionado=new CorridaFolio();
        this.activoEditar.setId(editar.getId()); 
        this.activoEditar.setFolio(editar.getFolio());
        this.activoEditar.setId_agronomo(editar.getId_agronomo());
        this.activoEditar.setId_productor(editar.getId_productor());
        this.activoEditar.setPeso_bruto(editar.getPeso_bruto());
        this.activoEditar.setPeso_neto(editar.getPeso_neto());
        this.activoEditar.setPeso_tara(editar.getPeso_tara());
        this.activoEditar.setNo_rejas(editar.getNo_rejas());
        this.activoEditar.setTipo_rejas(editar.getTipo_rejas());
        this.activoEditar.setTipo_limon(editar.getTipo_limon());
        this.activoEditar.setTipos_limones(editar.getTipos_limones());
        this.activoEditar.setTipos_rejas(editar.getTipos_rejas());
        this.activoEditar.setDejo(editar.getDejo());
        this.activoEditar.setIsdejo(editar.isIsdejo());
        this.activoEditar.setObservaciones(editar.getObservaciones());
        this.activoEditar.setSig_segundas((double)editar.getSig_segundas());
        this.activoEditar.setSig_terceras((double)editar.getSig_terceras());
        this.activoEditar.setSig_torreon((double)editar.getSig_torreon());
        this.activoEditar.setSig_coleada((double)editar.getSig_coleada());
        //this.activoEditar.setSi ((double)editar.getJapon());
        this.activoEditar.setProductor(editar.getProductor());
        this.activoEditar.setAgronomo(editar.getAgronomo());
        this.activoEditar.setCodigo(editar.getCodigo());
        this.activoEditar.setColeada(editar.getColeada());
        this.activoEditar.setEmpaque_110(editar.getEmpaque_110());
        this.activoEditar.setEmpaque_150(editar.getEmpaque_150());
        this.activoEditar.setEmpaque_175(editar.getEmpaque_175());
        this.activoEditar.setEmpaque_200(editar.getEmpaque_200());
        this.activoEditar.setEmpaque_230(editar.getEmpaque_230());
        this.activoEditar.setEmpaque_250(editar.getEmpaque_250());
        this.activoEditar.setSegundas(editar.getSegundas());
        this.activoEditar.setTerceras(editar.getTerceras());
        this.activoEditar.setTorreon(editar.getTorreon());
        this.activoEditar.setVerde_110(editar.getVerde_110());
        this.activoEditar.setVerde_150(editar.getVerde_150());
        this.activoEditar.setVerde_175(editar.getVerde_175());
        this.activoEditar.setVerde_200(editar.getVerde_200());
        this.activoEditar.setVerde_230(editar.getVerde_230());
        this.activoEditar.setVerde_250(editar.getVerde_250());
        this.activoEditar.setVerde_japon(editar.getVerde_japon());
        this.activoEditar.setComprador(editar.getComprador());
        this.activoEditar.setFacturar(editar.getFacturar());
        this.activoEditar.setIdcorrida(editar.getIdcorrida());
        this.activoEditar.setIdcomprador(editar.getIdcomprador());
        this.activoEditar.setIdfacturar(editar.getIdfacturar());
        this.activoEditar.setIdtipo(editar.getIdtipo());
        this.activoEditar.setTipo(editar.getTipo());
        return "pm:registroEditar";
    }
     
    public String cancelar()
    {
        return "pm:lista";
    }
    
    public String imprimir(Integer idCorrida,Integer id)
    {
        System.out.println("imprimir::"+id+ " ;  corrida::"+idCorrida);
        FolioController controllerFolio=new FolioController();
        Folios folio  =  controllerFolio.getOne(id);
        CorridasController corridasController=new CorridasController();
        Corrida corrida  =  corridasController.getOne(id);
        imprimeCorrida(folio,corrida);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "NOTA", 
                "Se mando a imprimir el Folio ["+id+"]"));
        
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "NOTA", 
                "Se mando a imprimir Pallet ["+id+"]"));
        return "pm:lista";
    }
    public void irAmenu()
    {
        try
        {
            context.getExternalContext().redirect("menu.xhtml?faces-redirect=true");
        }
        catch (IOException ex) {
            Logger.getLogger(ActivoMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
        
    public CorridaFolio getActivoSeleccionado() 
    {
        return this.activoSeleccionado;
    }

    public void setActivoSeleccionado(CorridaFolio activoSeleccionado) {
        this.activoSeleccionado = activoSeleccionado;
    }

    public CorridaFolio getActivoEditar() {
        return activoEditar;
    }

    public void setActivoEditar(CorridaFolio activoEditar) {
        this.activoEditar = activoEditar;
    }

    public CorridaFolio getActivoAnexar() {
        return activoAnexar;
    }

    public void setActivoAnexar(CorridaFolio activoAnexar) {
        this.activoAnexar = activoAnexar;
    }

    public CorridaFolio getActivoBorrado() {
        return activoBorrado;
    }

    public void setActivoBorrado(CorridaFolio activoBorrado) {
        this.activoBorrado = activoBorrado;
    }
    public CorridaFolio getActivo() {
        return activo;
    }

    public void setActivo(CorridaFolio activo) {
        this.activo = activo;
    }
    public String guardarCorrida(
        int   id,
        int   idFolio,
        String comprador,
        String facturar,
        String tipo,
        double verde_japon,
        double verde_110,
        double verde_150,
        double verde_175,
        double verde_200,
        double verde_230,
        double verde_250,
        double empaque_110,
        double empaque_150,
        double empaque_175,
        double empaque_200,
        double empaque_230,
        double empaque_250,
        double segundas,
        double terceras,
        double torreon,
        double coleada,int opc )
    {
        CorridasController ctr=new CorridasController();
        ctr.UpdateRecord(
                verde_japon,
                verde_110,
                verde_150,
                verde_175,
                verde_200,
                verde_230,
                verde_250,
                empaque_110,
                empaque_150,
                empaque_175,
                empaque_200,
                empaque_230,
                empaque_250,
                segundas,
                terceras,
                torreon,
                coleada,
                comprador,
                facturar,
                tipo,
                0, 0, 0, id);
        String opcion="";
        switch(opc)
        {
            case 1: opcion= "Módulo de encabezado"; break ;
            case 2: opcion= "Módulo de verdes"; break ;
            case 3: opcion= "Módulo de empaques"; break ;
            case 4: opcion= "Módulo de desechos"; break ;
            case 9: 
                    ctr.UpdateRecord(id,4);
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "NOTA", 
                            "Se Cambio con éxito a cerrado la corrida :"+idFolio));
                    imprimir(0,idFolio);
                    return "pm:lista";
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "NOTA", 
                "Se guardo con éxito el "+opcion+" de la corrida "+idFolio));
        return "pm:lista";
    }
       
    
    public void imprimeCorrida(Folios  folio,Corrida corrida)
    {
        try 
        {
            listarImpresoras();
            ByteArrayOutputStream documentoBytes=null;
            documentoBytes= crearDocumentoiText( folio,corrida);
            imprimir(documentoBytes);
        }
        catch (IOException | PrinterException ex) 
        {
            ex.printStackTrace();
        }
        
    }
    private  void imprimir(ByteArrayOutputStream documentoBytes) throws IOException, PrinterException
    {
        ByteArrayInputStream bais = new ByteArrayInputStream(documentoBytes.toByteArray());
        PDDocument document = PDDocument.load(bais);
        PrintService myPrintService = findPrintService(firstImpresora());
        PrinterJob printerJob = PrinterJob.getPrinterJob();
        printerJob.setPageable(new PDFPageable(document));
        printerJob.setPrintService(myPrintService);
        printerJob.print();

    }
    private PrintService findPrintService(String printerName) {
        PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, null);
        for (PrintService printService : printServices) {
            System.out.println(printService.getName());

            if (printService.getName().trim().equals(printerName)) {
                return printService;
            }
        }
        return null;
    }
    private String  firstImpresora() {
        PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, null);
        System.out.println("Lista de impresoras disponibles");
        for (PrintService printService : printServices) {
            return printService.getName();
        }
        return ""; 
    }
    
    
    private Table creaTabla1(
        Image img1,
        String productor,
        String folio,
        String comprador,
        String direccion,
        String facturar,
        String agronomo,
        String tipo) 
    { 
        Table table0 = new Table(columnWidths24);
        
        Table table1 = new Table(columnWidths4);
        
        table1.addCell("Productor:");
        table1.addCell(productor);
        table1.addCell("Folio:"); 
        table1.addCell(folio);
        
        table1.addCell("Comprador:");
        table1.addCell(comprador);
        table1.addCell(""); 
        table1.addCell("");
        
        table1.addCell("Facturar a:");
        table1.addCell(facturar); 
        table1.addCell("");
        table1.addCell(direccion);
        
        table1.addCell("Agrónomo:");  
        table1.addCell(agronomo);
        table1.addCell("Tipo:");
        table1.addCell(tipo);
        table1.setFontSize(7f); 
        
        table0.addCell(img1);
        table0.addCell(table1);
        
        return table0;
    }
    private Table creaTabla2() 
    { 
        Table table1 = new Table(columnWidths6);
        table1.setFontSize(8f); 
        table1.addCell("");
        table1.addCell("CALIBRE");
        table1.addCell("CANTIDAD (KG"); 
        table1.addCell("PORCENTAJE (%)"); 
        table1.addCell("PRECIO ($)");
        table1.addCell("TOTAL ($)");
        return table1;
    }
    private Table creaTabla3(Corrida c ,  Precios precios) 
    { 
        Table table0 = new Table(columnWidths15);
        
        Table table1 = new Table(columnWidths5);
        table1.setFontSize(9f);
        table0.setFontSize(8f);
        table1.addCell("Japon");
        table1.addCell(formato(new Double(c.getVerde_japon())).toString());
        table1.addCell(formato(c.getVerde_japon()/_total*100));
        table1.addCell(formato(precios.getVerde_japon()));
        table1.addCell(formato(c.getVerde_japon() *  precios.getVerde_japon()));
        
        table1.addCell("110");
        table1.addCell(formato(new Double(c.getVerde_110())).toString());
        table1.addCell(formato(c.getVerde_110()/_total*100));
        table1.addCell(formato(precios.getVerde_110()));
        table1.addCell(formato(c.getVerde_110() *  precios.getVerde_110()));
        
        table1.addCell("150");
        table1.addCell(formato(new Double(c.getVerde_150())).toString());
        table1.addCell(formato(c.getVerde_150()/_total*100));
        table1.addCell(formato(precios.getVerde_150()));
        table1.addCell(formato(c.getVerde_150() *  precios.getVerde_150()));
        
        table1.addCell("175");
        table1.addCell(formato(new Double(c.getVerde_175())).toString());
        table1.addCell(formato(c.getVerde_175()/_total*100));
        table1.addCell(formato(precios.getVerde_175()));
        table1.addCell(formato(c.getVerde_175() *  precios.getVerde_175()));
        
        table1.addCell("200");
        table1.addCell(formato(new Double(c.getVerde_200())).toString());
        table1.addCell(formato(c.getVerde_200()/_total*100));
        table1.addCell(formato(precios.getVerde_200()));
        table1.addCell(formato(c.getVerde_200() *  precios.getVerde_200()));
        
        table1.addCell("230");
        table1.addCell(formato(new Double(c.getVerde_230())).toString());
        table1.addCell(formato(c.getVerde_230()/_total*100));
        table1.addCell(formato(precios.getVerde_230()));
        table1.addCell(formato(c.getVerde_230() *  precios.getVerde_230()));
        
        table1.addCell("250");
        table1.addCell(formato(new Double(c.getVerde_250())).toString());
        table1.addCell(formato(c.getVerde_250()/_total*100));
        table1.addCell(formato(precios.getVerde_250()));
        table1.addCell(formato(c.getVerde_250() *  precios.getVerde_250()));
        
        table1.addCell("SUMA");
        table1.addCell(""+verdes_cantidad);
        table1.addCell(""+formato(verdes_porcentaje*100));
        table1.addCell("");
        
        verdes_total =(c.getVerde_japon() *  precios.getVerde_japon())+
                (c.getVerde_110() *  precios.getVerde_110())+
                (c.getVerde_150() *  precios.getVerde_150())+
                (c.getVerde_175() *  precios.getVerde_175())+
                (c.getVerde_200() *  precios.getVerde_200())+
                (c.getVerde_230() *  precios.getVerde_230())+
                (c.getVerde_250() *  precios.getVerde_250());
        
        table1.addCell(formato(verdes_total));
        
        table0.addCell("VERDES");
        table0.addCell(table1);
        
        return table0;
    }
    
    private Table creaTabla4(Corrida c,Precios precios) 
    { 
        Table table0 = new Table(columnWidths15);
        Table table1 = new Table(columnWidths5);
        table1.setFontSize(9f);
        table0.setFontSize(8f);
        table1.addCell("110");
        table1.addCell(formato(new Double(c.getEmpaque_110())).toString());
        table1.addCell(formato(c.getEmpaque_110()/_total*100));
        table1.addCell(formato(precios.getEmpaque_110()));
        table1.addCell(formato(c.getEmpaque_110() *  precios.getEmpaque_110()));
        
        table1.addCell("150");
        table1.addCell(formato(new Double(c.getEmpaque_150())).toString());
        table1.addCell(formato(c.getEmpaque_150()/_total*100));
        table1.addCell(formato(precios.getEmpaque_150()));
        table1.addCell(formato(c.getEmpaque_150() *  precios.getEmpaque_150()));
        
        table1.addCell("175");
        table1.addCell(formato(new Double(c.getEmpaque_175())).toString());
        table1.addCell(formato(c.getEmpaque_175()/_total*100));
        table1.addCell(formato(precios.getEmpaque_175()));
        table1.addCell(formato(c.getEmpaque_175() *  precios.getEmpaque_175()));
        
        table1.addCell("200");
        table1.addCell(formato(new Double(c.getEmpaque_200())).toString());
        table1.addCell(formato(c.getEmpaque_200()/_total*100));
        table1.addCell(formato(precios.getEmpaque_200()));
        table1.addCell(formato(c.getEmpaque_200() *  precios.getEmpaque_200()));
        
        table1.addCell("230");
        table1.addCell(formato(new Double(c.getEmpaque_230())).toString());
        table1.addCell(formato(c.getEmpaque_230()/_total*100));
        table1.addCell(formato(precios.getEmpaque_230()));
        table1.addCell(formato(c.getEmpaque_230() *  precios.getEmpaque_230()));
        
        table1.addCell("250");
        table1.addCell(formato(new Double(c.getEmpaque_250())).toString());
        table1.addCell(formato(c.getEmpaque_250()/_total*100));
        table1.addCell(formato(precios.getEmpaque_250()));
        table1.addCell(formato(c.getEmpaque_250() *  precios.getEmpaque_250()));
        
        table1.addCell("SUMA");
        table1.addCell(""+empaques_cantidad);
        table1.addCell(""+formato(empaques_porcentaje*100));
        table1.addCell("");
        
        empaques_total =
                (c.getEmpaque_110() *  precios.getEmpaque_110())+
                (c.getEmpaque_150() *  precios.getEmpaque_150())+
                (c.getEmpaque_175() *  precios.getEmpaque_175())+
                (c.getEmpaque_200() *  precios.getEmpaque_200())+
                (c.getEmpaque_230() *  precios.getEmpaque_230())+
                (c.getEmpaque_250() *  precios.getEmpaque_250());
        
        table1.addCell(formato(empaques_total));
        table0.addCell("EMPAQUES");
        table0.addCell(table1);

        return table0;
    }
    
    private Table creaTabla5(Corrida c,Precios precios) 
    { 
        Table table0 = new Table(columnWidths15);
        Table table1 = new Table(columnWidths5);
        table1.setFontSize(9f);
        table0.setFontSize(8f);
        table1.addCell("110");
        table1.addCell(formato(new Double(c.getSegundas())).toString());
        table1.addCell(formato(c.getSegundas()/_total*100));
        table1.addCell(formato(precios.getSegundas()));
        table1.addCell(formato(c.getSegundas() *  precios.getSegundas()));
        
        table1.addCell("150");
        table1.addCell(formato(new Double(c.getTerceras())).toString());
        table1.addCell(formato(c.getTerceras()/_total*100));
        table1.addCell(formato(precios.getTerceras()));
        table1.addCell(formato(c.getTerceras() *  precios.getTerceras()));
        
        table1.addCell("175");
        table1.addCell(formato(new Double(c.getTorreon())).toString());
        table1.addCell(formato(c.getTorreon()/_total*100));
        table1.addCell(formato(precios.getTorreon()));
        table1.addCell(formato(c.getTorreon() *  precios.getTorreon()));
        
        table1.addCell("200");
        table1.addCell(formato(new Double(c.getColeada())).toString());
        table1.addCell(formato(c.getColeada()/_total*100));
        table1.addCell(formato(precios.getColeada()));
        table1.addCell(formato(c.getColeada() *  precios.getColeada()));
        
        table1.addCell("SUMA");
        table1.addCell(""+desechos_cantidad);
        table1.addCell(""+formato(desechos_porcentaje*100));
        table1.addCell("");
        desechos_total =
                (c.getSegundas() *  precios.getSegundas())+
                (c.getTerceras() *  precios.getTerceras())+
                (c.getTorreon() *  precios.getTorreon())+
                (c.getColeada() *  precios.getColeada());
        table1.addCell(formato(desechos_total));
        
        table0.addCell("DESECHOS");
        table0.addCell(table1);
        
        return table0;
    }
    private Table creaTabla6_1() 
    { 
        Table table1 = new Table(columnWidths2);
        table1.setBorder(Border.NO_BORDER);
        table1.setFontSize(9f);
        table1.addCell(" ");
        table1.addCell(" ");
        return table1;
    }
    private Table creaTabla6_2(
        double campo1,
        double campo2,
        double campo3,
        double campo4) 
    { 
        Table tableVacio = creaTabla6_1();
        Table table1 = new Table(columnWidths004);
        Table table2 = new Table(columnWidths024);
        table2.setBorder(Border.NO_BORDER);
        table1.setFontSize(9f);
        table1.addCell(formato(verdes_cantidad + empaques_cantidad + desechos_cantidad));
        table1.addCell(formato((verdes_porcentaje + empaques_porcentaje + desechos_porcentaje)*100));
        table1.addCell("");
        table1.addCell(formato(verdes_total + empaques_total + desechos_total));
        
        table2.addCell(" ");
        table2.addCell(table1);
        return table2;
    }
    
    private Table creaTabla6_3(
        String etiqueta,
        double valor) 
    { 
        Table table1 = new Table(columnWidths1);
        table1.setBorder(Border.NO_BORDER);
        table1.setFontSize(9f);
        table1.addCell(etiqueta);
        
        Table table2 = new Table(columnWidths1);
        table2.setFontSize(9f);
        table2.addCell((new Double(valor)).toString());
        
        Table table3 = new Table(columnWidths1);
        table3.addCell(table1);
        table3.addCell(table2);
        
        return table1;
    }
    private Table creaTable7(Table t1,Table t2,Table t3)
    {
        Table table1 = new Table(columnWidths003);
        table1.addCell(t1);
        table1.addCell(t2);
        table1.addCell(t3);
        return table1;
    }
    private Table creaTable7(
            double d1,
            double d2)
    {
        Table table1 = new Table(columnWidths6);
        table1.setFontSize(8f); 
        table1.addCell("Kilos Ticket :");
        table1.addCell(""+formato(d1));
        table1.addCell(""); 
        table1.addCell(""); 
        table1.addCell("Total :");
        table1.addCell(""+formato(d2));
        return table1;
    }
    private Table creaTable8(double d1)
    {
        Table table1 = new Table(columnWidths6);
        table1.setFontSize(8f); 
        table1.addCell("Folio Ticket:");
        table1.addCell(""+d1);
        table1.addCell(""); 
        table1.addCell(""); 
        table1.addCell(" ");
        table1.addCell("");
        return table1;
    }
    
    private Table creaTable9(String calibrador)
    {
        Table table1 = new Table(columnWidths007);
        table1.setBorder(Border.NO_BORDER);
        table1.setFontSize(9f);
        table1.addCell("Elaboró:");
        table1.addCell(calibrador);
        return table1;
    }
    private ByteArrayOutputStream crearDocumentoiText(Folios  folio,Corrida corrida) 
    {
        ByteArrayOutputStream documentoBytes = null;
        try 
        {
            documentoBytes = new ByteArrayOutputStream();
            PdfWriter pdfWriter = new PdfWriter(documentoBytes);
            PdfDocument pdfDoc = new PdfDocument(pdfWriter);
            Document documento = new Document(pdfDoc, PageSize.LETTER); 
            ServletContext ctx = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext(); 
            String realPath = ctx.getRealPath("/");
            String imageFiless = realPath+"citricos.png"; 
            ImageData imagen2=ImageDataFactory.create(getImageQRImage(folio.getFolio(),"png",130,130)); 
            
            ImageData imagen1 = ImageDataFactory.create(imageFiless);
            Image img1=new Image(imagen1);
            img1.setHeight(80);
            img1.setWidth(100);
            Image img2=new Image(imagen2);
            img2.setHeight(130);
            img2.setWidth(130);
            Date date = new Date();
            int day =date.getDay();
            int dateX =  date.getDate();
            int hours =date.getHours();
            int minutes =date.getMinutes();
            int month =  date.getMonth() + 1;
            int year =  date.getYear() +1900;
            Paragraph par0 = new Paragraph(dia(day)+", "+dateX+" de "+mes(month)+" de "+year+" "+hours+":"+minutes);
            par0.setFontSize(8f);
            par0.setTextAlignment(TextAlignment.RIGHT);
            documento.add(par0);
            Paragraph par1 = new Paragraph("REPORTE DE CORRIDA");
            par1.setFontSize(11f);
            par1.setTextAlignment(TextAlignment.CENTER);
            documento.add(par1);
            documento.add(new Paragraph(" "));
            documento.add(creaTabla1(
                            img1,
                            folio.getProductor(),
                            folio.getFolio(),
                            corrida.getComprador(),
                            "--------",
                            corrida.getFacturar(),
                            folio.getAgronomo(),
                            corrida.getTipo() ));
        
            documento.add(new Paragraph(" "));
            
            
            documento.add(creaTabla2());
            documento.add(new Paragraph(" "));
            verdes_cantidad=format(corrida.getVerde_japon()+corrida.getVerde_110()+corrida.getVerde_150()+corrida.getVerde_175()+corrida.getVerde_200()+corrida.getVerde_230()+corrida.getVerde_250());
            empaques_cantidad=format(corrida.getEmpaque_110()+corrida.getEmpaque_150()+corrida.getEmpaque_175()+corrida.getEmpaque_200()+corrida.getEmpaque_230()+corrida.getEmpaque_250());
            desechos_cantidad=format(corrida.getSegundas()+corrida.getTerceras()+corrida.getTorreon()+corrida.getColeada());
            _total =  format(verdes_cantidad + empaques_cantidad +  desechos_cantidad);
            
            verdes_porcentaje=format(verdes_cantidad/_total);
            empaques_porcentaje=format(empaques_cantidad/_total);
            desechos_porcentaje=format(desechos_cantidad/_total);
            
            PreciosController controller=new PreciosController();
            Precios precios = null;
            if(folio.getEstatus()!=4)
                precios = new Precios();
            else
                precios = controller.getMax();
                
            documento.add(creaTabla3(corrida ,  precios));
            documento.add(new Paragraph(" "));
            
            documento.add(creaTabla4(corrida ,  precios));
            documento.add(new Paragraph(" "));
            
            documento.add(creaTabla5(corrida ,  precios));
            
            documento.add(new Paragraph(" "));
            documento.add(creaTabla6_2(1,2,3,4));
            
            documento.add(new Paragraph(" "));
            documento.add(creaTabla6_2(5,6,7,8));

            documento.add(new Paragraph(" "));
            
            double kilos = folio.getPeso_neto();
            double total =0.00;
            documento.add(creaTable7(kilos,total));
            
            documento.add(new Paragraph(" "));
            documento.add(creaTable8(11));
            
            documento.add(new Paragraph(" "));
            documento.add(creaTable9("Luis Blancas"));
            
            documento.close();
        } 
        catch (MalformedURLException ex) 
        {
            Logger.getLogger(FolioMB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return documentoBytes;
    }
    private static byte[] getImageQRImage(String text, String ext, int width, int height) 
    {
    	byte[] pngData = null;
        try {
                QRCodeWriter qrCodeWriter = new QRCodeWriter();
            BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, width, height);
            ByteArrayOutputStream pngOutputStream = new ByteArrayOutputStream();
            MatrixToImageWriter.writeToStream(bitMatrix, ext, pngOutputStream);
            pngData = pngOutputStream.toByteArray();  
        } catch(WriterException wex) {
                System.out.println(wex.getMessage());
        } catch(IOException ioe) {
                System.out.println(ioe.getMessage());
        } 
         return pngData;
    } 
    private void listarImpresoras() {
        PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, null);
        System.out.println("Lista de impresoras disponibles");

        for (PrintService printService : printServices) {
            System.out.println("\t" + printService.getName());
        }
    }

    private String dia(int day) 
    {
        switch (day)
        {
            case 0: return "Domingo";
            case 1: return "Lunes";
            case 2: return "Martes";
            case 3: return "Miercoles";
            case 4: return "Jueves";
            case 5: return "Viernes";
            case 6: return "Sabado";
        }
        return "Domingo";
    }

    private String mes(int month) 
    {
        switch (month)
        {
            case 1: return "Enero";
            case 2: return "Febrero";
            case 3: return "Marzo";
            case 4: return "Abril";
            case 5: return "Mayo";
            case 6: return "Junio";
            case 7: return "Julio";
            case 8: return "Agosto";
            case 9: return "Septiembre";
            case 10: return "Octubre";
            case 11: return "Noviembre";
            case 12: return "Diciembre";
        }
        return "Domingo";
    }

    private double format(double number) 
    {
        BigDecimal bd = new BigDecimal(""+number);
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
    private String formato(double number) 
    {
        DecimalFormat formato = new DecimalFormat("#,###.00");
        BigDecimal bd = new BigDecimal(""+number);
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        return ""+ formato.format(bd.doubleValue());
    }
}
