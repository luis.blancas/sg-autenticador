/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.org.citricos.mb; 
import java.io.IOException;
import java.io.Serializable;
import java.util.List; 
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct; 
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext; 
import mx.org.citicos.controller.CDIController; 
import mx.org.citricos.entity.Cdi; 

/**
 *
 * @author luis Adrian
 */ 
@ManagedBean (name = "cdiMB")
@RequestScoped
public class CdiMB implements Serializable
{
    private SessionMB  sessionMB =new SessionMB();
    CDIController controller=new CDIController();
    private final FacesContext context = FacesContext.getCurrentInstance();
    private List<Cdi> marcas; 
    private Cdi marcaSeleccionado;
    private Cdi marcaEditar;
    private Cdi marcaAnexar;
    private Cdi marcaBorrado; 
    
    private Integer id;
    private String  descripcion; 
    
    @PostConstruct
    public void init() {
        marcaSeleccionado=new Cdi();
        this.marcas=controller.getAll();
    }
    public List<Cdi> getCalidads() {
        
        this.marcas= controller.getAll();
        return marcas;
    } 
    public void seleccionEditar(Cdi editar )
    {
        this.marcaEditar=new Cdi();
        this.marcaEditar.setId(editar.getId()); 
        this.marcaEditar.setNombre(editar.getNombre()); 
        System.out.println("seleccion Editar "+editar.toString());
    }
    public void seleccionBorrado(Cdi borrado )
    {
        this.marcaBorrado=new Cdi();
        this.marcaBorrado.setId(borrado.getId());
        this.marcaBorrado.setNombre(borrado.getNombre());
        System.out.println("seleccionBorrado "+borrado.toString());
    } 
    public void editarmarca(Integer id,Cdi o)
    {
        try
        { 
            System.out.println("Editar id    :"+id);
            System.out.println("Editar Nombre:"+o.getNombre());
            controller.updateRecordUsr(o.getNombre(),id,sessionMB.getUsuariofinal().getId());
            context.getExternalContext().redirect("Listarmarca.xhtml?faces-redirect=true");
        }
        catch (IOException ex) {
            Logger.getLogger(CdiMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void anexarmarca()
    {
        try
        { 
            System.out.println("Anexar Nombre:"+this.descripcion);
            controller.insertRecordUsr(descripcion,sessionMB.getUsuariofinal().getId());
            context.getExternalContext().redirect("Listarmarca.xhtml?faces-redirect=true");
        }
        catch (IOException ex) 
        {
            Logger.getLogger(CdiMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void borrarmarca(Cdi marcaAborrar)
    {
        try
        { 
            controller.deleteRecord(marcaAborrar.getId(),sessionMB.getUsuariofinal().getId());
            System.out.println("seleccion Borrar "+marcaAborrar.toString()); 
            this.marcas= controller.getAll();
            context.getExternalContext().redirect("Listarmarca.xhtml?faces-redirect=true");
            context.addMessage(null, new FacesMessage("Borrar", "Registro  borrado con exito :  "+marcaAborrar.getNombre()));
        }
        catch (IOException ex) {
            Logger.getLogger(CdiMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /*-------------------------------------------------------------------------------------*/

    public CDIController getController() {
        return controller;
    }

    public void setController(CDIController controller) {
        this.controller = controller;
    }

    public List<Cdi> getMarcas() {
        return marcas;
    }

    public void setMarcas(List<Cdi> marcas) {
        this.marcas = marcas;
    }

    public Cdi getMarcaSeleccionado() {
        return marcaSeleccionado;
    }

    public void setMarcaSeleccionado(Cdi marcaSeleccionado) {
        this.marcaSeleccionado = marcaSeleccionado;
    }

    public Cdi getMarcaEditar() {
        return marcaEditar;
    }

    public void setMarcaEditar(Cdi marcaEditar) {
        this.marcaEditar = marcaEditar;
    }

    public Cdi getMarcaAnexar() {
        return marcaAnexar;
    }

    public void setMarcaAnexar(Cdi marcaAnexar) {
        this.marcaAnexar = marcaAnexar;
    }

    public Cdi getMarcaBorrado() {
        return marcaBorrado;
    }

    public void setMarcaBorrado(Cdi marcaBorrado) {
        this.marcaBorrado = marcaBorrado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
 
}
