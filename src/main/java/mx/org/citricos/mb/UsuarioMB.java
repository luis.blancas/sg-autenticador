/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.org.citricos.mb; 
import java.io.IOException;
import java.util.List; 
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct; 
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import mx.org.citicos.controller.ActivoController;
import mx.org.citicos.controller.PerfilController;
import mx.org.citicos.controller.UsuarioController;
import mx.org.citricos.entity.Activo;
import mx.org.citricos.entity.Perfil;
import mx.org.citricos.entity.Usuario;

/**
 *
 * @author luisa
 */ 
@ManagedBean (name = "usuarioMB")
@RequestScoped
public class UsuarioMB
{
    private final FacesContext context = FacesContext.getCurrentInstance();
    private List<Usuario> usuarios; 
    private Usuario usuarioSeleccionado;
    private Usuario usuarioEditar;
    private Usuario usuarioAnexar;
    private Usuario usuarioResetear;
    private Usuario usuarioBorrado;
    private List<Activo> activos; 
    private List<Perfil> perfiles;
    
    private Integer id;
    private String  nombre;
    private String  apellidos;
    private String  login;
    private String  password;
    private Integer perfil;
    private Integer activo;
    
    private Integer reset_id;
    private Integer reset_password1;
    private Integer reset_password2;
    
    @PostConstruct
    public void init() {
        usuarioSeleccionado=new Usuario();
        perfil=1;
        activo=1;
    }
    public List<Usuario> getUsuarios() {
        UsuarioController controller=new UsuarioController();
        this.usuarios= controller.getll();
        return usuarios;
    }
    public void seleccionResetear(Usuario reset )
    {
        this.usuarioResetear=new Usuario();
        this.usuarioResetear.setId(reset.getId());
        this.reset_id=reset.getId();
        System.out.println("seleccion Resetear "+reset.toString());
    }
    public void seleccionEditar(Usuario editar )
    {
        this.usuarioEditar=new Usuario();
        this.reset_id =editar.getId();
        this.usuarioEditar.setId(editar.getId());
        this.usuarioEditar.setPerfil(editar.getPerfil());
        this.usuarioEditar.setActivo(editar.getActivo());
        this.usuarioEditar.setLogin(editar.getLogin());
        this.usuarioEditar.setNombre(editar.getNombre());
        this.usuarioEditar.setPerfilstr(editar.getPerfilstr());
        
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getSessionMap().put("reset_id", this.reset_id);
            
        System.out.println("seleccion Editar "+editar.toString());
    }
    public void seleccionBorrado(Usuario borrado )
    {
        this.usuarioBorrado=new Usuario();
        this.usuarioBorrado.setId(borrado.getId());
        this.usuarioBorrado.setPerfil(borrado.getPerfil());
        this.usuarioBorrado.setActivo(borrado.getActivo());
        this.usuarioBorrado.setLogin(borrado.getLogin());
        this.usuarioBorrado.setNombre(borrado.getNombre());
        this.usuarioBorrado.setPerfilstr(borrado.getPerfilstr());
        System.out.println("seleccionBorrado "+borrado.toString());
    } 
    public void editarUsuario(Integer id,Usuario o)
    {
        try
        {
            UsuarioController controller=new UsuarioController();
            System.out.println("Editar id    :"+id);
            System.out.println("Editar Nombre:"+o.getNombre());
            System.out.println("Editar Apelli:"+o.getApellidos());
            System.out.println("Editar Login :"+o.getLogin());
            System.out.println("Editar Passwo:"+o.getPassword());
            System.out.println("Editar Perfil:"+o.getPerfil());
            System.out.println("Editar Activo:"+o.getActivo());
            controller.modificaUsuario(id,o.getNombre(),o.getApellidos(),o.getLogin(),o.getPerfil(),o.getActivo());
            context.getExternalContext().redirect("ListaUsuarios.xhtml?faces-redirect=true");
        }
        catch (IOException ex) {
            Logger.getLogger(ActivoMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void reset(String p1,String p2)
    {
        System.out.println("Editar p1:"+p1);
        System.out.println("Editar p2:"+p2);
    }
    public void resetPassword(Integer id,String pas1,String pas2)
    {
        try
        {
            UsuarioController controller=new UsuarioController();
            System.out.println("Editar Id    :"+id);
            System.out.println("Editar Nombre:"+pas1);
            System.out.println("Editar Apelli:"+pas2);
            if(!(pas1.toUpperCase().equals(pas2.toUpperCase())))
                context.addMessage(null, new FacesMessage("Resetear", "El password debe ser el mismo en los dos campos"));
            {
                int res=controller.resetPassword(id,pas1);
                context.getExternalContext().redirect("ListaUsuarios.xhtml?faces-redirect=true");
            }
        }
        catch (IOException ex) {
            Logger.getLogger(ActivoMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void AnexarUsuario()
    {
        try
        {
            UsuarioController controller=new UsuarioController();
            System.out.println("Editar Nombre:"+this.nombre);
            System.out.println("Editar Apelli:"+this.apellidos);
            System.out.println("Editar Login :"+this.login);
            System.out.println("Editar Passwo:"+this.password);
            System.out.println("Editar Perfil:"+this.perfil);
            System.out.println("Editar Activo:"+this.activo);
            controller.creaUsuario(nombre,apellidos,login,password,perfil,activo);
            context.getExternalContext().redirect("ListaUsuarios.xhtml?faces-redirect=true");
        }
        catch (IOException ex) 
        {
            Logger.getLogger(ActivoMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void borrarUsuario(Usuario usuarioAborrar)
    {
        try
        {
            UsuarioController controller=new UsuarioController();
            controller.borrarUsuario(usuarioAborrar.getId().intValue());
            System.out.println("seleccion Borrar "+usuarioAborrar.toString()); 
            this.usuarios= controller.getll();
            context.getExternalContext().redirect("ListaUsuarios.xhtml?faces-redirect=true");
            context.addMessage(null, new FacesMessage("Borrar", "Registro  borrado con exito :  "+usuarioAborrar.getNombre()+ " " +usuarioAborrar.getApellidos()));
        }
        catch (IOException ex) {
            Logger.getLogger(ActivoMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Usuario getUsuarioResetear() {
        return usuarioResetear;
    }

    public void setUsuarioResetear(Usuario usuarioResetear) {
        this.usuarioResetear = usuarioResetear;
    }
    
    public Usuario getUsuarioSeleccionado() {
        return usuarioSeleccionado;
    }

    public void setUsuarioSeleccionado(Usuario usuarioSeleccionado) {
        this.usuarioSeleccionado = usuarioSeleccionado;
    }

    public Usuario getUsuarioEditar() {
        return usuarioEditar;
    }

    public void setUsuarioEditar(Usuario usuarioEditar) {
        this.usuarioEditar = usuarioEditar;
    }

    public Usuario getUsuarioBorrado() {
        return usuarioBorrado;
    }

    public void setUsuarioBorrado(Usuario usuarioBorrado) {
        this.usuarioBorrado = usuarioBorrado;
    }
 
    public List<Activo> getActivos() {
        ActivoController activoCTR=new ActivoController(); 
        this.activos= activoCTR.getAll();
        return this.activos;
    }

    public void setActivos(List<Activo> activos) {
        this.activos = activos;
    }

    public List<Perfil> getPerfiles() {
        PerfilController perfilCTR=new PerfilController();
        this.perfiles= perfilCTR.getAllActivo();
        return this.perfiles;
    }

    public void setPerfiles(List<Perfil> perfiles) {
        this.perfiles = perfiles;
    }

    public Usuario getUsuarioAnexar() { 
        return usuarioAnexar;
    }

    public void setUsuarioAnexar(Usuario usuarioAnexar) {
        this.usuarioAnexar = usuarioAnexar;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Integer getPerfil() {
        return perfil;
    }

    public void setPerfil(Integer perfil) {
        this.perfil = perfil;
    }

    public Integer getActivo() {
        return activo;
    }

    public void setActivo(Integer activo) {
        this.activo = activo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getReset_id() {
        FacesContext context = FacesContext.getCurrentInstance();
        reset_id =  (Integer) context.getExternalContext().getSessionMap().get("reset_id");
        return reset_id;
    }

    public void setReset_id(Integer reset_id) {
        this.reset_id = reset_id;
    }

    public Integer getReset_password1() {
        return reset_password1;
    }

    public void setReset_password1(Integer reset_password1) {
        this.reset_password1 = reset_password1;
    }

    public Integer getReset_password2() {
        return reset_password2;
    }

    public void setReset_password2(Integer reset_password2) {
        this.reset_password2 = reset_password2;
    }

    
}
