/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.org.citricos.mb; 
import java.io.IOException;
import java.io.Serializable;
import java.util.List; 
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct; 
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean; 
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext; 
import mx.org.citicos.controller.CalidadController; 
import mx.org.citricos.entity.Calidad; 

/**
 *
 * @author luis Adrian
 */ 
@ManagedBean (name = "calidadMB")
@RequestScoped
public class CalidadMB implements Serializable
{
    private SessionMB  sessionMB =new SessionMB();
    CalidadController controller=new CalidadController();
    private final FacesContext context = FacesContext.getCurrentInstance();
    private List<Calidad> calidads; 
    private Calidad calidadSeleccionado;
    private Calidad calidadEditar;
    private Calidad calidadAnexar;
    private Calidad calidadBorrado; 
    
    private Integer id;
    private String  descripcion; 
    
    @PostConstruct
    public void init() {
        calidadSeleccionado=new Calidad(); 
    }
    public List<Calidad> getCalidads() {
        
        this.calidads= controller.getAll();
        return calidads;
    } 
    public void seleccionEditar(Calidad editar )
    {
        this.calidadEditar=new Calidad();
        this.calidadEditar.setId(editar.getId()); 
        this.calidadEditar.setNombre(editar.getNombre()); 
        System.out.println("seleccion Editar "+editar.toString());
    }
    public void seleccionBorrado(Calidad borrado )
    {
        this.calidadBorrado=new Calidad();
        this.calidadBorrado.setId(borrado.getId());
        this.calidadBorrado.setNombre(borrado.getNombre());
        System.out.println("seleccionBorrado "+borrado.toString());
    } 
    public void editarcalidad(Integer id,Calidad o)
    {
        try
        { 
            System.out.println("Editar id    :"+id);
            System.out.println("Editar Nombre:"+o.getNombre());
            controller.updateRecordUsr(o.getNombre(),id,sessionMB.getUsuariofinal().getId());
            context.getExternalContext().redirect("Listarcalidad.xhtml?faces-redirect=true");
        }
        catch (IOException ex) {
            Logger.getLogger(CalidadMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void anexarcalidad()
    {
        try
        { 
            System.out.println("Anexar Nombre:"+this.descripcion);
            controller.insertRecordUsr(descripcion,(sessionMB.getUsuariofinal().getId()));
            context.getExternalContext().redirect("Listarcalidad.xhtml?faces-redirect=true");
        }
        catch (IOException ex) 
        {
            Logger.getLogger(CalidadMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void borrarcalidad(Calidad calidadAborrar)
    {
        try
        { 
            controller.deleteRecord(calidadAborrar.getId(),sessionMB.getUsuariofinal().getId());
            System.out.println("seleccion Borrar "+calidadAborrar.toString()); 
            this.calidads= controller.getAll();
            context.getExternalContext().redirect("Listarcalidad.xhtml?faces-redirect=true");
            context.addMessage(null, new FacesMessage("Borrar", "Registro  borrado con exito :  "+calidadAborrar.getNombre()));
        }
        catch (IOException ex) {
            Logger.getLogger(CalidadMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /*-------------------------------------------------------------------------------------*/

    public CalidadController getController() {
        return controller;
    }

    public void setController(CalidadController controller) {
        this.controller = controller;
    }

    public Calidad getCalidadSeleccionado() {
        return calidadSeleccionado;
    }

    public void setCalidadSeleccionado(Calidad calidadSeleccionado) {
        this.calidadSeleccionado = calidadSeleccionado;
    }

    public Calidad getCalidadEditar() {
        return calidadEditar;
    }

    public void setCalidadEditar(Calidad calidadEditar) {
        this.calidadEditar = calidadEditar;
    }

    public Calidad getCalidadAnexar() {
        return calidadAnexar;
    }

    public void setCalidadAnexar(Calidad calidadAnexar) {
        this.calidadAnexar = calidadAnexar;
    }

    public Calidad getCalidadBorrado() {
        return calidadBorrado;
    }

    public void setCalidadBorrado(Calidad calidadBorrado) {
        this.calidadBorrado = calidadBorrado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
}
