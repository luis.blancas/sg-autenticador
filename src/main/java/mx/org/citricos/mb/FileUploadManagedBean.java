/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.org.citricos.mb;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import org.primefaces.event.FileUploadEvent;

@ManagedBean(name = "FileUploadManagedBean")
public class FileUploadManagedBean
{
    private String destination = "C:\\citricos\\tmp\\";
    private String nombre;
    public void upload(FileUploadEvent event) 
    {
        try 
            {
            copyFile(event.getFile().getFileName(), event.getFile().getInputstream());
            FacesMessage message = new FacesMessage("El archivo se ha subido con éxito!");
            FacesContext.getCurrentInstance().addMessage(null, message);
        } 
        catch (IOException e) 
        {
            e.printStackTrace();
        }

    }
    public void copyFile(String fileName, InputStream in) 
    {
        try 
        {
            this.nombre=destination + fileName;
            OutputStream out = new FileOutputStream(new File(destination + fileName));
            int read = 0;
            byte[] bytes = new byte[1024];
            while ((read = in.read(bytes)) != -1) 
            {
                out.write(bytes, 0, read);
            }
            in.close();
            out.flush();
            out.close();
            System.out.println("El archivo se ha creado con éxito!");

            DateFormat dateFormat = new SimpleDateFormat("yyyy–MM–dd HH_mm_ss");
            Date date = new Date();
            String ruta1 = destination + fileName;
            String ruta2 = destination + dateFormat.format(date)+"–"+fileName;
            System.out.println("Archivo: "+ruta1+" Renombrado a: "+ruta2);
            File archivo=new File(ruta1);
            archivo.renameTo(new File(ruta2));
        } 
        catch (IOException e) 
        {
            System.out.println(e.getMessage());
        }
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
}
