/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.org.citricos.mb;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import mx.org.citicos.controller.PalletController;
import mx.org.citicos.controller.PalletDescController;
import mx.org.citricos.entity.Pallet;
import mx.org.citricos.entity.Pallet_desc; 

/**
 *
 * @author luisa
 */
@ManagedBean(name="detalleeditMB")
@ViewScoped
public class DetalleEditMB implements Serializable
{
    private Pallet_desc editar;
    private Pallet pallet; 

    public Pallet getPallet() {
        return pallet;
    }

    public void setPallet(Pallet pallet) {
        this.pallet = pallet;
    }
    public String  seleccionPalletDetalleD(Pallet_desc editar,String tarima,String marca)
    {/*
        this.editar= new Pallet_desc();
        this.editar.setMarca(marca);
        this.editar.setTarima(tarima); 
        this.editar.setId(editar.getId());
        this.editar.setActivo(editar.getActivo());
        this.editar.setCajas(editar.getCajas());
        this.editar.setCalibre(editar.getCalibre());
        this.editar.setCalidad(editar.getCalidad());
        this.editar.setCdi(editar.getCdi());
        this.editar.setIdactivo(editar.getIdactivo());
        this.editar.setIdcalidad(editar.getIdcalidad());
        this.editar.setIdtarima(editar.getIdtarima());
        this.editar.setIdx(editar.getIdtarima());*/
        return "pm:registroEditar";
    }
    public Pallet_desc getEditar() 
    {/*
        this.editar=new Pallet_desc();
        FacesContext facesContext = FacesContext. getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        Map params=null;
        params = externalContext.getRequestParameterMap();
        Integer id=null;
        try
        {
            id = new Integer((String) params.get("id"));
            HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
            session.setAttribute("idDetalle",id); 
            PalletDescController tr=new PalletDescController();
            this.editar = tr.getOneEdit(id);
        }
        catch(Exception e)
        {
            HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
            id = (Integer) session.getAttribute("idDetalle"); 
            PalletDescController tr=new PalletDescController();
            this.editar = tr.getOneEdit(id);
        }
        String pagina="operadorDetalleNew.xhtml";*/
        return this.editar;        
    }

    public void setEditar(Pallet_desc editar) {
        this.editar = editar;
    }

     
    public void guardarDetalleEditado(
            Integer cajas,
            Integer idcalibre,
            Integer idcalidad,
            Integer idactivo,
            String cdi,
            Integer id,
            Integer idPallet,
            Integer usuario)
    {
        
        Integer  cajas_     = cajas;
        Integer idcalibre_ = idcalibre;
        Integer idcalidad_ = idcalidad;
        Integer idactivo_  = idactivo;
        Integer id_        = id;
        Integer idPallet_  = idPallet;
        Integer usuario_   = usuario;
        try
        {
            if(idcalibre_  != null && 
                    idcalidad_ != null && 
                        idactivo_ != null )
            {
                PalletController controller=new PalletController();
                PalletDescController detalle =new PalletDescController(idPallet);
                detalle.updateRecor(id_,cajas_,idcalibre_,idcalidad_,cdi,idactivo_,usuario_);
                seleccionMobileListUno(idPallet_);
            }
        }
        catch(Exception e)
        {
            System.out.println("e::"+e.getMessage());
        }
    } 
    public void seleccionMobileListUno(Integer id)
    {
        FacesContext context = FacesContext.getCurrentInstance();
        try
        { 
            context.getExternalContext().redirect("operadorDetalle.xhtml?faces-redirect=true&id="+id);
        }
        catch (IOException ex) {
            Logger.getLogger(PalletMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
