/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.org.citricos.mb; 
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List; 
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct; 
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;  
import javax.servlet.http.HttpSession;
import mx.org.citicos.controller.CatInsumosController;
import mx.org.citicos.controller.CatTipoPagoController;
import mx.org.citicos.controller.PagosController;  
import mx.org.citicos.controller.ProductorController;
import mx.org.citricos.entity.CatInsumos;
import mx.org.citricos.entity.CatTipoPago;
import mx.org.citricos.entity.Pagos;
import mx.org.citricos.entity.Productor; 

/**
 *
 * @author luis Adrian
 */ 
@ManagedBean (name = "pagosMB")
@RequestScoped
public class PagosMB implements Serializable
{
    private SessionMB  sessionMB =new SessionMB();
    PagosController controller=new PagosController();
    ProductorController controllerPRO=new ProductorController();
    CatInsumosController controllerINS=new CatInsumosController();
    CatTipoPagoController controllerTP=new CatTipoPagoController();
    private final FacesContext context = FacesContext.getCurrentInstance();
    private Float suma=0f;
    private Pagos registroSeleccionado;
    private Pagos registroEditar;
    private Pagos registroAnexar;
    private Pagos registroBorrado;
    private List<Pagos> pagostotales; 
    private List<Pagos> deudastotales; 
    private List<Pagos> resumentotales;
    private List<Pagos> detalle;
    private Integer    idDetalle; 
    private List<Productor> productores;
    private List<CatInsumos> insumos;
    private List<CatTipoPago> tipoPagosP;
    private List<CatTipoPago> tipoPagosN;
    private Integer id;
    private String descripcion;
    private Integer idproductor; 
    private String fecha;
    private Date   fechat;
    private Integer idtipopago;
    private Float monto;
    private Float montopromesa;
    private String fechapromesa;
    private Date   fechapromesat;
    private Integer idactivo;
    private Integer idinsumo;
    private Integer idDetalleSumatoria;
    @PostConstruct
    public void init() {
        registroSeleccionado=new Pagos();
        this.pagostotales=controller.getAll(1);
        this.deudastotales=controller.getAll(-1);
        this.resumentotales=controller.getTotaly();
        this.productores=controllerPRO.getAll();
        this.insumos=controllerINS.getAll();
        this.tipoPagosP=controllerTP.getAll(1);
        this.tipoPagosN=controllerTP.getAll(-1);
    }

    public void seleccionEditar(Pagos editar )
    {
        this.registroEditar=new Pagos();
        this.registroEditar.setId(editar.getId());  
        
        
        System.out.println("seleccion Editar "+editar.toString());
    }
    public void seleccionBorrado(Pagos editar )
    {
        this.registroEditar=new Pagos();
        this.registroEditar.setId(editar.getId());  
        this.registroEditar.setIdactivo(editar.getIdactivo());
        System.out.println("seleccionBorrado "+editar.toString());
    }
    public void seleccionResumen(Pagos ed)
    {
        try { 
            context.getExternalContext().redirect("Listar_1.xhtml?id="+ed.getId()+"&faces-redirect=true");
        } catch (IOException ex) {
            Logger.getLogger(PagosMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void editarRegistroPagos(Pagos ed, Integer idtipopago,Integer idproductor)
    {
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            String fechaN=format.format(ed.getFechat());
            controller.updateRecord(ed.getDescripcion()
                    ,ed.getIdproductor(),fechaN,idtipopago, ed.getMonto(),ed.getId(),
                    sessionMB.getUsuariofinal().getId());
            context.getExternalContext().redirect("Listar.xhtml?faces-redirect=true");
        } catch (IOException ex) {
            Logger.getLogger(PagosMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void editarRegistroDeudas(Pagos ed, Integer idtipopago,Integer idproductor,Integer idInsumo)
    {
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            String fechaT=format.format(ed.getFechat());
            String fechaPro="";
            if(ed.getFechapromesat()!=null)
                fechaPro = format.format(ed.getFechapromesat());
            if(idInsumo==null) idInsumo=0;
            if(montopromesa==null) montopromesa=new Float(0.0);
            if(fechaPro==null) fechaPro="";
            controller.updateRecord(ed.getDescripcion()
                    ,idproductor,fechaT,idtipopago, ed.getMonto(),
                    fechaPro,ed.getMontopromesa(),idInsumo,ed.getId(),
                    (sessionMB.getUsuariofinal().getId()));
            context.getExternalContext().redirect("Listar.xhtml?faces-redirect=true");
        } catch (IOException ex) {
            Logger.getLogger(PagosMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void anexarPago()
    { 
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            String fechaN=format.format(fechat);
            controller.insertRecord(descripcion,idproductor,fechaN, idtipopago, monto,
                    sessionMB.getUsuariofinal().getId());
            context.getExternalContext().redirect("Listar.xhtml?faces-redirect=true");
        } catch (IOException ex) {
            Logger.getLogger(PagosMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    } 
    public void anexarDeuda( )
    {
         
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            String fechaM=format.format(fechat); 
            String fechaPro="";
            if(fechapromesat!=null) fechaPro = format.format(fechapromesat);
            if(idinsumo==null) idinsumo=0;
            if(montopromesa==null) montopromesa=new Float(0.0);
            if(fechaPro==null) fechaPro="";
            controller.insertRecord(descripcion, idproductor,fechaM, idtipopago, monto
                , fechaPro, montopromesa, idinsumo,sessionMB.getUsuariofinal().getId());
            context.getExternalContext().redirect("Listar.xhtml?faces-redirect=true");
        } catch (IOException ex) {
            Logger.getLogger(PagosMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void borrarRegistro(Pagos registroAborrar)
    {
        try
        { 
            controller.deleteRecord(registroAborrar.getId(),sessionMB.getUsuariofinal().getId());
            System.out.println("seleccion Borrar "+registroAborrar.toString()); 
            this.pagostotales=controller.getAll(1);
            this.deudastotales=controller.getAll(-1);
            this.resumentotales=controller.getTotaly();
            context.getExternalContext().redirect("Listar.xhtml?faces-redirect=true");
            context.addMessage(null, new FacesMessage("Borrar", "Registro  borrado con exito :  "+registroAborrar.getId()));
        }
        catch (IOException ex) {
            Logger.getLogger(PagosMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /*-------------------------------------------------------------------------------------*/
   
    public List<Pagos> getDetalle() 
    {
        Integer idDetalle=null;
        try
        {
            FacesContext facesContext = FacesContext. getCurrentInstance();
            ExternalContext externalContext = facesContext.getExternalContext();
            Map params = externalContext.getRequestParameterMap();
            idDetalle=  new Integer((String) params.get("id" )); 
            HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
            session.setAttribute("idDetalle", idDetalle);
        }
        catch(Exception e)
        {
            try
            {
                FacesContext facesContext = FacesContext.getCurrentInstance();
                HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
                idDetalle = (Integer) session.getAttribute("idDetalle");
            }
            catch(Exception ed)
            {
                ed.printStackTrace();
            }
        }
        try { 
            
            this.detalle =  controller.getAllbyProductor(idDetalle); 
            float dato=0;
            for(int i=0;i<this.detalle.size();i++)
                dato =  dato  + ( ((this.detalle.get(i))).getMonto() * ((this.detalle.get(i))).getTipo());
            this.monto =new Float(dato);
        } catch (Exception ex) {
            Logger.getLogger(PagosMB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return detalle;
    }

    public PagosController getController() {
        return controller;
    }

    public void setController(PagosController controller) {
        this.controller = controller;
    } 

    public Pagos getRegistroSeleccionado() {
        return registroSeleccionado;
    }

    public void setRegistroSeleccionado(Pagos registroSeleccionado) {
        this.registroSeleccionado = registroSeleccionado;
    }

    public Pagos getRegistroEditar() {
        return registroEditar;
    }

    public void setRegistroEditar(Pagos registroEditar) {
        this.registroEditar = registroEditar;
    }

    public Pagos getRegistroAnexar() {
        return registroAnexar;
    }

    public void setRegistroAnexar(Pagos registroAnexar) {
        this.registroAnexar = registroAnexar;
    }

    public Pagos getRegistroBorrado() {
        return registroBorrado;
    }

    public void setRegistroBorrado(Pagos registroBorrado) {
        this.registroBorrado = registroBorrado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ProductorController getControllerPRO() {
        return controllerPRO;
    }

    public void setControllerPRO(ProductorController controllerPRO) {
        this.controllerPRO = controllerPRO;
    }

    public CatInsumosController getControllerINS() {
        return controllerINS;
    }

    public void setControllerINS(CatInsumosController controllerINS) {
        this.controllerINS = controllerINS;
    }

    public CatTipoPagoController getControllerTP() {
        return controllerTP;
    }

    public void setControllerTP(CatTipoPagoController controllerTP) {
        this.controllerTP = controllerTP;
    }

    public List<Pagos> getPagostotales() {
        return pagostotales;
    }

    public void setPagostotales(List<Pagos> pagostotales) {
        this.pagostotales = pagostotales;
    }

    public List<Pagos> getDeudastotales() {
        return deudastotales;
    }

    public void setDeudastotales(List<Pagos> deudastotales) {
        this.deudastotales = deudastotales;
    }

    public List<Pagos> getResumentotales() {
        return resumentotales;
    }

    public void setResumentotales(List<Pagos> resumentotales) {
        this.resumentotales = resumentotales;
    }

    public List<Productor> getProductores() {
        return productores;
    }

    public void setProductores(List<Productor> productores) {
        this.productores = productores;
    }

    public List<CatInsumos> getInsumos() {
        return insumos;
    }

    public void setInsumos(List<CatInsumos> insumos) {
        this.insumos = insumos;
    }

    public List<CatTipoPago> getTipoPagosP() {
        return tipoPagosP;
    }

    public void setTipoPagosP(List<CatTipoPago> tipoPagosP) {
        this.tipoPagosP = tipoPagosP;
    }

    public List<CatTipoPago> getTipoPagosN() {
        return tipoPagosN;
    }

    public void setTipoPagosN(List<CatTipoPago> tipoPagosN) {
        this.tipoPagosN = tipoPagosN;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getIdproductor() {
        return idproductor;
    }

    public void setIdproductor(Integer idproductor) {
        this.idproductor = idproductor;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Date getFechat() {
        return fechat;
    }

    public void setFechat(Date fechat) {
        this.fechat = fechat;
    }

    public Integer getIdtipopago() {
        return idtipopago;
    }

    public void setIdtipopago(Integer idtipopago) {
        this.idtipopago = idtipopago;
    }

    public Float getMonto() {
        return monto;
    }

    public void setMonto(Float monto) {
        this.monto = monto;
    }

    public Float getMontopromesa() {
        return montopromesa;
    }

    public void setMontopromesa(Float montopromesa) {
        this.montopromesa = montopromesa;
    }

    public String getFechapromesa() {
        return fechapromesa;
    }

    public void setFechapromesa(String fechapromesa) {
        this.fechapromesa = fechapromesa;
    }

    public Date getFechapromesat() {
        return fechapromesat;
    }

    public void setFechapromesat(Date fechapromesat) {
        this.fechapromesat = fechapromesat;
    }

    public Integer getIdactivo() {
        return idactivo;
    }

    public void setIdactivo(Integer idactivo) {
        this.idactivo = idactivo;
    }

    public Integer getIdinsumo() {
        return idinsumo;
    }

    public void setIdinsumo(Integer idinsumo) {
        this.idinsumo = idinsumo;
    }

    public void setDetalle(List<Pagos> detalle) {
        this.detalle = detalle;
    }

    public Integer getIdDetalle() {
        return idDetalle;
    }

    public void setIdDetalle(Integer idDetalle) {
        this.idDetalle = idDetalle;
    }

    public Float getSuma() {
        return suma;
    }

    public void setSuma(Float suma) {
        this.suma = suma;
    }

    public Integer getIdDetalleSumatoria() {
        return idDetalleSumatoria;
    }

    public void setIdDetalleSumatoria(Integer idDetalleSumatoria) {
        this.idDetalleSumatoria = idDetalleSumatoria;
    }

    
}