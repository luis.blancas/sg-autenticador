/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.org.citricos.mb; 
import java.io.IOException;
import java.io.Serializable;
import java.util.List; 
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct; 
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext; 
import mx.org.citicos.controller.CatInsumosController; 
import mx.org.citricos.entity.CatInsumos; 

/**
 *
 * @author luis Adrian
 */ 
@ManagedBean (name = "catinsumoMB")
@RequestScoped
public class CatinsumoMB implements Serializable
{
    private SessionMB  sessionMB =new SessionMB();
    CatInsumosController controller=new CatInsumosController();
    private final FacesContext context = FacesContext.getCurrentInstance();
    private List<CatInsumos> marcas; 
    private CatInsumos marcaSeleccionado;
    private CatInsumos marcaEditar;
    private CatInsumos marcaAnexar;
    private CatInsumos marcaBorrado; 
    
    private Integer id;
    private String  descripcion;
    private Float monto;
    private Integer cantidad;
    private String clave;
    
    @PostConstruct
    public void init() {
        marcaSeleccionado=new CatInsumos();
        this.marcas=controller.getAll();
    }
    public List<CatInsumos> getCalidads() {
        
        this.marcas= controller.getAll();
        return marcas;
    } 
    public void seleccionEditar(CatInsumos editar )
    {
        this.marcaEditar=new CatInsumos();
        this.marcaEditar.setId(editar.getId()); 
        this.marcaEditar.setConcepto(editar.getConcepto()); 
        this.marcaEditar.setMonto(editar.getMonto());
        this.marcaEditar.setCantidad(editar.getCantidad());
        this.marcaEditar.setClave(editar.getClave()); 
    }
    public void seleccionBorrado(CatInsumos borrado )
    {
        this.marcaBorrado=new CatInsumos();
        this.marcaBorrado.setId(borrado.getId());
        this.marcaBorrado.setConcepto(borrado.getConcepto()); 
        this.marcaBorrado.setMonto(borrado.getMonto());
        this.marcaBorrado.setCantidad(borrado.getCantidad());
        this.marcaBorrado.setClave(borrado.getClave()); 
    } 
    public void editarmarca(Integer id,CatInsumos o)
    {
        try
        {  
            controller.updateRecordUsr(o.getConcepto(),o.getMonto(),o.getCantidad(),
                    o.getClave(),sessionMB.getUsuariofinal().getId(),id );
            context.getExternalContext().redirect("Listar.xhtml?faces-redirect=true");
        }
        catch (IOException ex) {
            Logger.getLogger(CatinsumoMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void anexarmarca()
    {
        try
        {  
            controller.insertRecordUsr(descripcion,monto,cantidad,clave,sessionMB.getUsuariofinal().getId());
            context.getExternalContext().redirect("Listar.xhtml?faces-redirect=true");
        }
        catch (IOException ex) 
        {
            Logger.getLogger(CatinsumoMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void borrarmarca(CatInsumos marcaAborrar)
    {
        try
        { 
            controller.deleteRecord(marcaAborrar.getId(),sessionMB.getUsuariofinal().getId());
            System.out.println("seleccion Borrar "+marcaAborrar.toString()); 
            this.marcas= controller.getAll();
            context.getExternalContext().redirect("Listar.xhtml?faces-redirect=true");
            context.addMessage(null, new FacesMessage("Borrar", "Registro  borrado con exito :  "+marcaAborrar.getConcepto()));
        }
        catch (IOException ex) {
            Logger.getLogger(CatinsumoMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /*-------------------------------------------------------------------------------------*/

    public CatInsumosController getController() {
        return controller;
    }

    public void setController(CatInsumosController controller) {
        this.controller = controller;
    }

    public List<CatInsumos> getMarcas() {
        return marcas;
    }

    public void setMarcas(List<CatInsumos> marcas) {
        this.marcas = marcas;
    }

    public CatInsumos getMarcaSeleccionado() {
        return marcaSeleccionado;
    }

    public void setMarcaSeleccionado(CatInsumos marcaSeleccionado) {
        this.marcaSeleccionado = marcaSeleccionado;
    }

    public CatInsumos getMarcaEditar() {
        return marcaEditar;
    }

    public void setMarcaEditar(CatInsumos marcaEditar) {
        this.marcaEditar = marcaEditar;
    }

    public CatInsumos getMarcaAnexar() {
        return marcaAnexar;
    }

    public void setMarcaAnexar(CatInsumos marcaAnexar) {
        this.marcaAnexar = marcaAnexar;
    }

    public CatInsumos getMarcaBorrado() {
        return marcaBorrado;
    }

    public void setMarcaBorrado(CatInsumos marcaBorrado) {
        this.marcaBorrado = marcaBorrado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Float getMonto() {
        return monto;
    }

    public void setMonto(Float monto) {
        this.monto = monto;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }
 
}
