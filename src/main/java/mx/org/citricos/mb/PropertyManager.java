/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.org.citricos.mb;
import java.io.Serializable;
import java.util.ArrayList;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import  mx.org.citricos.entity.Person;
import java.util.List;
 
@Named(value = "manager")
@ViewScoped
public class PropertyManager implements Serializable {
 
    private String name;
    private String surname;
    private int age;
    private String city;
    private List cacheList;
    
 
    public void save() {
        Person p = new Person(name, surname, age, city);
        cacheList.add(p);
    }
 
    public void clear() {
        cacheList.clear();
    }
 
    public void postProcessXLS(Object document) {
        HSSFWorkbook wb = (HSSFWorkbook) document;
        HSSFSheet sheet = wb.getSheetAt(0);
        CellStyle style = wb.createCellStyle();
        style.setFillBackgroundColor(IndexedColors.AQUA.getIndex());
 
        for (Row row : sheet) {
            for (Cell cell : row) {
                cell.setCellValue(cell.getStringCellValue().toUpperCase());
                cell.setCellStyle(style);
            }
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public List getCacheList() {
        cacheList=new ArrayList();
        for(int i=0;i<39;i++)
        {
            Person p = new Person("name"+i, "surname"+i, 1000 +i, "city"+i);
            cacheList.add(p);
        }
        return cacheList;
    }

    public void setCacheList(List cacheList) {
        this.cacheList = cacheList;
    }
 
}