/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.org.citricos.mb; 
import java.io.IOException;
import java.io.Serializable;
import java.util.List; 
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct; 
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext; 
import mx.org.citicos.controller.MarcaController; 
import mx.org.citricos.entity.Marca; 

/**
 *
 * @author luis Adrian
 */ 
@ManagedBean (name = "marcaMB")
@RequestScoped
public class MarcaMB implements Serializable
{
    private SessionMB  sessionMB =new SessionMB();
    MarcaController controller=new MarcaController();
    private final FacesContext context = FacesContext.getCurrentInstance();
    private List<Marca> marcas; 
    private Marca marcaSeleccionado;
    private Marca marcaEditar;
    private Marca marcaAnexar;
    private Marca marcaBorrado; 
    
    private Integer id;
    private String  descripcion; 
    
    @PostConstruct
    public void init() {
        marcaSeleccionado=new Marca();
        this.marcas=controller.getAll();
    }
    public List<Marca> getCalidads() {
        
        this.marcas= controller.getAll();
        return marcas;
    } 
    public void seleccionEditar(Marca editar )
    {
        this.marcaEditar=new Marca();
        this.marcaEditar.setId(editar.getId()); 
        this.marcaEditar.setNombre(editar.getNombre()); 
        System.out.println("seleccion Editar "+editar.toString());
    }
    public void seleccionBorrado(Marca borrado )
    {
        this.marcaBorrado=new Marca();
        this.marcaBorrado.setId(borrado.getId());
        this.marcaBorrado.setNombre(borrado.getNombre());
        System.out.println("seleccionBorrado "+borrado.toString());
    } 
    public void editarmarca(Integer id,Marca o )
    {
        try
        {  
            controller.updateRecordUsr(o.getNombre(),id,sessionMB.getUsuariofinal().getId());
            context.getExternalContext().redirect("Listarmarca.xhtml?faces-redirect=true");
        }
        catch (IOException ex) {
            Logger.getLogger(MarcaMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void anexarmarca()
    {
        try
        { 
            controller.insertRecordUsr(descripcion,sessionMB.getUsuariofinal().getId());
            context.getExternalContext().redirect("Listarmarca.xhtml?faces-redirect=true");
        }
        catch (IOException ex) 
        {
            Logger.getLogger(MarcaMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void borrarmarca(Marca marcaAborrar )
    {
        try
        { 
            controller.deleteRecord(marcaAborrar.getId(),sessionMB.getUsuariofinal().getId());
            this.marcas= controller.getAll();
            context.getExternalContext().redirect("Listarmarca.xhtml?faces-redirect=true");
            context.addMessage(null, new FacesMessage("Borrar", "Registro borrado con exito :  "+marcaAborrar.getNombre()));
        }
        catch (IOException ex) {
            Logger.getLogger(MarcaMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public MarcaController getController() {
        return controller;
    }

    public void setController(MarcaController controller) {
        this.controller = controller;
    }

    public List<Marca> getMarcas() {
        return marcas;
    }

    public void setMarcas(List<Marca> marcas) {
        this.marcas = marcas;
    }

    public Marca getMarcaSeleccionado() {
        return marcaSeleccionado;
    }

    public void setMarcaSeleccionado(Marca marcaSeleccionado) {
        this.marcaSeleccionado = marcaSeleccionado;
    }

    public Marca getMarcaEditar() {
        return marcaEditar;
    }

    public void setMarcaEditar(Marca marcaEditar) {
        this.marcaEditar = marcaEditar;
    }

    public Marca getMarcaAnexar() {
        return marcaAnexar;
    }

    public void setMarcaAnexar(Marca marcaAnexar) {
        this.marcaAnexar = marcaAnexar;
    }

    public Marca getMarcaBorrado() {
        return marcaBorrado;
    }

    public void setMarcaBorrado(Marca marcaBorrado) {
        this.marcaBorrado = marcaBorrado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
