/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.org.citricos.mb; 
import java.io.IOException;
import java.io.Serializable;
import java.util.List; 
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct; 
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext; 
import mx.org.citicos.controller.ClienteController; 
import mx.org.citricos.entity.Cliente; 

/**
 *
 * @author luis Adrian
 */ 
@ManagedBean (name = "clienteMB")
@RequestScoped
public class ClienteMB implements Serializable
{
    private SessionMB  sessionMB =new SessionMB();
    ClienteController controller=new ClienteController();
    private final FacesContext context = FacesContext.getCurrentInstance();
    private List<Cliente> todos; 
    private Cliente registroSeleccionado;
    private Cliente registroEditar;
    private Cliente registroAnexar;
    private Cliente registroBorrado; 
    
    private Integer id;
    private String  pais;
    private String  estado;
    private String  nombre;
    private String  apellidos;
    
    @PostConstruct
    public void init() {
        registroSeleccionado=new Cliente();
        this.todos=controller.getAll();
    }
    public List<Cliente> getCalidads() {
        
        this.todos= controller.getAll();
        return todos;
    } 
    public void seleccionEditar(Cliente editar )
    {
        this.registroEditar=new Cliente();
        this.registroEditar.setId(editar.getId()); 
        this.registroEditar.setNombre(editar.getNombre()); 
        System.out.println("seleccion Editar "+editar.toString());
    }
    public void seleccionBorrado(Cliente borrado ,Integer us)
    {
        this.registroBorrado=new Cliente();
        this.registroBorrado.setId(borrado.getId());
        this.registroBorrado.setNombre(borrado.getNombre());
        System.out.println("seleccionBorrado "+borrado.toString());
    } 
    public void editarregistro(Integer id,Cliente o )
    {
        try
        {  
            controller.updateRecord(o.getPais(),o.getEstado(),o.getNombre(),o.getApellidos(),
                    sessionMB.getUsuariofinal().getId(),id );
            context.getExternalContext().redirect("Listar.xhtml?faces-redirect=true");
        }
        catch (IOException ex) {
            Logger.getLogger(ClienteMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void anexarregistro( )
    {
        try
        {  
            controller.insertRecord(this.pais,this.estado,this.nombre,this.apellidos,
                    sessionMB.getUsuariofinal().getId());
            context.getExternalContext().redirect("Listar.xhtml?faces-redirect=true");
        }
        catch (IOException ex) 
        {
            Logger.getLogger(ClienteMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void borrarregistro(Cliente registroAborrar )
    {
        try
        { 
            controller.deleteRecord(registroAborrar.getId(),sessionMB.getUsuariofinal().getId()); 
            this.todos= controller.getAll();
            context.getExternalContext().redirect("Listar.xhtml?faces-redirect=true");
            context.addMessage(null, new FacesMessage("Borrar", "Registro  borrado con exito :  "+registroAborrar.getNombre()));
        }
        catch (IOException ex) {
            Logger.getLogger(ClienteMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /*-------------------------------------------------------------------------------------*/

    public ClienteController getController() {
        return controller;
    }

    public void setController(ClienteController controller) {
        this.controller = controller;
    }

    public List<Cliente> getTodos() {
        return todos;
    }

    public void setTodos(List<Cliente> todos) {
        this.todos = todos;
    }

    public Cliente getRegistroSeleccionado() {
        return registroSeleccionado;
    }

    public void setRegistroSeleccionado(Cliente registroSeleccionado) {
        this.registroSeleccionado = registroSeleccionado;
    }

    public Cliente getRegistroEditar() {
        return registroEditar;
    }

    public void setRegistroEditar(Cliente registroEditar) {
        this.registroEditar = registroEditar;
    }

    public Cliente getRegistroAnexar() {
        return registroAnexar;
    }

    public void setRegistroAnexar(Cliente registroAnexar) {
        this.registroAnexar = registroAnexar;
    }

    public Cliente getRegistroBorrado() {
        return registroBorrado;
    }

    public void setRegistroBorrado(Cliente registroBorrado) {
        this.registroBorrado = registroBorrado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }
    
}
