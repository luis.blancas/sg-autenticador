/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.org.citricos.mb;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import mx.org.citicos.controller.PreciosController;
import mx.org.citricos.entity.Precios;

/**
 *
 * @author luis Adrian
 */ 
@ManagedBean (name = "preciosMB")
@RequestScoped
public class PreciosMB  implements Serializable
{
    private final FacesContext context = FacesContext.getCurrentInstance();
    PreciosController controller=new PreciosController();
    private List<Precios> activos; 
    private Precios activoSeleccionado;
    private Precios activoEditar =new Precios();
    private Precios activoAnexar =new Precios();
    private Precios activoBorrado=new Precios();  
    private Precios activo       =new Precios();  
    @PostConstruct
    public void init() 
    {
        this.activoSeleccionado=new Precios();
        this.activoSeleccionado=controller.getSeleccionMasActual();
        
    }
    public List<Precios> getActivos() 
    {
        this.activos= controller.getAll();
        return activos;
    }
    public String seleccionEditar(Precios editar )
    {
        this.activoEditar=new Precios();
        this.activoEditar.setId(editar.getId());
        this.activoEditar.setFecha(editar.getFecha());
        this.activoEditar.setVerde_japon(editar.getVerde_japon());
        this.activoEditar.setVerde_110(editar.getVerde_110());
        this.activoEditar.setVerde_150(editar.getVerde_150());
        this.activoEditar.setVerde_175(editar.getVerde_175());
        this.activoEditar.setVerde_200(editar.getVerde_200());
        this.activoEditar.setVerde_230(editar.getVerde_230());
        this.activoEditar.setVerde_250(editar.getVerde_250());
        this.activoEditar.setEmpaque_110(editar.getEmpaque_110());
        this.activoEditar.setEmpaque_150(editar.getEmpaque_150());
        this.activoEditar.setEmpaque_175(editar.getEmpaque_175());
        this.activoEditar.setEmpaque_200(editar.getEmpaque_200());
        this.activoEditar.setEmpaque_230(editar.getEmpaque_230());
        this.activoEditar.setEmpaque_250(editar.getEmpaque_250());
        this.activoEditar.setSegundas(editar.getSegundas());
        this.activoEditar.setTerceras(editar.getTerceras());
        this.activoEditar.setTorreon(editar.getTorreon());
        this.activoEditar.setColeada(editar.getColeada());
        return "pm:registroEditar";
    }
    
    public void creaNuevoRegistro( 
                                Double verde_japon,Double verde_110,Double verde_150,Double verde_175,Double verde_200,Double verde_230,Double verde_250,
                                Double empaque_110,Double empaque_150,Double empaque_175,Double empaque_200,Double empaque_230,Double empaque_250,	
                                Double segundas,Double terceras,Double torreon,Double coleada,
                                Integer creado)
    {
        
        try
        {
            controller.insertRecord(
                    verde_japon,verde_110,verde_150,verde_175,verde_200,verde_230,verde_250,
                    empaque_110,empaque_150,empaque_175,empaque_200,empaque_230,empaque_250,	
                    segundas,terceras,torreon,coleada,
                    creado);
            context.getExternalContext().redirect("preciosd.xhtml?faces-redirect=true");
        }
        catch (IOException ex)
        {
            Logger.getLogger(ActivoMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void cancelar()
    {
        try
        {
            context.getExternalContext().redirect("preciosd.xhtml?faces-redirect=true");
        }
        catch (IOException ex) 
        {
            Logger.getLogger(ActivoMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     
    public void irAmenu()
    {
        try
        {
            context.getExternalContext().redirect("menu.xhtml?faces-redirect=true");
        }
        catch (IOException ex) {
            Logger.getLogger(ActivoMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
        
    public Precios getActivoSeleccionado() 
    {
        if(this.activoSeleccionado==null)
        {
            this.activoSeleccionado=new Precios();
            this.activoSeleccionado=controller.getSeleccionMasActual();
        }
        System.out.println("this.activoSeleccionado>>"+this.activoSeleccionado.toString());
        return this.activoSeleccionado;
    }

    public void setActivoSeleccionado(Precios activoSeleccionado) {
        this.activoSeleccionado = activoSeleccionado;
    }

    public Precios getActivoEditar() {
        return activoEditar;
    }

    public void setActivoEditar(Precios activoEditar) {
        this.activoEditar = activoEditar;
    }

    public Precios getActivoAnexar() {
        return activoAnexar;
    }

    public void setActivoAnexar(Precios activoAnexar) {
        this.activoAnexar = activoAnexar;
    }

    public Precios getActivoBorrado() {
        return activoBorrado;
    }

    public void setActivoBorrado(Precios activoBorrado) {
        this.activoBorrado = activoBorrado;
    }

    public PreciosController getController() {
        return controller;
    }

    public void setController(PreciosController controller) {
        this.controller = controller;
    }

    public Precios getActivo() {
        return activo;
    }

    public void setActivo(Precios activo) {
        this.activo = activo;
    }
    
    
}
