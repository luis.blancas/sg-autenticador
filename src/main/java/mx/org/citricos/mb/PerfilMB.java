/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.org.citricos.mb; 
import java.io.IOException;
import java.io.Serializable;
import java.util.List; 
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct; 
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean; 
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext; 
import mx.org.citicos.controller.PerfilController; 
import mx.org.citricos.entity.Perfil; 

/**
 *
 * @author luis Adrian
 */ 
@ManagedBean (name = "perfilMB")
@RequestScoped
public class PerfilMB implements Serializable
{
    private SessionMB  sessionMB =new SessionMB();
    PerfilController controller=new PerfilController();
    private final FacesContext context = FacesContext.getCurrentInstance();
    private List<Perfil> todos; 
    private Perfil perfilSeleccionado;
    private Perfil perfilEditar;
    private Perfil perfilAnexar;
    private Perfil perfilBorrado; 
    
    private Integer id;
    private String  descripcion; 
    private String  pagina; 
    @PostConstruct
    public void init() {
        perfilSeleccionado=new Perfil();
        this.todos=controller.getAll();
    }
    public List<Perfil> getCalidads() {
        
        this.todos= controller.getAll();
        return todos;
    } 
    public void seleccionEditar(Perfil editar )
    {
        this.perfilEditar=new Perfil();
        this.perfilEditar.setId(editar.getId()); 
        this.perfilEditar.setNombre(editar.getNombre()); 
        this.perfilEditar.setPagina(editar.getPagina()); 
        System.out.println("seleccion Editar "+editar.toString());
    }
    public void seleccionBorrado(Perfil borrado )
    {
        this.perfilBorrado=new Perfil();
        this.perfilBorrado.setId(borrado.getId());
        this.perfilBorrado.setNombre(borrado.getNombre());
        this.perfilBorrado.setPagina(borrado.getPagina()); 
        System.out.println("seleccionBorrado "+borrado.toString());
    } 
    public void editarperfil(Integer id,Perfil o)
    {
        try
        { 
            System.out.println("Editar id    :"+id);
            System.out.println("Editar Nombre:"+o.getNombre());
            controller.updateRecordUsr(o.getNombre(),o.getPagina(), id,sessionMB.getUsuariofinal().getId());
            context.getExternalContext().redirect("Listarperfil.xhtml?faces-redirect=true");
        }
        catch (IOException ex) {
            Logger.getLogger(PerfilMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void anexarperfil()
    {
        try
        { 
            System.out.println("Anexar Nombre:"+this.descripcion);
            controller.insertRecordUsr(descripcion,pagina,sessionMB.getUsuariofinal().getId());
            context.getExternalContext().redirect("Listarperfil.xhtml?faces-redirect=true");
        }
        catch (IOException ex) 
        {
            Logger.getLogger(PerfilMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void borrarperfil(Perfil perfilAborrar)
    {
        try
        { 
            controller.deleteRecord(perfilAborrar.getId().intValue(),sessionMB.getUsuariofinal().getId());
            System.out.println("seleccion Borrar "+perfilAborrar.toString()); 
            this.todos= controller.getAll();
            context.getExternalContext().redirect("Listarperfil.xhtml?faces-redirect=true");
            context.addMessage(null, new FacesMessage("Borrar", "Registro  borrado con exito :  "+perfilAborrar.getNombre()));
        }
        catch (IOException ex) {
            Logger.getLogger(PerfilMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /*-------------------------------------------------------------------------------------*/

    public PerfilController getController() {
        return controller;
    }

    public void setController(PerfilController controller) {
        this.controller = controller;
    }
    
    public Perfil getPerfilSeleccionado() {
        return perfilSeleccionado;
    }

    public void setPerfilSeleccionado(Perfil perfilSeleccionado) {
        this.perfilSeleccionado = perfilSeleccionado;
    }

    public Perfil getPerfilEditar() {
        return perfilEditar;
    }

    public void setPerfilEditar(Perfil perfilEditar) {
        this.perfilEditar = perfilEditar;
    }

    public Perfil getPerfilAnexar() {
        return perfilAnexar;
    }

    public void setPerfilAnexar(Perfil perfilAnexar) {
        this.perfilAnexar = perfilAnexar;
    }

    public Perfil getPerfilBorrado() {
        return perfilBorrado;
    }

    public void setPerfilBorrado(Perfil perfilBorrado) {
        this.perfilBorrado = perfilBorrado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<Perfil> getTodos() {
        return todos;
    }

    public void setTodos(List<Perfil> todos) {
        this.todos = todos;
    }

    public String getPagina() {
        return pagina;
    }

    public void setPagina(String pagina) {
        this.pagina = pagina;
    }
    
}
