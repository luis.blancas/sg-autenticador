/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.org.citricos.mb; 
import java.io.IOException;
import java.io.Serializable;
import java.util.List; 
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct; 
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext; 
import mx.org.citicos.controller.TipoProductorController; 
import mx.org.citricos.entity.Tipop; 

/**
 *
 * @author luis Adrian
 */ 
@ManagedBean (name = "tipopMB")
@RequestScoped
public class TipoProductorMB implements Serializable
{
    private SessionMB  sessionMB =new SessionMB();
    private final FacesContext context = FacesContext.getCurrentInstance();
    private TipoProductorController controller=new TipoProductorController();
    private List<Tipop> activos; 
    private Tipop activoSeleccionado;
    private Tipop activoEditar;
    private Tipop activoAnexar;
    private Tipop activoBorrado; 
    
    private Integer id;
    private String  nombre; 
    
    @PostConstruct
    public void init() {
        activoSeleccionado=new Tipop(); 
    }
    public List<Tipop> getActivos() {
        
        this.activos= controller.getAll();
        return activos;
    } 
    public void seleccionEditar(Tipop editar )
    {
        this.activoEditar=new Tipop();
        this.activoEditar.setId(editar.getId()); 
        this.activoEditar.setNombre(editar.getNombre()); 
        System.out.println("seleccion Editar "+editar.toString());
    }
    public void seleccionBorrado(Tipop borrado )
    {
        this.activoBorrado=new Tipop();
        this.activoBorrado.setId(borrado.getId());
        this.activoBorrado.setNombre(borrado.getNombre());
        System.out.println("seleccionBorrado "+borrado.toString());
    } 
    public void editarActivo(Integer id,Tipop o)
    {
        try
        { 
            System.out.println("Editar id    :"+id);
            System.out.println("Editar Nombre:"+o.getNombre());
            controller.updateRecord(o.getNombre(),id.intValue());
            context.getExternalContext().redirect("ListaActivo.xhtml?faces-redirect=true");
        }
        catch (IOException ex) {
            Logger.getLogger(TipoProductorMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void anexarActivo()
    {
        try
        { 
            System.out.println("Anexar Nombre:"+this.nombre);
            controller.insertRecord(nombre);
            context.getExternalContext().redirect("ListaActivo.xhtml?faces-redirect=true");
        }
        catch (IOException ex) 
        {
            Logger.getLogger(TipoProductorMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void borrarActivo(Tipop activoAborrar)
    {
        try
        { 
            controller.deleteRecord(activoAborrar.getId());
            System.out.println("seleccion Borrar "+activoAborrar.toString()); 
            this.activos= controller.getAll();
            context.getExternalContext().redirect("ListaActivo.xhtml?faces-redirect=true");
            context.addMessage(null, new FacesMessage("Borrar", "Registro  borrado con exito :  "+activoAborrar.getNombre()));
        }
        catch (IOException ex) {
            Logger.getLogger(TipoProductorMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Tipop getActivoSeleccionado() {
        return activoSeleccionado;
    }

    public void setActivoSeleccionado(Tipop activoSeleccionado) {
        this.activoSeleccionado = activoSeleccionado;
    }

    public Tipop getActivoEditar() {
        return activoEditar;
    }

    public void setActivoEditar(Tipop activoEditar) {
        this.activoEditar = activoEditar;
    }

    public Tipop getActivoAnexar() {
        return activoAnexar;
    }

    public void setActivoAnexar(Tipop activoAnexar) {
        this.activoAnexar = activoAnexar;
    }

    public Tipop getActivoBorrado() {
        return activoBorrado;
    }

    public void setActivoBorrado(Tipop activoBorrado) {
        this.activoBorrado = activoBorrado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
}
