/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.org.citricos.mb;
  
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * <p>DescripciÃ³n</p>
 * ManageBean usado para albergar datos comunes.
 *
  */
@Controller
@Scope(value="singleton")
public class CommonDataMB implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Value("${max.inputText.length}")
    private Integer maxInputTextLength;
    
    @Value("${defaultTargetUrl}")
    private String defaultTargetUrl;

    @Value("${defaultFailureUrl}")
    private String defaultFailureUrl;
    
    @Value("${app.mode.light}")
    private boolean appModeLight;

    @Value("${render.gobal.growl}")
    private boolean renderGobalGrowl;
    


    @Value("${version.app}")
    private String versionApp;


    public String getVersionApp() {
        return versionApp;
    }

    @Value("${reporte.url}")
    private String reporteUrl;


    public String getReporteUrl() {
        return reporteUrl;
    }
    
    /**
     * TamaÃ±o mÃ¡ximo usado en los inputText.
     *
     * @return TamaÃ±o definido en la llave max.inputText.length del application.properties
     */
    public Integer getMaxInputTextLength() {
        return maxInputTextLength;
    }

    /**
     * Ruta default a la que se tiene que redirigir en caso de llegar a una pÃ¡gina genÃ©rica
     * como las de error o informaciÃ³n.
     * 
     * @return Valor de la llave defaultTargetUrl del archivo application.properties.
     */
    public String getDefaultTargetUrl() {
        return defaultTargetUrl;
    }

    /**
     * Ruta defualt a la que se manda en caso de error de autentificaciÃ³n.
     * 
     * @return Ruta para el error de autentificaciÃ³n.
     */
    public String getDefaultFailureUrl() {
        return defaultFailureUrl;
    }
 
    /**
     * Indica si la aplicaciÃ³n estÃ¡ en modo light.
     * @return Indica si la aplicaciÃ³n estÃ¡ en modo light.
     */
    public boolean isAppModeLight() {
        return appModeLight;
    }

    /**
     * Indica si se debe renderear el growl global
     * @return Indica si se debe renderear el growl global
     */
    public boolean isRenderGobalGrowl() {
        return renderGobalGrowl;
    }
    
    /**    
     * Verifica si el usuario tiene, al menos, un rol de la lista.
     * @param roles Lista de roles separada por comas.
     * @return Verdadero en caso que tenga un rol de la lista, falso en otro caso.
     */
    public boolean isAnyRole(String roles) {
         
            return false;
        
    }

    public String getUsuarioNombre() 
    {
        return "Blancas";
    }
    
    public String getFechaActual(){
        String fechaActual="";
        SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        Date nvaFechaActual = new Date();
        fechaActual=formatoFecha.format(nvaFechaActual);
        return fechaActual;
    }
}
