/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.org.citricos.mb; 
import java.io.IOException;
import java.io.Serializable;
import java.util.List; 
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct; 
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean; 
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;  
import mx.org.citicos.controller.TransportistaController;  
import mx.org.citricos.entity.Transportista;  

/**
 *
 * @author luis Adrian
 */ 
@ManagedBean (name = "transportistaMB")
@RequestScoped
public class TransportistaMB implements Serializable
{
    private SessionMB  sessionMB =new SessionMB();
    TransportistaController controller=new TransportistaController();
    private final FacesContext context = FacesContext.getCurrentInstance();
    private List<Transportista> todos; 
    private Transportista registroSeleccionado;
    private Transportista registroEditar;
    private Transportista registroAnexar;
    private Transportista registroBorrado;
    private Integer id;
    private String nombre;
    private String placas;
    private String numero_economico;
    private String numero;  
    @PostConstruct
    public void init() {
        registroSeleccionado=new Transportista();
        this.todos=controller.getAll();
    }
    public List<Transportista> getCalidads() 
    {    
        this.todos= controller.getAll();
        return todos;
    } 
    public void seleccionEditar(Transportista editar )
    {
        this.registroEditar=new Transportista();
        this.registroEditar.setId(editar.getId()); 
        this.registroEditar.setNombre(editar.getNombre()); 
        this.registroEditar.setPlacas(editar.getPlacas());
        this.registroEditar.setNumero_economico(editar.getNumero_economico());
        this.registroEditar.setNumero(editar.getNumero());
        System.out.println("seleccion Editar "+editar.toString());
    }
    public void seleccionBorrado(Transportista editar )
    {
        this.registroEditar.setId(editar.getId()); 
        this.registroEditar.setNombre(editar.getNombre()); 
        this.registroEditar.setPlacas(editar.getPlacas());
        this.registroEditar.setNumero_economico(editar.getNumero_economico());
        this.registroEditar.setNumero(editar.getNumero());
        System.out.println("seleccionBorrado "+editar.toString());
    } 
    public void editarRegistro( Integer idx,Transportista o)
    {
        try
        {  
            controller.updateRecord(o.getNombre(),o.getPlacas(),o.getNumero_economico(),sessionMB.getUsuariofinal().getId(),idx);
            context.getExternalContext().redirect("Listar.xhtml?faces-redirect=true");
        }
        catch (IOException ex) {
            Logger.getLogger(TransportistaMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void anexarRegistro()
    {
        try
        { 
             
            controller.insertRecord(nombre,placas,numero_economico,sessionMB.getUsuariofinal().getId());
            context.getExternalContext().redirect("Listar.xhtml?faces-redirect=true");
        }
        catch (IOException ex) 
        {
            Logger.getLogger(TransportistaMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void borrarRegistro(Transportista registroAborrar)
    {
        try
        { 
            controller.deleteRecord(registroAborrar.getId(),sessionMB.getUsuariofinal().getId());
            System.out.println("seleccion Borrar "+registroAborrar.toString()); 
            this.todos= controller.getAll();
            context.getExternalContext().redirect("Listar.xhtml?faces-redirect=true");
            context.addMessage(null, new FacesMessage("Borrar", "Registro  borrado con exito :  "+registroAborrar.getNumero()));
        }
        catch (IOException ex) {
            Logger.getLogger(TransportistaMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /*-------------------------------------------------------------------------------------*/
 

    public TransportistaController getController() {
        return controller;
    }

    public void setController(TransportistaController controller) {
        this.controller = controller;
    } 
    public Transportista getRegistroSeleccionado() {
        return registroSeleccionado;
    }

    public void setRegistroSeleccionado(Transportista registroSeleccionado) {
        this.registroSeleccionado = registroSeleccionado;
    }

    public Transportista getRegistroEditar() {
        return registroEditar;
    }

    public void setRegistroEditar(Transportista registroEditar) {
        this.registroEditar = registroEditar;
    }

    public Transportista getRegistroAnexar() {
        return registroAnexar;
    }

    public void setRegistroAnexar(Transportista registroAnexar) {
        this.registroAnexar = registroAnexar;
    }

    public Transportista getRegistroBorrado() {
        return registroBorrado;
    }

    public void setRegistroBorrado(Transportista registroBorrado) {
        this.registroBorrado = registroBorrado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPlacas() {
        return placas;
    }

    public void setPlacas(String placas) {
        this.placas = placas;
    }

    public String getNumero_economico() {
        return numero_economico;
    }

    public void setNumero_economico(String numero_economico) {
        this.numero_economico = numero_economico;
    }

    public List<Transportista> getTodos() {
        return todos;
    }

    public void setTodos(List<Transportista> todos) {
        this.todos = todos;
    }
 
    
}