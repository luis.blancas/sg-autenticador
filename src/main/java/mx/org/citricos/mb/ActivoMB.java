/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.org.citricos.mb; 
import java.io.IOException;
import java.io.Serializable;
import java.util.List; 
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct; 
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext; 
import mx.org.citicos.controller.ActivoController;
import mx.org.citricos.entity.Activo; 

/**
 *
 * @author luis Adrian
 */ 
@ManagedBean (name = "activoMB")
@RequestScoped
public class ActivoMB implements Serializable
{
    private final FacesContext context = FacesContext.getCurrentInstance();
    private List<Activo> activos; 
    private Activo activoSeleccionado;
    private Activo activoEditar;
    private Activo activoAnexar;
    private Activo activoBorrado; 
    
    private Integer id;
    private String  nombre; 
    
    @PostConstruct
    public void init() {
        activoSeleccionado=new Activo(); 
    }
    public List<Activo> getActivos() {
        ActivoController controller=new ActivoController();
        this.activos= controller.getAll();
        return activos;
    } 
    public void seleccionEditar(Activo editar )
    {
        this.activoEditar=new Activo();
        this.activoEditar.setId(editar.getId()); 
        this.activoEditar.setNombre(editar.getNombre()); 
        System.out.println("seleccion Editar "+editar.toString());
    }
    public void seleccionBorrado(Activo borrado )
    {
        this.activoBorrado=new Activo();
        this.activoBorrado.setId(borrado.getId());
        this.activoBorrado.setNombre(borrado.getNombre());
        System.out.println("seleccionBorrado "+borrado.toString());
    } 
    public void editarActivo(Integer id,Activo o)
    {
        try
        {
            ActivoController controller=new ActivoController();
            System.out.println("Editar id    :"+id);
            System.out.println("Editar Nombre:"+o.getNombre());
            controller.updateRecord(o.getNombre(),id);
            context.getExternalContext().redirect("ListaActivo.xhtml?faces-redirect=true");
        }
        catch (IOException ex) {
            Logger.getLogger(ActivoMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void anexarActivo()
    {
        try
        {
            ActivoController controller=new ActivoController();
            System.out.println("Anexar Nombre:"+this.nombre);
            controller.insertRecord(nombre);
            context.getExternalContext().redirect("ListaActivo.xhtml?faces-redirect=true");
        }
        catch (IOException ex) 
        {
            Logger.getLogger(ActivoMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void borrarActivo(Activo activoAborrar)
    {
        try
        {
            ActivoController controller=new ActivoController();
            controller.deleteRecord(activoAborrar.getId().intValue());
            System.out.println("seleccion Borrar "+activoAborrar.toString()); 
            this.activos= controller.getAll();
            context.getExternalContext().redirect("ListaActivo.xhtml?faces-redirect=true");
            context.addMessage(null, new FacesMessage("Borrar", "Registro  borrado con exito :  "+activoAborrar.getNombre()));
        }
        catch (IOException ex) {
            Logger.getLogger(ActivoMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Activo getActivoSeleccionado() {
        return activoSeleccionado;
    }

    public void setActivoSeleccionado(Activo activoSeleccionado) {
        this.activoSeleccionado = activoSeleccionado;
    }

    public Activo getActivoEditar() {
        return activoEditar;
    }

    public void setActivoEditar(Activo activoEditar) {
        this.activoEditar = activoEditar;
    }

    public Activo getActivoAnexar() {
        return activoAnexar;
    }

    public void setActivoAnexar(Activo activoAnexar) {
        this.activoAnexar = activoAnexar;
    }

    public Activo getActivoBorrado() {
        return activoBorrado;
    }

    public void setActivoBorrado(Activo activoBorrado) {
        this.activoBorrado = activoBorrado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
}
