/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.org.citricos.mb;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List; 
import javax.annotation.PostConstruct;
import  mx.org.citricos.entity.*;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import mx.org.citicos.controller.*;

/**
 *
 * @author luisa
 */
@ManagedBean  
@ViewScoped  
public class dataExporterView implements Serializable  {
    

    private List<Calibre> calibre;
    private CalibreController calibreCTR;
    
    private List<Activo> activo;
    private ActivoController activoCTR;
    
    private List<Calidad> calidad;
    private CalidadController calidadCTR;
    
    private List<Cliente> cliente;
    private ClienteController clienteCTR;
    
    private List<Marca> marca;
    private MarcaController marcaCTR;
    
    private List<Transportista> transportista;
    private TransportistaController transportistaCTR;
    
    private List<Transporte> transporte;
    private TransporteController transporteCTR;
    
    private List<Usuario> usuario;
    private UsuarioController usuarioCTR;
    
    private List<Pallet> pallet;
    private PalletController palletCTR;
    
    private List<Perfil> perfil;
    private PerfilController perfilCTR;
    
    private List<Tarima> tarima;
    private TarimaController tarimaCTR;
     
    private HashMap map=new HashMap(); 
    
    @PostConstruct
    public void init() 
    {
        activoCTR=new ActivoController();
        activo = activoCTR.getAll();
        
        for(int i=0;i<activo.size();i++)
           map.put(activo.get(i).getId(),activo.get(i).getNombre());
        
        calibreCTR=new CalibreController();
        calibre = calibreCTR.getAll(map);
        
        
        calidadCTR=new CalidadController();
        calidad = calidadCTR.getAll(map);
        
        clienteCTR=new ClienteController();
        cliente = clienteCTR.getAll(map);
        
        transportistaCTR=new TransportistaController();
        transportista = transportistaCTR.getAll(map);
        
        
        
        usuarioCTR=new UsuarioController();
        usuario = usuarioCTR.getll(map);
        
        perfilCTR=new PerfilController();
        perfil = perfilCTR.getAll(map);
        
        palletCTR=new PalletController();
        pallet = palletCTR.getAll(map);
        
        marcaCTR=new MarcaController();
        marca = marcaCTR.getAll(map);
        
        tarimaCTR=new TarimaController();
        tarima = tarimaCTR.getAll(map);
    }

    public List<Calibre> getCalibre() {
        return calibre;
    }

    public void setCalibre(List<Calibre> calibre) {
        this.calibre = calibre;
    }

    public CalibreController getCalibreCTR() {
        return calibreCTR;
    }

    public void setCalibreCTR(CalibreController calibreCTR) {
        this.calibreCTR = calibreCTR;
    }

    public List<Activo> getActivo() {
        return activo;
    }

    public void setActivo(List<Activo> activo) {
        this.activo = activo;
    }

    public ActivoController getActivoCTR() {
        return activoCTR;
    }

    public void setActivoCTR(ActivoController activoCTR) {
        this.activoCTR = activoCTR;
    }

    public List<Calidad> getCalidad() {
        return calidad;
    }

    public void setCalidad(List<Calidad> calidad) {
        this.calidad = calidad;
    }

    public CalidadController getCalidadCTR() {
        return calidadCTR;
    }

    public void setCalidadCTR(CalidadController calidadCTR) {
        this.calidadCTR = calidadCTR;
    }

    public List<Cliente> getCliente() {
        return cliente;
    }

    public void setCliente(List<Cliente> cliente) {
        this.cliente = cliente;
    }

    public ClienteController getClienteCTR() {
        return clienteCTR;
    }

    public void setClienteCTR(ClienteController clienteCTR) {
        this.clienteCTR = clienteCTR;
    }

    public List<Marca> getMarca() {
        return marca;
    }

    public void setMarca(List<Marca> marca) {
        this.marca = marca;
    }

    public MarcaController getMarcaCTR() {
        return marcaCTR;
    }

    public void setMarcaCTR(MarcaController marcaCTR) {
        this.marcaCTR = marcaCTR;
    }

    public List<Transportista> getTransportista() {
        return transportista;
    }

    public void setTransportista(List<Transportista> transportista) {
        this.transportista = transportista;
    }

    public TransportistaController getTransportistaCTR() {
        return transportistaCTR;
    }

    public void setTransportistaCTR(TransportistaController transportistaCTR) {
        this.transportistaCTR = transportistaCTR;
    }

    public List<Transporte> getTransporte() {
        return transporte;
    }
    
    public List<Transporte> getTransportes(Integer i) 
    {
        transporteCTR=new TransporteController();
        transporte = transporteCTR.getAll(i,getMap());
        return transporte;
    }
    public List<Pallet_desc> getDetalle(Integer i) 
    {
        PalletDescController ctr=new PalletDescController(i);
        List<Pallet_desc> lista= ctr.getAll(i,getMap());
        return lista;
    }
    
    public void setTransporte(List<Transporte> transporte) {
        this.transporte = transporte;
    }

    public TransporteController getTransporteCTR() {
        return transporteCTR;
    }

    public void setTransporteCTR(TransporteController transporteCTR) {
        this.transporteCTR = transporteCTR;
    }

    public List<Usuario> getUsuario() {
        return usuario;
    }

    public void setUsuario(List<Usuario> usuario) {
        this.usuario = usuario;
    }

    public UsuarioController getUsuarioCTR() {
        return usuarioCTR;
    }

    public void setUsuarioCTR(UsuarioController usuarioCTR) {
        this.usuarioCTR = usuarioCTR;
    }

    public List<Pallet> getPallet() {
        return pallet;
    }

    public void setPallet(List<Pallet> pallet) {
        this.pallet = pallet;
    }

    public PalletController getPalletCTR() {
        return palletCTR;
    }

    public void setPalletCTR(PalletController palletCTR) {
        this.palletCTR = palletCTR;
    }

    public List<Perfil> getPerfil() {
        return perfil;
    }

    public void setPerfil(List<Perfil> perfil) {
        this.perfil = perfil;
    }

    public PerfilController getPerfilCTR() {
        return perfilCTR;
    }

    public void setPerfilCTR(PerfilController perfilCTR) {
        this.perfilCTR = perfilCTR;
    } 

    public List<Tarima> getTarima() {
        return tarima;
    }

    public void setTarima(List<Tarima> tarima) {
        this.tarima = tarima;
    }

    public TarimaController getTarimaCTR() {
        return tarimaCTR;
    }

    public void setTarimaCTR(TarimaController tarimaCTR) {
        this.tarimaCTR = tarimaCTR;
    }

    public HashMap getMap() {
        return map;
    }

    public void setMap(HashMap map) {
        this.map = map;
    }
    
    
}
