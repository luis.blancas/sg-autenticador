/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.org.citricos.mb;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import javax.faces.context.ExternalContext;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import mx.org.citicos.controller.ActivoController;
import mx.org.citicos.controller.CalibreController;
import mx.org.citicos.controller.CalidadController;
import mx.org.citicos.controller.MarcaController;
import org.primefaces.event.RowEditEvent;
import mx.org.citricos.entity.Pallet_desc;
import mx.org.citicos.service.PalletDetalleService;
import mx.org.citricos.entity.Pallet;
import org.primefaces.event.CellEditEvent;
import mx.org.citicos.controller.PalletController;  
import mx.org.citicos.controller.PalletDescController;
import mx.org.citicos.controller.TarimaController;
import mx.org.citricos.entity.Activo;
import mx.org.citricos.entity.Calibre;
import mx.org.citricos.entity.Calidad;
import mx.org.citricos.entity.Marca;
import mx.org.citricos.entity.Tarima;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.printing.PDFPageable;
/**
 *
 * @author luisa
 */
@ManagedBean(name="detalleMB")
@ViewScoped
public class DetalleMB implements Serializable
{
    private SessionMB  sessionMB =new SessionMB(); 
    private List<Pallet_desc> detalle; 
    private Pallet pallet; 
    private Pallet_desc seleccionado;
    private Pallet_desc editar;
    @ManagedProperty("#{detalleService}")
    private PalletDetalleService service; 
    @PostConstruct
    public void init() {
     
        FacesContext facesContext = FacesContext. getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        Map params=null;
        params = externalContext.getRequestParameterMap();
        Integer idX = new Integer((String) params.get("id"));
        PalletController tr=new PalletController();
        this.pallet=new Pallet();
        this.pallet=tr.getOne(idX);
        this.detalle=getDetalle(idX);
        System.out.println("contador::"+this.detalle.size()); 
    }

    public void onRowEdit(RowEditEvent event) {
        System.out.println("onRowEdit"); 
        String msj= service.modificaDetale(((Pallet_desc) event.getObject()).getId(), ((Pallet_desc) event.getObject()).getCajas(),
                    ((Pallet_desc) event.getObject()).getCdi(),((Pallet_desc) event.getObject()).getCalidad(),
                    ((Pallet_desc) event.getObject()).getCalibre(),((Pallet_desc) event.getObject()).getActivo(),
                    1);
        FacesMessage msg = new FacesMessage("Edicion de Detalle: "+msj, ((Pallet_desc) event.getObject()).getId().toString());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    public void imprimir(Integer i)
    {
        System.out.println("imprimir "+i); 
        HashMap map=new HashMap();
        ActivoController activoCTR=new ActivoController(); 
        service.setIdPalet(pallet.getId()); 
        List<Activo> activo = activoCTR.getAll();
        for(int j=0;j<activo.size();j++)
           map.put(activo.get(j).getId(),activo.get(j).getNombre());
        pallet.setActivos((String)map.get(pallet.getIdactivo()));
        imprimePallet( pallet,service.getTodos());
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "NOTA", "Se mando a imprimir Pallet ["+i+"]"));

    }
    public void onRowCancel(RowEditEvent event) { 
        FacesMessage msg = new FacesMessage("Edicion de Detalle cancelado", ((Pallet_desc) event.getObject()).getId().toString());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
     
    public void onCellEdit(CellEditEvent event) {
        
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue(); 
        if(newValue != null && !newValue.equals(oldValue)) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Cell Changed", "Old: " + oldValue + ", New:" + newValue);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void onAddNew() 
    {
        
        try
        { 
            FacesContext context = FacesContext.getCurrentInstance(); 
            Pallet_desc desc=service.getOneNew(); 
            context.getExternalContext().redirect("MasTransporte.xhtml?faces-redirect=true"); 
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public List<Pallet_desc> getDetalle(int idPallet) 
    { 
        service.setIdPalet(idPallet);  
        return service.getTodos(); 
    }

    public PalletDetalleService getService() {
        return service;
    }

    public void setService(PalletDetalleService service) {
        this.service = service;
    }

    public List<Pallet_desc> getDetalle() {
        return detalle;
    }

    public void setDetalle(List<Pallet_desc> detalle) {
        this.detalle = detalle;
    }

    public Pallet getPallet() {
        return pallet;
    }

    public void setPallet(Pallet pallet) {
        this.pallet = pallet;
    }
    private HashMap creaMapaCalibre(ArrayList<Calibre> all) {
        HashMap m=new HashMap();
        for(int i =0;i<all.size();i++)
            m.put(all.get(i).getId(),all.get(i).getNombre());
        return m;
    }
    private HashMap creaMapaTarima(ArrayList<Tarima> all) {
        HashMap m=new HashMap();
        for(int i =0;i<all.size();i++)
            m.put(all.get(i).getId(),all.get(i).getNombre());
        return m;
    }
    private HashMap creaMapaCalidad(ArrayList<Calidad> all) {
        HashMap m=new HashMap();
        for(int i =0;i<all.size();i++)
            m.put(all.get(i).getId(),all.get(i).getNombre());
        return m;
    }
    private HashMap creaMapaMarca(ArrayList<Marca> all) {
        HashMap m=new HashMap();
        for(int i =0;i<all.size();i++)
            m.put(all.get(i).getId(),all.get(i).getNombre());
        return m;
    }
    
    public void imprimePallet(Pallet maestro,List<Pallet_desc> detalle)
    {
        HashMap mapaCalibre=creaMapaCalibre((new CalibreController()).getAll());
        HashMap mapaTarima =creaMapaTarima ((new TarimaController()).getAll());
        HashMap mapaCalidad=creaMapaCalidad((new CalidadController()).getAll());
        HashMap mapaMarca  =creaMapaMarca  ((new MarcaController()).getAll());
        String cadena= ""+maestro.getId()+"|"+maestro.getNumero()+"|"+maestro.getCajas()+"|"+
                mapaMarca.get(maestro.getIdmarca())+"|"+
                mapaTarima.get(maestro.getIdtarima());
        String detail="";
        String listaDetalle="";
        for(int i=0;i<detalle.size();i++)
        {
            Pallet_desc det=detalle.get(i);
            detail= ""+det.getId()+"|"+det.getCajas()+"|"+det.getCdi()+"|"+
                mapaCalibre.get(det.getIdcalibre())+"|"+
                mapaCalidad.get(det.getIdcalidad());
            if((i+1)<detalle.size())
                detail=detail+"@"; 
            listaDetalle=listaDetalle+detail;
        }
        
        try 
        {
            listarImpresoras();
            ByteArrayOutputStream documentoBytes = crearDocumentoiText(maestro.getNumero(), cadena,listaDetalle);
            imprimir(documentoBytes,cadena,listaDetalle);
        }
        catch (IOException | PrinterException ex) 
        {
            ex.printStackTrace();
        }
        
    }
    /**
     * Envia a imprimir el ByteArrayOutoutStream creado de un documento iText
     *
     * @param documentoBytes
     * @throws IOException
     * @throws PrinterException
     */
    private  void imprimir(ByteArrayOutputStream documentoBytes,String cadena,String detalle) throws IOException, PrinterException {

        // Aqui convertimos la el arreglo de salida a uno de entrada que podemos
        // mandar a la impresora
        ByteArrayInputStream bais = new ByteArrayInputStream(documentoBytes.toByteArray());

        // Creamos un PDDocument con el arreglo de entrada que creamos        
        PDDocument document = PDDocument.load(bais);

        PrintService myPrintService = this.findPrintService(firstImpresora());
        PrinterJob printerJob = PrinterJob.getPrinterJob();

        printerJob.setPageable(new PDFPageable(document));
        printerJob.setPrintService(myPrintService);

        printerJob.print();

    }

    /**
     * Muestra en pantalla la lista de todas las impresoras disponibles en el
     * sistema
     */
    private void listarImpresoras() {
        PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, null);
        System.out.println("Lista de impresoras disponibles");

        for (PrintService printService : printServices) {
            System.out.println("\t" + printService.getName());
        }
    }
    private String  firstImpresora() {
        PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, null);
        System.out.println("Lista de impresoras disponibles");
        String elemento="";
        for (PrintService printService : printServices) {
            return printService.getName();
        }
        return ""; 
    }

    /**
     * Nos regresa el PrintService que representa la impresora con el nombre que
     * le indiquemos
     * @param printerName nombre de la impresora que deseamos usar
     * @return PrintService que representa la impresora que deseamos usar
     */
    private PrintService findPrintService(String printerName) {
        PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, null);
        for (PrintService printService : printServices) {
            System.out.println(printService.getName());

            if (printService.getName().trim().equals(printerName)) {
                return printService;
            }
        }
        return null;
    }
    private static byte[] getImageQRImage(String text, String ext, int width, int height) {
    	byte[] pngData = null;
		try {
			QRCodeWriter qrCodeWriter = new QRCodeWriter();
		    BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, width, height);
		    ByteArrayOutputStream pngOutputStream = new ByteArrayOutputStream();
		    MatrixToImageWriter.writeToStream(bitMatrix, ext, pngOutputStream);
		    pngData = pngOutputStream.toByteArray();  
		} catch(WriterException wex) {
			System.out.println(wex.getMessage());
		} catch(IOException ioe) {
			System.out.println(ioe.getMessage());
		} 
		 return pngData;
	}
    private byte[] extractBytes(File file) throws IOException 
    {
    	byte[] bytes =null;
    	try {

            //File file = new File("C:\\images\\origen\\foto1.jpg");
            System.out.println(file.exists() + "!!");
            
            FileInputStream fis = new FileInputStream(file);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];
            try {
                for (int readNum; (readNum = fis.read(buf)) != -1;) {
                    bos.write(buf, 0, readNum); 
                    System.out.println("read " + readNum + " bytes,");
                }
            } catch (IOException ex) {
            	System.out.println("Error File JPG:"+ex.getMessage());
            }
            
            //bytes is the ByteArray we need
            bytes = bos.toByteArray();
	    }
    	catch (FileNotFoundException e) 
    	{
	        e.printStackTrace();
	    }
    	catch (IOException e) 
    	{
	        e.printStackTrace();
	    }
        return bytes;
    }
    private ByteArrayOutputStream crearDocumentoiText(String id,String cadena,String detalle) {
        // Es en este ByteArrayOutputStream donde se pone el documento una vez 
        // que se llama a documento.close()
        ByteArrayOutputStream documentoBytes = new ByteArrayOutputStream();

        PdfWriter pdfWriter = new PdfWriter(documentoBytes);
        PdfDocument pdfDoc = new PdfDocument(pdfWriter);
        Document documento = new Document(pdfDoc, PageSize.LETTER);
        byte[] bytes=null;
        ImageData img2=ImageDataFactory.create(getImageQRImage(id,"png",130,130));
        documento.add(this.crearImages(new Image(img2)));
        documento.add(new Paragraph("Servicios de Citricos Cadillo S.A. de C.V."));
        documento.add(this.crearTablaCabecera(cadena,  id));
        documento.add(new Paragraph("Información de Pallet :  "+id));
        documento.add(this.crearTablaDetalle(detalle));
        documento.close();

        return documentoBytes;
    }
 
    private Table crearTablaCabecera(String cadena,String id) 
    {
        float[] anchos = {25F, 30F, 30F,30F,30F};
        Table table = new Table(anchos);
        table.setWidth(500F);
        table.addCell("Id");
        table.addCell("Número Pallet");
        table.addCell("Cajas");
        table.addCell("Tarima");
        table.addCell("Marca");
        StringTokenizer tokens=new StringTokenizer(cadena,"|");
        int cont=tokens.countTokens();
        
        if(cont ==5)
        {
        	String tk1=tokens.nextToken();
        	String tk2=tokens.nextToken();
        	String tk3=tokens.nextToken();
        	String tk4=tokens.nextToken();
        	String tk5=tokens.nextToken();
        	if(tk2.equals(id))
        	{
	        	table.addCell(tk1);
	        	if(tk2.equals(id))
	        		table.addCell(id);
	        	else
	        		table.addCell("Problemas con Id");
	            table.addCell(tk3);
	            table.addCell(tk4);
	            table.addCell(tk5);	
        	}	
        }
        return table;
    }
    private Table crearImages(Image img2) 
    {
        float[] anchos = {100F,100F, 100F};
        Table table = new Table(anchos);
        table.setWidth(100F);
        table.addCell(img2);
        return table;
    }
    private Table crearTablaDetalle(String cadena) 
    {
        float[] anchos = {25F, 25F, 35F,35F,35F};
        Table table = new Table(anchos);
        table.setWidth(500F);
        table.addCell("Id");
        table.addCell("Cajas");
        table.addCell("CDI");
        table.addCell("Calibre");
        table.addCell("Calidad");
        StringTokenizer tokensD=new StringTokenizer(cadena,"@");
        int contD=tokensD.countTokens();
        for(int i=0;i<contD;i++)
        {
        	String detalle=tokensD.nextToken();
	        StringTokenizer tokens=new StringTokenizer(detalle,"|");
	        int cont=tokens.countTokens();
	        
	        if(cont ==5)
	        { 
	        	for(int f=0;f<5;f++)
		        	table.addCell(tokens.nextToken());	 
	        }
        }
        return table;
    } 

    public SessionMB getSessionMB() {
        return sessionMB;
    }

    public void setSessionMB(SessionMB sessionMB) {
        this.sessionMB = sessionMB;
    }

    public Pallet_desc getSeleccionado() {
        return seleccionado;
    }

    public void setSeleccionado(Pallet_desc seleccionado) {
        this.seleccionado = seleccionado;
    }

    public Pallet_desc getEditar() 
    {
        this.editar=new Pallet_desc();
        FacesContext facesContext = FacesContext. getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        Map params=null;
        params = externalContext.getRequestParameterMap();
        try
        {
            Integer id = new Integer((String) params.get("id"));
            Integer idx = new Integer((String) params.get("idx"));
            PalletController tr=new PalletController();
            this.pallet=new Pallet();
            this.pallet=tr.getOne(id);
            this.detalle=getDetalle(id);
            if(this.detalle.size()>0)
            {
                for(int i=0;i<this.detalle.size();i++)
                {
                    Pallet_desc detalle=this.detalle.get(i);
                    if(detalle.getId().equals(idx)  && detalle.getIdtarima().equals(id))
                    {
                        this.editar = this.detalle.get(i);
                        return editar;
                    }
                }
            }
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
        return this.editar;
    }

    public void setEditar(Pallet_desc editar) {
        this.editar = editar;
    }

     
    public void guardarDetalleEditado(
            Integer cajas,
            Integer idcalibre,
            Integer idcalidad,
            Integer idactivo,
            String cdi,
            Integer id,
            Integer idPallet,
            Integer usuario)
    {
        
        Integer  cajas_     = cajas;
        Integer idcalibre_ = idcalibre;
        Integer idcalidad_ = idcalidad;
        Integer idactivo_  = idactivo;
        Integer id_        = id;
        Integer idPallet_  = idPallet;
        Integer usuario_   = usuario;
        try
        {
            if(idcalibre_  != null && 
                    idcalidad_ != null && 
                        idactivo_ != null )
            {
                PalletController controller=new PalletController();
                PalletDescController detalle =new PalletDescController(idPallet);
                detalle.updateRecor(id_,cajas_,idcalibre_,idcalidad_,cdi,idactivo_,usuario_);
                Pallet pallet = controller.getOne(idPallet_);
                FacesContext context = FacesContext.getCurrentInstance();
                context.getExternalContext().redirect("Listar.xhtml?faces-redirect=true");
            }
        }
        catch(Exception e)
        {
            System.out.println("e::"+e.getMessage());
        }
    } 
}
