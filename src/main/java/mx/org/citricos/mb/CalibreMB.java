/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.org.citricos.mb; 
import java.io.IOException;
import java.io.Serializable;
import java.util.List; 
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct; 
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext; 
import mx.org.citicos.controller.CalibreController; 
import mx.org.citricos.entity.Calibre; 

/**
 *
 * @author luis Adrian
 */ 
@ManagedBean (name = "calibreMB")
@RequestScoped
public class CalibreMB implements Serializable
{
    private SessionMB  sessionMB =new SessionMB();
    CalibreController controller=new CalibreController();
    private final FacesContext context = FacesContext.getCurrentInstance();
    private List<Calibre> todos; 
    private Calibre registroSeleccionado;
    private Calibre registroEditar;
    private Calibre registroAnexar;
    private Calibre registroBorrado; 
    
    private Integer id;
    private String  descripcion; 
    
    @PostConstruct
    public void init() {
        registroSeleccionado=new Calibre();
        this.todos=controller.getAll();
    }
    public List<Calibre> getCalidads() {
        
        this.todos= controller.getAll();
        return todos;
    } 
    public void seleccionEditar(Calibre editar )
    {
        this.registroEditar=new Calibre();
        this.registroEditar.setId(editar.getId()); 
        this.registroEditar.setNombre(editar.getNombre()); 
        System.out.println("seleccion Editar "+editar.toString());
    }
    public void seleccionBorrado(Calibre borrado )
    {
        this.registroBorrado=new Calibre();
        this.registroBorrado.setId(borrado.getId());
        this.registroBorrado.setNombre(borrado.getNombre());
        System.out.println("seleccionBorrado "+borrado.toString());
    } 
    public void editarregistro(Integer id,Calibre o)
    {
        try
        { 
            System.out.println("Editar id    :"+id);
            System.out.println("Editar Nombre:"+o.getNombre());
            controller.updateRecordUsr(o.getNombre(),id,sessionMB.getUsuariofinal().getId());
            context.getExternalContext().redirect("Listarcalibre.xhtml?faces-redirect=true");
        }
        catch (IOException ex) {
            Logger.getLogger(CalibreMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void anexarregistro()
    {
        try
        { 
            System.out.println("Anexar Nombre:"+this.descripcion);
            controller.insertRecordUsr(descripcion,sessionMB.getUsuariofinal().getId());
            context.getExternalContext().redirect("Listarcalibre.xhtml?faces-redirect=true");
        }
        catch (IOException ex) 
        {
            Logger.getLogger(CalibreMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void borrarregistro(Calibre registroAborrar)
    {
        try
        { 
            controller.deleteRecord(registroAborrar.getId(),sessionMB.getUsuariofinal().getId());
            System.out.println("seleccion Borrar "+registroAborrar.toString()); 
            this.todos= controller.getAll();
            context.getExternalContext().redirect("Listarcalibre.xhtml?faces-redirect=true");
            context.addMessage(null, new FacesMessage("Borrar", "Registro  borrado con exito :  "+registroAborrar.getNombre()));
        }
        catch (IOException ex) {
            Logger.getLogger(CalibreMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /*-------------------------------------------------------------------------------------*/

    public CalibreController getController() {
        return controller;
    }

    public void setController(CalibreController controller) {
        this.controller = controller;
    }

    public List<Calibre> getTodos() {
        return todos;
    }

    public void setTodos(List<Calibre> todos) {
        this.todos = todos;
    }

    public Calibre getRegistroSeleccionado() {
        return registroSeleccionado;
    }

    public void setRegistroSeleccionado(Calibre registroSeleccionado) {
        this.registroSeleccionado = registroSeleccionado;
    }

    public Calibre getRegistroEditar() {
        return registroEditar;
    }

    public void setRegistroEditar(Calibre registroEditar) {
        this.registroEditar = registroEditar;
    }

    public Calibre getRegistroAnexar() {
        return registroAnexar;
    }

    public void setRegistroAnexar(Calibre registroAnexar) {
        this.registroAnexar = registroAnexar;
    }

    public Calibre getRegistroBorrado() {
        return registroBorrado;
    }

    public void setRegistroBorrado(Calibre registroBorrado) {
        this.registroBorrado = registroBorrado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    
}
