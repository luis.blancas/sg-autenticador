/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.org.citricos.mb; 
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable; 
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List; 
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct; 
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext; 
import mx.org.citicos.controller.ProductorController;  
import mx.org.citicos.controller.TipoProductorController;  
import mx.org.citicos.controller.AgronomoController;  
import mx.org.citricos.entity.Tipop;
import mx.org.citricos.entity.Tarima;
import mx.org.citricos.entity.Productor;  
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author luis Adrian
 */ 
@ManagedBean (name = "productorMB")
@RequestScoped
public class ProductorMB implements Serializable
{
    private SessionMB  sessionMB =new SessionMB();
    ProductorController controller=new ProductorController();
    TipoProductorController controllertp=new TipoProductorController();
    AgronomoController controllerag=new AgronomoController();
    private final FacesContext context = FacesContext.getCurrentInstance();
    private List<Productor> todos;
    private Productor registroSeleccionado;
    private Productor registroEditar;
    private Productor registroAnexar;
    private Productor registroBorrado;
    private String rfc;
    private String nombre;
    private String telefono1;
    private String telefono2;
    private Integer tipo;
    private Integer agronomo;
    private String nombre_archivo;
    private String direccion;
    private List<Tipop>   tipos;
    private List<Tarima>   agronomos;
    private StreamedContent graphicImage;
    @PostConstruct
    public void init() 
    {
        registroSeleccionado=new Productor();
        this.todos=controller.getAll();
        this.tipos=controllertp.getAll();
    }
    public List<Productor> getCalidads() 
    {    
        this.todos= controller.getAll();
        return todos;
    } 
    public void seleccionEditar(Productor editar )
    {
        this.registroEditar=new Productor();
        this.registroEditar.setId(editar.getId()); 
        this.registroEditar.setRfc(editar.getRfc()); 
        this.registroEditar.setNombre(editar.getNombre());
        this.registroEditar.setDireccion(editar.getDireccion());
        this.registroEditar.setTelefono1(editar.getTelefono1());
        this.registroEditar.setTelefono2(editar.getTelefono2());
        this.registroEditar.setTipo(editar.getTipo());
        this.registroEditar.setTipos(editar.getTipos());
        
        System.out.println("seleccion Editar "+editar.toString());
    }
    public void seleccionBorrado(Productor editar )
    { 
        this.registroBorrado=new Productor();
        this.registroBorrado.setId(editar.getId()); 
        this.registroBorrado.setRfc(editar.getRfc()); 
        this.registroBorrado.setNombre(editar.getNombre());
        this.registroBorrado.setDireccion(editar.getDireccion());
        this.registroBorrado.setTelefono1(editar.getTelefono1());
        this.registroBorrado.setTelefono2(editar.getTelefono2());
        this.registroBorrado.setTipo(editar.getTipo());
        this.registroBorrado.setTipos(editar.getTipos());
    } 
    public void editarRegistro(Integer id, Productor o)
    {
        try
        { 
            System.out.println("Editar id    :"+id); 
            controller.updateRecord(o.getRfc(),o.getNombre(),o.getDireccion(),o.getTelefono1(),o.getTelefono2(),o.getTipo() ,
                    sessionMB.getUsuariofinal().getId(),id);
            context.getExternalContext().redirect("Listar.xhtml?faces-redirect=true");
        }
        catch (IOException ex) {
            Logger.getLogger(ProductorMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void anexarRegistro()
    {
        try
        { 
            System.out.println("Anexar Nombre:"+this.nombre); 
            controller.insertRecord(rfc,nombre,direccion,telefono1,telefono2,tipo,
                    sessionMB.getUsuariofinal().getId());
            context.getExternalContext().redirect("Listar.xhtml?faces-redirect=true");
        }
        catch (IOException ex) 
        {
            Logger.getLogger(ProductorMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void borrarRegistro(Productor registroAborrar)
    {
        try
        { 
            controller.deleteRecord(registroAborrar.getId(),
                    sessionMB.getUsuariofinal().getId());
            System.out.println("seleccion Borrar "+registroAborrar.toString()); 
            this.todos= controller.getAll();
            context.getExternalContext().redirect("Listar.xhtml?faces-redirect=true");
            context.addMessage(null, new FacesMessage("Borrar", "Registro  borrado con exito :  "+registroAborrar.getNombre()));
        }
        catch (IOException ex) {
            Logger.getLogger(ProductorMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /*-------------------------------------------------------------------------------------*/

    public ProductorController getController() {
        return controller;
    }

    public void setController(ProductorController controller) {
        this.controller = controller;
    }

    public List<Productor> getTodos() {
        return todos;
    }

    public void setTodos(List<Productor> todos) {
        this.todos = todos;
    }

    public Productor getRegistroSeleccionado() {
        return registroSeleccionado;
    }

    public void setRegistroSeleccionado(Productor registroSeleccionado) {
        this.registroSeleccionado = registroSeleccionado;
    }

    public Productor getRegistroEditar() {
        return registroEditar;
    }

    public void setRegistroEditar(Productor registroEditar) {
        this.registroEditar = registroEditar;
    }

    public Integer getAgronomo() {
        return agronomo;
    }

    public void setAgronomo(Integer agronomo) {
        this.agronomo = agronomo;
    }
    
    
    public Productor getRegistroAnexar() {
        return registroAnexar;
    }

    public void setRegistroAnexar(Productor registroAnexar) {
        this.registroAnexar = registroAnexar;
    }

    public Productor getRegistroBorrado() {
        return registroBorrado;
    }

    public void setRegistroBorrado(Productor registroBorrado) {
        this.registroBorrado = registroBorrado;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono1() {
        return telefono1;
    }

    public void setTelefono1(String telefono1) {
        this.telefono1 = telefono1;
    }

    public String getTelefono2() {
        return telefono2;
    }

    public void setTelefono2(String telefono2) {
        this.telefono2 = telefono2;
    }

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }

    public String getNombre_archivo() {
        return nombre_archivo;
    }

    public void setNombre_archivo(String nombre_archivo) {
        this.nombre_archivo = nombre_archivo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public TipoProductorController getControllertp() {
        return controllertp;
    }

    public void setControllertp(TipoProductorController controllertp) {
        this.controllertp = controllertp;
    }

    public List getTipos() {
        return tipos;
    }

    public void setTipos(List tipos) {
        this.tipos = tipos;
    }
    
    private UploadedFile file;
 
    public UploadedFile getFile() {
        return file;
    }
 
    public void setFile(UploadedFile file) {
        this.file = file;
    }
     
    public void upload() {
        if(file != null) {
            FacesMessage message = new FacesMessage("Succesful", file.getFileName() + " is uploaded.");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
    }
     
 
    public void handleFileUpload(FileUploadEvent event) 
    {
        String destination = "C:\\citricos\\tmp\\";
        try 
            {
            copyFile(event.getFile().getFileName(), event.getFile().getInputstream(),destination);
            FacesMessage message = new FacesMessage("El archivo se ha subido con éxito! : ");
            FacesContext.getCurrentInstance().addMessage(null, message);
        } 
        catch (IOException e) 
        {
            e.printStackTrace();
        }

    }
    public void copyFile(String fileName, InputStream in, String destination) 
    {
        try 
        {
            this.nombre=destination + fileName;
            OutputStream out = new FileOutputStream(new File(destination + fileName));
            int read = 0;
            byte[] bytes = new byte[1024];
            while ((read = in.read(bytes)) != -1) 
            {
                out.write(bytes, 0, read);
            }
            in.close();
            out.flush();
            out.close();
            System.out.println("El archivo se ha creado con éxito!");

            DateFormat dateFormat = new SimpleDateFormat("yyyy–MM–dd HH_mm_ss");
            Date date = new Date();
            String ruta1 = destination + fileName;
            String ruta2 = destination +fileName;
            System.out.println("Archivo: "+ruta1+" Renombrado a: "+ruta2);
            File archivo=new File(ruta1);
            archivo.renameTo(new File(ruta2));
        } 
        catch (IOException e) 
        {
            System.out.println(e.getMessage());
        }
    }
    public void prepararImagen(String fileName)
    {
        try {
            if(fileName == null)
                fileName=  "CADILLO";
            if(fileName.length()==0)
                fileName=  "CADILLO";
            String path = "C:\\citricos\\tmp\\"+fileName+".jpg";
            graphicImage = new DefaultStreamedContent(new FileInputStream(path), "image/png");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductorMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

     public StreamedContent getGraphicImage() 
     {
        prepararImagen(registroEditar.getRfc());
        return graphicImage;
    }

    public void setGraphicImage(StreamedContent graphicImage) {
        this.graphicImage = graphicImage;
    }

    public List<Tarima> getAgronomos() 
    {
        agronomos=controllerag.getAll();
        return agronomos;
    }

    public void setAgronomos(List<Tarima> agronomos) {
        this.agronomos = agronomos;
    }
    
    
}