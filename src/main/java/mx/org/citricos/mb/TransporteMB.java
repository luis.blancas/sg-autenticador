/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.org.citricos.mb; 
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List; 
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct; 
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean; 
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext; 
import mx.org.citicos.controller.ClienteController;
import mx.org.citicos.controller.TransporteController; 
import mx.org.citicos.controller.TransportistaController; 
import mx.org.citricos.entity.Cliente;
import mx.org.citricos.entity.Transporte; 
import mx.org.citricos.entity.Transportista;

/**
 *
 * @author luis Adrian
 */ 
@ManagedBean (name = "transporteMB")
@RequestScoped
public class TransporteMB implements Serializable
{ 
    private SessionMB  sessionMB =new SessionMB();
    TransporteController controller=new TransporteController();
    private final FacesContext context = FacesContext.getCurrentInstance();
    private List<Transporte> todos;
    private List<Transportista> transportistas;
    private Transporte registroSeleccionado;
    private Transporte registroEditar;
    private Transporte registroAnexar;
    private Transporte registroBorrado;
    private Integer id;
    private String numerotermo;
    private String numerosello;
    private String lector_temp;
    private String lote;
    private Date fecha;
    private Integer idcliente;
    private Integer idtransportista;
    private Integer idactivo;
    private String numero;
    private List<Cliente> clientes;  
    @PostConstruct
    public void init() {
        registroSeleccionado=new Transporte();
        this.todos=controller.getAll();
    }
    public List<Transporte> getCalidads() 
    {    
        this.todos= controller.getAll();
        return todos;
    } 
    public void seleccionEditar(Transporte editar )
    {
        this.registroEditar=new Transporte();
        this.registroEditar.setId(editar.getId()); 
        this.registroEditar.setNumerotermo(editar.getNumerotermo()); 
        this.registroEditar.setNumerosello(editar.getNumerotermo());
        this.registroEditar.setLector_temp(editar.getNumerotermo());
        this.registroEditar.setLote(editar.getNumerotermo());
        this.registroEditar.setFecha(editar.getNumerotermo());
        this.registroEditar.setIdcliente(editar.getIdcliente());
        this.registroEditar.setIdtransportista(editar.getIdtransportista());
        this.registroEditar.setIdactivo(editar.getIdactivo());
        this.registroEditar.setNumero(editar.getNumero());
    }
    public void seleccionBorrado(Transporte editar )
    {
        this.registroEditar=new Transporte();
        this.registroEditar.setId(editar.getId()); 
        this.registroEditar.setNumerotermo(editar.getNumerotermo()); 
        this.registroEditar.setNumerosello(editar.getNumerotermo());
        this.registroEditar.setLector_temp(editar.getNumerotermo());
        this.registroEditar.setLote(editar.getNumerotermo());
        this.registroEditar.setFecha(editar.getNumerotermo());
        this.registroEditar.setIdcliente(editar.getIdcliente());
        this.registroEditar.setIdtransportista(editar.getIdtransportista());
        this.registroEditar.setIdactivo(editar.getIdactivo());
    } 
    public void editarRegistro(Integer idTr,Integer idCtl,Transporte o)
    {
        try
        { 
            System.out.println("Editar id    :"+id);
            System.out.println("Editar Nombre:"+o.getNumero());
            controller.updateRecord(o.getNumerotermo(),o.getNumerosello(),o.getLector_temp(),o.getLote(),o.getFecha(),idCtl,
                    sessionMB.getUsuariofinal().getId(),idTr);
            context.getExternalContext().redirect("Listar.xhtml?faces-redirect=true");
        }
        catch (IOException ex) {
            Logger.getLogger(TransporteMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void anexarRegistro(Integer idTr,Integer idCtl)
    {
        try
        { 
            System.out.println("Anexar Nombre:"+this.numerotermo+"  ->  "+idTr+"  --->ctl :"+
                    idCtl +"   --> user:"+sessionMB.getUsuariofinal().getId());
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            String fechaN=format.format(fecha);
            
            controller.insertRecord(numerotermo,numerosello,lector_temp,lote,fechaN,idCtl,
                    sessionMB.getUsuariofinal().getId(),idTr);
            context.getExternalContext().redirect("Listar.xhtml?faces-redirect=true");
        }
        catch (IOException ex) 
        {
            Logger.getLogger(TransporteMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void borrarRegistro(Transporte registroAborrar)
    {
        try
        { 
            controller.deleteRecord(registroAborrar.getId(),
                    sessionMB.getUsuariofinal().getId());
            System.out.println("seleccion Borrar "+registroAborrar.toString()); 
            this.todos= controller.getAll();
            context.getExternalContext().redirect("Listar.xhtml?faces-redirect=true");
            context.addMessage(null, new FacesMessage("Borrar", "Registro  borrado con exito :  "+registroAborrar.getNumero()));
        }
        catch (IOException ex) {
            Logger.getLogger(TransporteMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /*-------------------------------------------------------------------------------------*/
    public List<Cliente> getClientes() {
        ClienteController cont =new ClienteController();
        clientes= cont.getAll();
        return clientes;
    }

    public TransporteController getController() {
        return controller;
    }

    public void setController(TransporteController controller) {
        this.controller = controller;
    }

    public List<Transporte> getTodos() {
        return todos;
    }

    public void setTodos(List<Transporte> todos) {
        this.todos = todos;
    }

    public Transporte getRegistroSeleccionado() {
        return registroSeleccionado;
    }

    public void setRegistroSeleccionado(Transporte registroSeleccionado) {
        this.registroSeleccionado = registroSeleccionado;
    }

    public Transporte getRegistroEditar() {
        return registroEditar;
    }

    public void setRegistroEditar(Transporte registroEditar) {
        this.registroEditar = registroEditar;
    }

    public Transporte getRegistroAnexar() {
        return registroAnexar;
    }

    public void setRegistroAnexar(Transporte registroAnexar) {
        this.registroAnexar = registroAnexar;
    }

    public Transporte getRegistroBorrado() {
        return registroBorrado;
    }

    public void setRegistroBorrado(Transporte registroBorrado) {
        this.registroBorrado = registroBorrado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumerotermo() {
        return numerotermo;
    }

    public void setNumerotermo(String numerotermo) {
        this.numerotermo = numerotermo;
    }

    public String getNumerosello() {
        return numerosello;
    }

    public void setNumerosello(String numerosello) {
        this.numerosello = numerosello;
    }

    public String getLector_temp() {
        return lector_temp;
    }

    public void setLector_temp(String lector_temp) {
        this.lector_temp = lector_temp;
    }

    public String getLote() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Integer getIdcliente() {
        return idcliente;
    }

    public void setIdcliente(Integer idcliente) {
        this.idcliente = idcliente;
    }

    public Integer getIdtransportista() {
        return idtransportista;
    }

    public void setIdtransportista(Integer idtransportista) {
        this.idtransportista = idtransportista;
    }

    public Integer getIdactivo() {
        return idactivo;
    }

    public void setIdactivo(Integer idactivo) {
        this.idactivo = idactivo;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public List<Transportista> getTransportistas() {
        TransportistaController ctr=new TransportistaController();
        transportistas = ctr.getAll();
        return transportistas;
    }

    public void setTransportistas(List<Transportista> transportistas) {
        this.transportistas = transportistas;
    }
 
    
}