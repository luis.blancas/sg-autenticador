/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.org.citricos.mb; 
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct; 
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean; 
import javax.faces.bean.RequestScoped; 
import javax.faces.context.FacesContext;  
import mx.org.citicos.controller.ActivoController;
import mx.org.citicos.controller.CDIController;
import mx.org.citicos.controller.CalibreController;
import mx.org.citicos.controller.CalidadController;
import mx.org.citicos.controller.MarcaController;
import mx.org.citicos.controller.PalletController;   
import mx.org.citicos.controller.PalletDescController;
import mx.org.citicos.controller.TarimaController;
import mx.org.citicos.controller.TransporteController; 
import mx.org.citricos.entity.Activo;
import mx.org.citricos.entity.Marca;
import mx.org.citricos.entity.Pallet;
import mx.org.citricos.entity.Tarima;
import mx.org.citricos.entity.Cdi;
import mx.org.citricos.entity.Calidad;
import mx.org.citricos.entity.Calibre;
import mx.org.citricos.entity.Pallet_desc;
import mx.org.citricos.entity.Transporte;
import org.primefaces.context.RequestContext;

/**
 *
 * @author luis Adrian
 */
@ManagedBean(name="palletMB")
@RequestScoped
public class PalletMB implements Serializable
{ 
    private SessionMB  sessionMB =new SessionMB();
    PalletController controller=new PalletController();
    private final FacesContext context = FacesContext.getCurrentInstance();
    private List<Pallet> todos; 
    private Pallet registroSeleccionado;
    private Pallet registroEditar;
    private Pallet registroAnexar;
    private Pallet registroBorrado;
    private Pallet registroTransporte;
    private Pallet registroeditardetalle;
    private Pallet_desc detalle;
    private Pallet_desc detalleNuevo;
    private Pallet_desc detalleEdita;
    private Integer    id;
    private String     qr;
    private String     numero;
    private Integer    cajas;
    private Integer    cajasDetalle;
    private Integer    idactivo;
    private Integer    idtarima;
    private Integer    idmarca;
    private Integer    idcalidad;
    private Integer    idcalibre;
    private Integer    idcdi;
    private Integer    idtransporte; 
    private List<Tarima> cat_tarima;
    private List<Marca>  cat_marca;
    private List<Calibre> cat_calibre;
    private List<Cdi>  cat_cdi;
    private List<Calidad>  cat_calidad;
    private List<Transporte> cat_transporte;
    private List<Activo> cat_activo;
    private Integer    tr_id;
    private Integer    tr_idtransporte;
    
    private Integer    detalle_cajas;
    private String     detalle_cdi;
    private Integer    detalle_calidad;
    private Integer    detalle_calibre;
    private Integer    detalle_activo;
    
    private Integer    pallet_editar_id;
    private String     pallet_editar_ta;
    private String     pallet_editar_ma;
    
    @PostConstruct
    public void init() {
        registroSeleccionado=new Pallet();
        this.todos=controller.getAll();
        
        TarimaController ctar  = new TarimaController();
        cat_tarima = ctar.getAll();
        MarcaController cmar  = new MarcaController();
        cat_marca = cmar.getAll();
        TransporteController ctra  = new TransporteController();
        cat_transporte = ctra.getAll();
        
        CalibreController cCal  = new CalibreController();
        cat_calibre = cCal.getAll();
        CalidadController cCalid  = new CalidadController();
        cat_calidad = cCalid.getAll();
        
        CDIController cCDI  = new CDIController();
        cat_cdi = cCDI.getAll();
        
        ActivoController conActivo =new ActivoController();
        cat_activo = conActivo.getAll();
    }
    public List<Pallet> getCalidads() 
    {    
        this.todos= controller.getAll();
        
        return todos;
    } 
    public void seleccionEditar(Pallet editar )
    {
        this.registroEditar=new Pallet();
        this.registroEditar.setId(editar.getId()); 
        this.registroEditar.setQr(editar.getQr()); 
        this.registroEditar.setCajas(editar.getCajas());
        this.registroEditar.setIdactivo(editar.getIdactivo());
        this.registroEditar.setIdtarima(editar.getIdtarima());
        this.registroEditar.setIdmarca(editar.getIdmarca());
        this.registroEditar.setIdtransporte(editar.getIdtransporte());
        this.registroEditar.setNumero(editar.getNumero());  
        System.out.println("seleccion registroEditar "+registroEditar.toString());
        
        this.registroTransporte=new Pallet();
        this.registroTransporte.setId(editar.getId()); 
        this.registroTransporte.setQr(editar.getQr()); 
        this.registroTransporte.setCajas(editar.getCajas());
        this.registroTransporte.setIdactivo(editar.getIdactivo());
        this.registroTransporte.setIdtarima(editar.getIdtarima());
        this.registroTransporte.setIdmarca(editar.getIdmarca());
        this.registroTransporte.setIdtransporte(editar.getIdtransporte());
        this.registroTransporte.setNumero(editar.getNumero()); 
        System.out.println("seleccion registroTransporte "+registroTransporte.toString());
        
        
    }
    public String seleccionEditarMovil(Pallet editar )
    {
        this.registroEditar=new Pallet();
        this.registroEditar.setId(editar.getId()); 
        this.registroEditar.setQr(editar.getQr()); 
        this.registroEditar.setCajas(editar.getCajas());
        this.registroEditar.setIdactivo(editar.getIdactivo());
        this.registroEditar.setIdtarima(editar.getIdtarima());
        this.registroEditar.setIdmarca(editar.getIdmarca());
        this.registroEditar.setIdtransporte(editar.getIdtransporte());
        this.registroEditar.setNumero(editar.getNumero());  
        System.out.println("seleccion registroEditar "+registroEditar.toString());
        
        this.registroTransporte=new Pallet();
        this.registroTransporte.setId(editar.getId()); 
        this.registroTransporte.setQr(editar.getQr()); 
        this.registroTransporte.setCajas(editar.getCajas());
        this.registroTransporte.setIdactivo(editar.getIdactivo());
        this.registroTransporte.setIdtarima(editar.getIdtarima());
        this.registroTransporte.setIdmarca(editar.getIdmarca());
        this.registroTransporte.setIdtransporte(editar.getIdtransporte());
        this.registroTransporte.setNumero(editar.getNumero()); 
        System.out.println("seleccion registroTransporte "+registroTransporte.toString());
        
        return "pm:registroEditar";
    }
    public String seleccionNuevoMobileDetalle(int id)
    {
        this.registroAnexar=new Pallet(); 
        this.registroAnexar.setId(-1); 
        this.registroAnexar.setQr("");
        this.registroAnexar.setCajas(0);
        this.registroAnexar.setIdactivo(0);
        this.registroAnexar.setIdtarima(0);
        this.registroAnexar.setIdmarca(0);
        this.registroAnexar.setIdtransporte(0);
        this.registroAnexar.setNumero("");
        Pallet pt=controller.getOne(id);
        seleccionEditar(pt);
        return "pm:registroNuevo";
    }
    public String seleccionNuevoMobile(Pallet editar)
    {
        this.registroAnexar=new Pallet(); 
        this.registroAnexar.setId(-1); 
        this.registroAnexar.setQr("");
        this.registroAnexar.setCajas(0);
        this.registroAnexar.setIdactivo(0);
        this.registroAnexar.setIdtarima(0);
        this.registroAnexar.setIdmarca(0);
        this.registroAnexar.setIdtransporte(0);
        this.registroAnexar.setNumero("");
        seleccionEditar(editar);
        return "pm:registroNuevo";
    }
    public String seleccionNuevoMobile()
    {
        this.registroAnexar=new Pallet(); 
        this.registroAnexar.setId(-1); 
        this.registroAnexar.setQr("");
        this.registroAnexar.setCajas(0);
        this.registroAnexar.setIdactivo(0);
        this.registroAnexar.setIdtarima(0);
        this.registroAnexar.setIdmarca(0);
        this.registroAnexar.setIdtransporte(0);
        this.registroAnexar.setNumero("");
        return "pm:registroNuevo";
    }
    public String seleccionNuevoDetallMobile()
    {
        detalle_cajas=0;
        detalle_cdi="";
        detalle_calidad=0;
        detalle_calibre=0;
        detalle_activo=0;
        return "pm:registroNuevo";
    }
    
    public void seleccionPalletNuevoD(Pallet editar )
    {
        this.registroEditar=new Pallet();
        this.registroEditar.setId(editar.getId()); 
        this.registroEditar.setQr(editar.getQr()); 
        this.registroEditar.setCajas(editar.getCajas());
        this.registroEditar.setIdactivo(editar.getIdactivo());
        this.registroEditar.setIdtarima(editar.getIdtarima());
        this.registroEditar.setIdmarca(editar.getIdmarca());
        this.registroEditar.setIdtransporte(editar.getIdtransporte());
        this.registroEditar.setNumero(editar.getNumero());  
        System.out.println("seleccion registroEditar "+registroEditar.toString());
           
        this.detalleNuevo = new Pallet_desc();
        this.detalleNuevo.setId(0); 
        this.detalleNuevo.setActivo("");
        this.detalleNuevo.setCajas(0);
        this.detalleNuevo.setCalibre("");
        this.detalleNuevo.setCalidad("");
        this.detalleNuevo.setCdi("");
        this.detalleNuevo.setIdactivo(1);
        this.detalleNuevo.setIdcalidad(0);
        this.detalleNuevo.setIdtarima(0);
        this.detalleNuevo.setIdx(editar.getId());
        RequestContext  request = RequestContext.getCurrentInstance().getCurrentInstance();
        request.execute("PF('registroNuevo').show();");
    }
    
    public void seleccionPalletDetalleD(Pallet_desc editar,Integer i,String tarima,String marca)
    {
        this.pallet_editar_id=i;
        this.pallet_editar_ma=marca;
        this.pallet_editar_ta=tarima;
        
        this.detalleEdita = new Pallet_desc();
        this.detalleEdita.setId(editar.getId());
        this.detalleEdita.setActivo(editar.getActivo());
        this.detalleEdita.setCajas(editar.getCajas());
        this.detalleEdita.setCalibre(editar.getCalibre());
        this.detalleEdita.setCalidad(editar.getCalidad());
        this.detalleEdita.setCdi(editar.getCdi());
        this.detalleEdita.setIdactivo(editar.getIdactivo());
        this.detalleEdita.setIdcalidad(editar.getIdcalidad());
        this.detalleEdita.setIdtarima(editar.getIdtarima());
        this.detalleEdita.setIdx(editar.getIdtarima());
        try
        { 
            context.getExternalContext().redirect("operadorDetalleEditado.xhtml?faces-redirect=true&id="+i);
        }
        catch (IOException ex) {
            Logger.getLogger(PalletMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void seleccionMobileListUno()
    {
        try
        { 
            context.getExternalContext().redirect("operador.xhtml?faces-redirect=true");
        }
        catch (IOException ex) {
            Logger.getLogger(PalletMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public String seleccionMobileList()
    {
        return "pm:registroList";
    }
    public void guardarDetalleNuevo(int cajas,int idcalibre,int idcalidad,int idactivo,String cdi,int idPallet,int usuario)
    {
        try
        { 
            PalletDescController detalle =new PalletDescController(idPallet);
            detalle.insertRecord(cajas,idcalibre,idcalidad,cdi,idPallet,usuario);
            context.getExternalContext().redirect("operadorDetalleEdit.xhtml?faces-redirect=true&id="+idPallet);
        }
        catch (IOException ex) {
            Logger.getLogger(PalletMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void  guardarDetalleEditadoEE(Integer cajas)
    {
        Integer  cajas_     = cajas;
        try
        { 
            context.getExternalContext().redirect("operadorDetalle.xhtml?faces-redirect=true&id="+1);
        }
        catch (IOException ex) 
        {
            Logger.getLogger(PalletMB.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
           
    
    
    
    public void seleccionEditarMobile(Pallet editar)
    {
        this.registroEditar=new Pallet();
        this.registroEditar.setId(editar.getId()); 
        this.registroEditar.setQr(editar.getQr()); 
        this.registroEditar.setCajas(editar.getCajas());
        this.registroEditar.setIdactivo(editar.getIdactivo());
        this.registroEditar.setIdtarima(editar.getIdtarima());
        this.registroEditar.setIdmarca(editar.getIdmarca());
        this.registroEditar.setIdtransporte(editar.getIdtransporte());
        this.registroEditar.setNumero(editar.getNumero());  
        
        this.registroTransporte=new Pallet();
        this.registroTransporte.setId(editar.getId()); 
        this.registroTransporte.setQr(editar.getQr()); 
        this.registroTransporte.setCajas(editar.getCajas());
        this.registroTransporte.setIdactivo(editar.getIdactivo());
        this.registroTransporte.setIdtarima(editar.getIdtarima());
        this.registroTransporte.setIdmarca(editar.getIdmarca());
        this.registroTransporte.setIdtransporte(editar.getIdtransporte());
        this.registroTransporte.setNumero(editar.getNumero());
        try
        { 
            context.getExternalContext().redirect("operadorDetalle.xhtml?faces-redirect=true&id="+editar.getId());
        }
        catch (IOException ex) {
            Logger.getLogger(PalletMB.class.getName()).log(Level.SEVERE, null, ex);
        }        
    } 
    public void seleccionDetalle(Pallet_desc de)
    {
        //operadorDetalle
    }
    public void seleccionBorrado(Pallet editar )
    {
        this.registroBorrado=new Pallet();
        this.registroBorrado.setId(editar.getId()); 
        this.registroBorrado.setQr(editar.getQr()); 
        this.registroBorrado.setCajas(editar.getCajas());
        this.registroBorrado.setIdactivo(editar.getIdactivo());
        this.registroBorrado.setIdtarima(editar.getIdtarima());
        this.registroBorrado.setIdmarca(editar.getIdmarca());
        this.registroBorrado.setIdtransporte(editar.getIdtransporte());
        this.registroBorrado.setNumero(editar.getNumero());
        System.out.println("seleccionBorrado "+editar.toString());
    }
    
    public void seleccionTransporte(Pallet editar )
    {
        tr_id = editar.getId();
        tr_idtransporte =0;
        
        List<Transporte> lis=this.cat_transporte; 
        if(lis.size()>0)
            tr_idtransporte = ((Transporte)lis.get(0)).getId();
        if(editar.getIdtransporte()!=null)
        {
            if(editar.getIdtransporte()>0)
                tr_idtransporte =editar.getIdtransporte();
        }
        this.registroTransporte.setNumero(editar.getNumero());
        //System.out.println("seleccionTransporte>>>tr_id>>>>>>>>>>>"+tr_id);
        //System.out.println("seleccionTransporte>>>tr_idtransporte>"+tr_idtransporte);
        RequestContext  request = RequestContext.getCurrentInstance().getCurrentInstance();
        this.registroTransporte=new Pallet();
        this.registroTransporte.setId(editar.getId()); 
        this.registroTransporte.setQr(editar.getQr()); 
        this.registroTransporte.setCajas(editar.getCajas());
        this.registroTransporte.setIdactivo(editar.getIdactivo());
        this.registroTransporte.setIdtarima(editar.getIdtarima());
        this.registroTransporte.setIdmarca(editar.getIdmarca());
        this.registroTransporte.setIdtransporte(editar.getIdtransporte());
        this.registroTransporte.setNumero(editar.getNumero()); 
        request.execute("PF('Editarregistro1').show();");
    }
    public void editarRegistro( Integer idx,Pallet o)
    {
        try
        { 
            //System.out.println("Editar id    :"+o.getId());
            //System.out.println("Editar Nombre:"+o.getNumero()); 
            controller.updateRecord(o.getCajas(),o.getIdtarima(),o.getIdmarca(),sessionMB.getUsuariofinal().getId(),idx);
            context.getExternalContext().redirect("Listar.xhtml?faces-redirect=true");
        }
        catch (IOException ex) {
            Logger.getLogger(PalletMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void editarRegistro(int id,int cajas,int Idtarima,int Idmarca)
    {
        try
        { 
            controller.updateRecord(cajas,Idtarima,Idmarca,sessionMB.getUsuariofinal().getId(),id);
            context.getExternalContext().redirect("frigorifico.xhtml?faces-redirect=true");
        }
        catch (IOException ex) {
            Logger.getLogger(PalletMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void asigarDetallar(Integer i)
    {
        try
        { 
            context.getExternalContext().redirect("MasTransporte.xhtml?id="+i+"&faces-redirect=true");
        }
        catch (IOException ex) {
            Logger.getLogger(PalletMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void anexarRegistro()
    {
        try
        { 
             Integer idU=sessionMB.getUsuariofinal().getId();
            controller.insertRecord(cajas,idtarima,idmarca,
                    idU,idU);
            context.getExternalContext().redirect("Listar.xhtml?faces-redirect=true");
        }
        catch (IOException ex) 
        {
            Logger.getLogger(PalletMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void anexarRegistro(int idtarima,int idmarca)
    {
        try
        { 
             Integer idU=sessionMB.getUsuariofinal().getId();
            controller.insertRecord(0,idtarima,idmarca,
                    idU,idU);
            context.getExternalContext().redirect("frigorifico.xhtml?faces-redirect=true");
        }
        catch (IOException ex) 
        {
            Logger.getLogger(PalletMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void borrarRegistro(Pallet registroAborrar)
    {
        try
        { 
            controller.deleteRecord(registroAborrar.getId(),
                    sessionMB.getUsuariofinal().getId());
            System.out.println("seleccion Borrar "+registroAborrar.toString()); 
            this.todos= controller.getAll();
            context.getExternalContext().redirect("Listar.xhtml?faces-redirect=true");
            context.addMessage(null, new FacesMessage("Borrar", "Registro  borrado con exito :  "+registroAborrar.getNumero()));
        }
        catch (IOException ex) {
            Logger.getLogger(PalletMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /*-------------------------------------------------------------------------------------*/

    public PalletController getController() {
        return controller;
    }

    public void setController(PalletController controller) {
        this.controller = controller;
    }

    public List<Pallet> getTodos() {
        return todos;
    }

    public void setTodos(List<Pallet> todos) {
        this.todos = todos;
    }

    public Pallet getRegistroSeleccionado() {
        return registroSeleccionado;
    }

    public void setRegistroSeleccionado(Pallet registroSeleccionado) {
        this.registroSeleccionado = registroSeleccionado;
    }

    public Pallet getRegistroEditar() {
        return registroEditar;
    }

    public void setRegistroEditar(Pallet registroEditar) {
        this.registroEditar = registroEditar;
    }

    public Pallet getRegistroAnexar() {
        return registroAnexar;
    }

    public void setRegistroAnexar(Pallet registroAnexar) {
        this.registroAnexar = registroAnexar;
    }

    public Pallet getRegistroBorrado() {
        return registroBorrado;
    }

    public void setRegistroBorrado(Pallet registroBorrado) {
        this.registroBorrado = registroBorrado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getQr() {
        return qr;
    }

    public void setQr(String qr) {
        this.qr = qr;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Integer getCajas() {
        return cajas;
    }

    public void setCajas(Integer cajas) {
        this.cajas = cajas;
    }

    public Integer getIdactivo() {
        return idactivo;
    }

    public void setIdactivo(Integer idactivo) {
        this.idactivo = idactivo;
    }

    public Integer getIdtarima() {
        return idtarima;
    }

    public void setIdtarima(Integer idtarima) {
        this.idtarima = idtarima;
    }

    public Integer getIdmarca() {
        return idmarca;
    }

    public void setIdmarca(Integer idmarca) {
        this.idmarca = idmarca;
    }

    public Integer getIdtransporte() {
        return idtransporte;
    }

    public void setIdtransporte(Integer idtransporte) {
        this.idtransporte = idtransporte;
    }

    public List<Tarima> getCat_tarima() {
        return cat_tarima;
    }

    public void setCat_tarima(List<Tarima> cat_tarima) {
        this.cat_tarima = cat_tarima;
    }

    public List<Marca> getCat_marca() {
        return cat_marca;
    }

    public void setCat_marca(List<Marca> cat_marca) {
        this.cat_marca = cat_marca;
    }

    public List<Transporte> getCat_transporte() {
        return cat_transporte;
    }

    public void setCat_transporte(List<Transporte> cat_transporte) {
        this.cat_transporte = cat_transporte;
    }

    public Pallet getRegistroTransporte() 
    {
        return this.registroTransporte;
    }

    public void setRegistroTransporte(Pallet registroTransporte) {
        this.registroTransporte = registroTransporte;
    }

    public Integer getTr_id() {
        return tr_id;
    }

    public void setTr_id(Integer tr_id) {
        this.tr_id = tr_id;
    }

    public Integer getTr_idtransporte() {
        return tr_idtransporte;
    }
    
    public void setTr_idtransporte(Integer tr_idtransporte) {
        this.tr_idtransporte = tr_idtransporte;
    }

    public SessionMB getSessionMB() {
        return sessionMB;
    }

    public void setSessionMB(SessionMB sessionMB) {
        this.sessionMB = sessionMB;
    }

    public Integer getCajasDetalle() {
        return cajasDetalle;
    }

    public void setCajasDetalle(Integer cajasDetalle) {
        this.cajasDetalle = cajasDetalle;
    }

    public List<Calibre> getCat_calibre() {
        return cat_calibre;
    }

    public void setCat_calibre(List<Calibre> cat_calibre) {
        this.cat_calibre = cat_calibre;
    }

    public List<Cdi> getCat_cdi() {
        return cat_cdi;
    }

    public void setCat_cdi(List<Cdi> cat_cdi) {
        this.cat_cdi = cat_cdi;
    }

    public List<Calidad> getCat_calidad() {
        return cat_calidad;
    }

    public void setCat_calidad(List<Calidad> cat_calidad) {
        this.cat_calidad = cat_calidad;
    }

    public Integer getIdcalidad() {
        return idcalidad;
    }

    public void setIdcalidad(Integer idcalidad) {
        this.idcalidad = idcalidad;
    }

    public Integer getIdcalibre() {
        return idcalibre;
    }

    public void setIdcalibre(Integer idcalibre) {
        this.idcalibre = idcalibre;
    }

    public Integer getIdcdi() {
        return idcdi;
    }

    public void setIdcdi(Integer idcdi) {
        this.idcdi = idcdi;
    }

    public Pallet_desc getDetalle() {
        return detalle;
    }

    public void setDetalle(Pallet_desc detalle) {
        this.detalle = detalle;
    }

    public Integer getDetalle_cajas() {
        return detalle_cajas;
    }

    public void setDetalle_cajas(Integer detalle_cajas) {
        this.detalle_cajas = detalle_cajas;
    }

    public String getDetalle_cdi() {
        return detalle_cdi;
    }

    public void setDetalle_cdi(String detalle_cdi) {
        this.detalle_cdi = detalle_cdi;
    }

    public Integer getDetalle_calidad() {
        return detalle_calidad;
    }

    public void setDetalle_calidad(Integer detalle_calidad) {
        this.detalle_calidad = detalle_calidad;
    }

    public Integer getDetalle_calibre() {
        return detalle_calibre;
    }

    public void setDetalle_calibre(Integer detalle_calibre) {
        this.detalle_calibre = detalle_calibre;
    }

    public Integer getDetalle_activo() {
        return detalle_activo;
    }

    public void setDetalle_activo(Integer detalle_activo) {
        this.detalle_activo = detalle_activo;
    }

    public List<Activo> getCat_activo() {
        return cat_activo;
    }

    public void setCat_activo(List<Activo> cat_activo) {
        this.cat_activo = cat_activo;
    }

    public Pallet getRegistroeditardetalle() {
        return registroeditardetalle;
    }

    public void setRegistroeditardetalle(Pallet registroeditardetalle) {
        this.registroeditardetalle = registroeditardetalle;
    }

    public Pallet_desc getDetalleNuevo() {
        return detalleNuevo;
    }

    public void setDetalleNuevo(Pallet_desc detalleNuevo) {
        this.detalleNuevo = detalleNuevo;
    }

    public Pallet_desc getDetalleEdita() {
        return detalleEdita;
    }

    public void setDetalleEdita(Pallet_desc detalleEdita) {
        this.detalleEdita = detalleEdita;
    }

    public Integer getPallet_editar_id() {
        return pallet_editar_id;
    }

    public void setPallet_editar_id(Integer pallet_editar_id) {
        this.pallet_editar_id = pallet_editar_id;
    }

    public String getPallet_editar_ta() {
        return pallet_editar_ta;
    }

    public void setPallet_editar_ta(String pallet_editar_ta) {
        this.pallet_editar_ta = pallet_editar_ta;
    }

    public String getPallet_editar_ma() {
        return pallet_editar_ma;
    }

    public void setPallet_editar_ma(String pallet_editar_ma) {
        this.pallet_editar_ma = pallet_editar_ma;
    }
    
    
}