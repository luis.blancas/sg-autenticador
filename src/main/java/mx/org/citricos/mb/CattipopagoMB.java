/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.org.citricos.mb; 
import java.io.IOException;
import java.io.Serializable;
import java.util.List; 
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct; 
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext; 
import mx.org.citicos.controller.CatTipoPagoController; 
import mx.org.citricos.entity.CatTipoPago; 

/**
 *
 * @author luis Adrian
 */ 
@ManagedBean (name = "cattipopagoMB")
@RequestScoped
public class CattipopagoMB implements Serializable
{
    private SessionMB  sessionMB =new SessionMB();
    CatTipoPagoController controller=new CatTipoPagoController();
    private final FacesContext context = FacesContext.getCurrentInstance();
    private List<CatTipoPago> marcas; 
    private CatTipoPago marcaSeleccionado;
    private CatTipoPago marcaEditar;
    private CatTipoPago marcaAnexar;
    private CatTipoPago marcaBorrado; 
    
    private Integer id;
    private String  descripcion;
    private Integer tipo;
    private String  tipos;
    
    @PostConstruct
    public void init() {
        marcaSeleccionado=new CatTipoPago();
        this.marcas=controller.getAll();
    }
    public List<CatTipoPago> getCalidads() {
        
        this.marcas= controller.getAll();
        return marcas;
    } 
    public void seleccionEditar(CatTipoPago editar )
    {
        this.marcaEditar=new CatTipoPago();
        this.marcaEditar.setId(editar.getId()); 
        this.marcaEditar.setNombre(editar.getNombre()); 
        this.marcaEditar.setTipo(editar.getTipo());
        System.out.println("seleccion Editar "+editar.toString());
    }
    public void seleccionBorrado(CatTipoPago borrado )
    {
        this.marcaBorrado=new CatTipoPago();
        this.marcaBorrado.setId(borrado.getId());
        this.marcaBorrado.setNombre(borrado.getNombre());
        this.marcaBorrado.setTipo(borrado.getTipo());
        System.out.println("seleccionBorrado "+borrado.toString());
    } 
    public void editarmarca(Integer id,CatTipoPago o)
    {
        try
        { 
            System.out.println("Editar id    :"+id);
            System.out.println("Editar Nombre:"+o.getNombre());
            controller.updateRecord(o.getNombre(),o.getTipo(), id,
                    sessionMB.getUsuariofinal().getId());
            context.getExternalContext().redirect("Listar.xhtml?faces-redirect=true");
        }
        catch (IOException ex) {
            Logger.getLogger(CattipopagoMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void anexarmarca()
    {
        try
        { 
            System.out.println("Anexar Nombre:"+this.descripcion);
            controller.insertRecord(descripcion,tipo,
                    sessionMB.getUsuariofinal().getId());
            context.getExternalContext().redirect("Listar.xhtml?faces-redirect=true");
        }
        catch (IOException ex) 
        {
            Logger.getLogger(CattipopagoMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void borrarmarca(CatTipoPago marcaAborrar)
    {
        try
        { 
            controller.deleteRecord(marcaAborrar.getId(),sessionMB.getUsuariofinal().getId());
            System.out.println("seleccion Borrar "+marcaAborrar.toString()); 
            this.marcas= controller.getAll();
            context.getExternalContext().redirect("Listar.xhtml?faces-redirect=true");
            context.addMessage(null, new FacesMessage("Borrar", "Registro  borrado con exito :  "+marcaAborrar.getNombre()));
        }
        catch (IOException ex) {
            Logger.getLogger(CattipopagoMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /*-------------------------------------------------------------------------------------*/

    public CatTipoPagoController getController() {
        return controller;
    }

    public void setController(CatTipoPagoController controller) {
        this.controller = controller;
    }

    public List<CatTipoPago> getMarcas() {
        return marcas;
    }

    public void setMarcas(List<CatTipoPago> marcas) {
        this.marcas = marcas;
    }

    public CatTipoPago getMarcaSeleccionado() {
        return marcaSeleccionado;
    }

    public void setMarcaSeleccionado(CatTipoPago marcaSeleccionado) {
        this.marcaSeleccionado = marcaSeleccionado;
    }

    public CatTipoPago getMarcaEditar() {
        return marcaEditar;
    }

    public void setMarcaEditar(CatTipoPago marcaEditar) {
        this.marcaEditar = marcaEditar;
    }

    public CatTipoPago getMarcaAnexar() {
        return marcaAnexar;
    }

    public void setMarcaAnexar(CatTipoPago marcaAnexar) {
        this.marcaAnexar = marcaAnexar;
    }

    public CatTipoPago getMarcaBorrado() {
        return marcaBorrado;
    }

    public void setMarcaBorrado(CatTipoPago marcaBorrado) {
        this.marcaBorrado = marcaBorrado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }

    public String getTipos() {
        if(this.tipo==1)
            return "Positivo";
        return "Negativo";
    }

    public void setTipos(String tipos) {
        this.tipos = tipos;
    }

}
