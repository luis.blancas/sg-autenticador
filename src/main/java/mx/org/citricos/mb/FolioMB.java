/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.org.citricos.mb; 
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List; 
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct; 
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;  
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.servlet.ServletContext;
import mx.org.citicos.controller.AgronomoController;
import mx.org.citicos.controller.FolioController;
import mx.org.citicos.controller.ProductorController;
import mx.org.citicos.controller.TlimonController;
import mx.org.citicos.controller.TrejaController;
import mx.org.citricos.entity.Corrida;
import mx.org.citricos.entity.Folios; 
import mx.org.citricos.entity.Productor;
import mx.org.citricos.entity.Tarima;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.printing.PDFPageable;
import org.primefaces.event.FlowEvent;
/**
 *
 * @author luis Adrian
 */ 
@ManagedBean (name = "folioMB")
@RequestScoped
public class FolioMB implements Serializable
{
    private final FacesContext context = FacesContext.getCurrentInstance();
    private List<Folios> registros_basculas;
    private List<Folios> registros_recepcion; 
    private List<Folios> registros_confirmacion; 
    private List<Folios> corridas; 
    private Folios activoSeleccionado;
    private Folios activoEditar;
    private Folios activoAnexar;
    private Folios activoBorrado; 
    private List<Productor> productores;
    private List<Tarima> agronomos;
    private List<Tarima> tamanos_limon;
    private List<Tarima> tamanos_reja;
    private String url;
    private String mensaje;
    private Integer id;
    private String  folio;
    private String  fecha;
    private Integer id_productor;
    private Integer peso_bruto=0;
    private Integer peso_tara=0;
    private Integer peso_neto=0;
    //---------------------------------------
    private Integer no_rejas;
    private Integer tipo_rejas;
    private Integer tipo_limon;
    private Integer id_agronomo;
    private Integer dejo;
    private String observaciones;
            
    //---------------------------------------
    private Integer segundas;
    private Integer terceras;
    private Integer torreon;
    private Integer coleada;
    private Integer japon;
    //---------------------------------------
    //---------------------------------------
    private String productor;
    private String agronomo;
    private String tipos_rejas;
    private String tipos_limones;
    private boolean isdejo;
    private boolean skip;
    @PostConstruct
    public void init() {
        activoSeleccionado=new Folios();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date d=new Date();
        String fechaN=format.format(d);
        this.fecha = fechaN;
        ProductorController prod=new ProductorController();
        this.productores= prod.getAll();
        AgronomoController ctr=new AgronomoController();
        this.agronomos=ctr.getAll();
        TlimonController ctrtl=new TlimonController();
        this.tamanos_limon=ctrtl.getAll();
        TrejaController ctrtr=new TrejaController();
        this.tamanos_reja=ctrtr.getAll();
    }
    public String onFlowProcess(FlowEvent event) {
        if(skip) {
            skip = false;   //reset in case user goes back
            return "confirm";
        }
        else {
            return event.getNewStep();
        }
    }
    public void seleccionEditar(Folios editar )
    {
        this.activoEditar=new Folios();
        this.activoEditar.setId(editar.getId()); 
        this.activoEditar.setFolio(editar.getFolio());
        this.activoEditar.setFecha(editar.getFecha());
        this.activoEditar.setId_agronomo(editar.getId_agronomo());
        this.activoEditar.setPeso_bruto(editar.getPeso_bruto());
        this.activoEditar.setPeso_neto(editar.getPeso_neto());
        this.activoEditar.setPeso_tara(editar.getPeso_tara());
        this.activoEditar.setNo_rejas(editar.getNo_rejas());
        this.activoEditar.setTipo_rejas(editar.getTipo_rejas());
        this.activoEditar.setTipo_limon(editar.getTipo_limon());
        this.activoEditar.setTipos_limones(editar.getTipos_limones());
        this.activoEditar.setTipos_rejas(editar.getTipos_rejas());
        this.activoEditar.setDejo(editar.getDejo());
        this.activoEditar.setObservaciones(editar.getObservaciones());
        this.activoEditar.setSegundas(editar.getSegundas());
        this.activoEditar.setTerceras(editar.getTerceras());
        this.activoEditar.setTorreon(editar.getTorreon());
        this.activoEditar.setColeada(editar.getColeada());
        this.activoEditar.setJapon(editar.getJapon());
        this.activoEditar.setProductor(editar.getProductor());
        this.activoEditar.setAgronomo(editar.getAgronomo());
        System.out.println("seleccion Editar "+editar.toString());
    }
    
    public String seleccionEditarMobile(Folios editar )
    {
        this.activoEditar=new Folios();
        this.activoSeleccionado=new Folios();
        
        this.activoSeleccionado.setId(editar.getId()); 
        this.activoSeleccionado.setFolio(editar.getFolio());
        this.activoSeleccionado.setFecha(editar.getFecha());
        this.activoSeleccionado.setId_agronomo(editar.getId_agronomo());
        this.activoSeleccionado.setId_productor(editar.getId_productor());
        this.activoSeleccionado.setPeso_bruto(editar.getPeso_bruto());
        this.activoSeleccionado.setPeso_neto(editar.getPeso_neto());
        this.activoSeleccionado.setPeso_tara(editar.getPeso_tara());
        this.activoSeleccionado.setNo_rejas(editar.getNo_rejas());
        this.activoSeleccionado.setTipo_rejas(editar.getTipo_rejas());
        this.activoSeleccionado.setTipo_limon(editar.getTipo_limon());
        this.activoSeleccionado.setTipos_limones(editar.getTipos_limones());
        this.activoSeleccionado.setTipos_rejas(editar.getTipos_rejas());
        this.activoSeleccionado.setDejo(editar.getDejo());
        this.activoSeleccionado.setIsdejo(editar.isIsdejo());
        this.activoSeleccionado.setObservaciones(editar.getObservaciones());
        this.activoSeleccionado.setSegundas(editar.getSegundas());
        this.activoSeleccionado.setTerceras(editar.getTerceras());
        this.activoSeleccionado.setTorreon(editar.getTorreon());
        this.activoSeleccionado.setColeada(editar.getColeada());
        this.activoSeleccionado.setJapon(editar.getJapon());
        this.activoSeleccionado.setProductor(editar.getProductor());
        this.activoSeleccionado.setAgronomo(editar.getAgronomo());
        this.activoSeleccionado.setCodigo(editar.getCodigo());
        
        Corrida  corrida = new Corrida();
        corrida.setColeada(editar.getCorrida().getColeada());
        corrida.setEmpaque_110(editar.getCorrida().getEmpaque_110());
        corrida.setEmpaque_150(editar.getCorrida().getEmpaque_150());
        corrida.setEmpaque_175(editar.getCorrida().getEmpaque_175());
        corrida.setEmpaque_200(editar.getCorrida().getEmpaque_200());
        corrida.setEmpaque_230(editar.getCorrida().getEmpaque_230());
        corrida.setEmpaque_250(editar.getCorrida().getEmpaque_250());
        corrida.setSegundas(editar.getCorrida().getSegundas());
        corrida.setTerceras(editar.getCorrida().getTerceras());
        corrida.setTorreon(editar.getCorrida().getTorreon());
        corrida.setVerde_110(editar.getCorrida().getVerde_110());
        corrida.setVerde_150(editar.getCorrida().getVerde_150());
        corrida.setVerde_175(editar.getCorrida().getVerde_175());
        corrida.setVerde_200(editar.getCorrida().getVerde_200());
        corrida.setVerde_230(editar.getCorrida().getVerde_230());
        corrida.setVerde_250(editar.getCorrida().getVerde_250());
        corrida.setVerde_japon(editar.getCorrida().getVerde_japon());
        corrida.setComprador(editar.getCorrida().getComprador());
        corrida.setFacturar(editar.getCorrida().getFacturar());
        corrida.setFolio(editar.getCorrida().getFolio());
        corrida.setId(editar.getCorrida().getId());
        corrida.setIdcomprador(editar.getCorrida().getIdcomprador());
        corrida.setIdfacturar(editar.getCorrida().getIdfacturar());
        corrida.setIdtipo(editar.getCorrida().getIdtipo());
        corrida.setTipo(editar.getCorrida().getTipo());
        this.activoSeleccionado.setCorrida(corrida);
        
        this.activoEditar.setId(editar.getId()); 
        this.activoEditar.setFolio(editar.getFolio());
        this.activoEditar.setFecha(editar.getFecha());
        this.activoEditar.setId_agronomo(editar.getId_agronomo());
        this.activoEditar.setId_productor(editar.getId_productor());
        this.activoEditar.setPeso_bruto(editar.getPeso_bruto());
        this.activoEditar.setPeso_neto(editar.getPeso_neto());
        this.activoEditar.setPeso_tara(editar.getPeso_tara());
        this.activoEditar.setNo_rejas(editar.getNo_rejas());
        this.activoEditar.setTipo_rejas(editar.getTipo_rejas());
        this.activoEditar.setTipo_limon(editar.getTipo_limon());
        this.activoEditar.setTipos_limones(editar.getTipos_limones());
        this.activoEditar.setTipos_rejas(editar.getTipos_rejas());
        this.activoEditar.setDejo(editar.getDejo());
        this.activoEditar.setIsdejo(editar.isIsdejo());
        this.activoEditar.setObservaciones(editar.getObservaciones());
        this.activoEditar.setSegundas(editar.getSegundas());
        this.activoEditar.setTerceras(editar.getTerceras());
        this.activoEditar.setTorreon(editar.getTorreon());
        this.activoEditar.setColeada(editar.getColeada());
        this.activoEditar.setJapon(editar.getJapon());
        this.activoEditar.setProductor(editar.getProductor());
        this.activoEditar.setAgronomo(editar.getAgronomo());
        this.activoEditar.setCodigo(editar.getCodigo());
        this.activoEditar.setCorrida(corrida);
        
        System.out.println("seleccion Editar (1)"+editar.toString());
        System.out.println("seleccion Editar (2)"+this.activoEditar.toString());
        return "pm:regEditar";
    }
    public String reactivar()
    {
        return "pm:regEditar";
    }
    public String seleccionNuevoMobile()
    {
        this.activoAnexar=new Folios(); 
        
        this.activoAnexar.setId(-1); 
        this.activoAnexar.setFolio("");
        this.activoAnexar.setFecha("");
        this.activoAnexar.setId_agronomo(0);
        this.activoAnexar.setId_productor(0);
        this.activoAnexar.setPeso_bruto(0);
        this.activoAnexar.setPeso_neto(0);
        this.activoAnexar.setPeso_tara(0);
        this.activoAnexar.setNo_rejas(0);
        this.activoAnexar.setTipo_rejas(0);
        this.activoAnexar.setTipo_limon(0);
        this.activoAnexar.setTipos_limones("");
        this.activoAnexar.setTipos_rejas("");
        this.activoAnexar.setDejo(0);
        this.activoAnexar.setObservaciones("");
        this.activoAnexar.setSegundas(0);
        this.activoAnexar.setTerceras(0);
        this.activoAnexar.setTorreon(0);
        this.activoAnexar.setColeada(0);
        this.activoAnexar.setJapon(0);
        this.activoAnexar.setProductor("");
        this.activoAnexar.setAgronomo("");
        return "pm:registroNuevo";
    }
    public void seleccionBorrado(Folios editar )
    {
        this.activoBorrado=new Folios();
        this.activoBorrado.setId(editar.getId());
        this.activoBorrado=new Folios();
        this.activoBorrado.setId(editar.getId()); 
        this.activoBorrado.setFolio(editar.getFolio());
        this.activoBorrado.setFecha(editar.getFecha());
        this.activoBorrado.setId_agronomo(editar.getId_agronomo());
        this.activoBorrado.setPeso_bruto(editar.getPeso_bruto());
        this.activoBorrado.setPeso_neto(editar.getPeso_neto());
        this.activoBorrado.setPeso_tara(editar.getPeso_tara());
        this.activoBorrado.setNo_rejas(editar.getNo_rejas());
        this.activoBorrado.setTipo_rejas(editar.getTipo_rejas());
        this.activoBorrado.setTipo_limon(editar.getTipo_limon());
        this.activoBorrado.setTipos_limones(editar.getTipos_limones());
        this.activoBorrado.setTipos_rejas(editar.getTipos_rejas());
        this.activoBorrado.setDejo(editar.getDejo());
        this.activoBorrado.setObservaciones(editar.getObservaciones());
        this.activoBorrado.setSegundas(editar.getSegundas());
        this.activoBorrado.setTerceras(editar.getTerceras());
        this.activoBorrado.setTorreon(editar.getTorreon());
        this.activoBorrado.setColeada(editar.getColeada());
        this.activoBorrado.setJapon(editar.getJapon());
        this.activoBorrado.setProductor(editar.getProductor());
        this.activoBorrado.setAgronomo(editar.getAgronomo());
        System.out.println("seleccionBorrado "+editar.toString());
    }
    //1a Opcion
    public String  anexarActivo(Integer id_productor,int peso_bruto, int peso_tara,Integer usuario)
    {
        try
        {
            FolioController controller=new FolioController();
            Folios f=controller.insertRecord(id_productor,peso_bruto,peso_tara,
                    (peso_bruto - peso_tara),usuario);
            this.id_productor=0;
            this.peso_bruto=0;
            this.peso_tara=0;
            context.addMessage(null, new FacesMessage("Folio Creado", "Registro creado con exito ["+f.getId()+"]"));
            return seleccionEditarMobile(f);
        }
        catch (Exception ex) 
        {
                context.addMessage(null, new FacesMessage("Error", "Problemas ["+ex.getMessage()+"]"));
        }
        return "bascula.xhtml?faces-redirect=true";
    } 
    //1a opcion
    public void editarActivo(int bruto,int tara,Integer id_productor,Integer id,int opc,Integer usuario)
    {
        try
        {
            FolioController controller=new FolioController();
            controller.updateRecordFase1(id_productor,bruto,tara,
                    (bruto-tara),id,opc,usuario);
            if(opc==1)
                imprimir(id,opc);
            context.getExternalContext().redirect("bascula.xhtml?faces-redirect=true");
        }
        catch (IOException ex) 
        {
            Logger.getLogger(FolioMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void sumar(Folios o)
    {
        if(o.getPeso_bruto()>0 &&
           o.getPeso_tara()>0  &&
           (o.getPeso_bruto() - o.getPeso_tara())>0)
        {
            o.setPeso_neto(o.getPeso_bruto() - o.getPeso_tara());
        }
        else
        {
            o.setPeso_neto(-1);
        }
    }
    //1a opcion
    public void editarActivo(Folios o,Integer id_productor,int opc)
    {
        try
        {
            FolioController controller=new FolioController();
            controller.updateRecordFase1(id_productor,o.getPeso_bruto(),o.getPeso_tara(),
                    (o.getPeso_bruto()-o.getPeso_tara()),o.getId(),opc);
            context.getExternalContext().redirect("ListaActivo.xhtml?faces-redirect=true");
        }
        catch (IOException ex) 
        {
            Logger.getLogger(FolioMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void cambio()
    {
        this.peso_neto = this.peso_bruto - this.peso_tara;
        System.out.println(this.peso_neto+" = "+this.peso_bruto+" - "+this.peso_tara);
    }
    //2a opcion mobile
    public void editarRegitroMobile2(Integer no_rejas,Integer tipo_rejas,Integer tipo_limon,Integer id_agronomo,
                    String observaciones,boolean isdejo,int opc,Integer id,Integer usuario)
    { 
        int idejo = 0;
        try
        {
            FolioController controller=new FolioController();
            if(isdejo)
                idejo = 1;
            else
                idejo = 0;
            if(id_agronomo != null)
                controller.updateRecord(no_rejas,tipo_rejas,
                        tipo_limon,id_agronomo,idejo,
                        observaciones,id,opc,usuario);
            else
                controller.updateRecord(no_rejas,tipo_rejas,
                        tipo_limon,0,idejo,
                        observaciones,id,opc,usuario);
            if(opc==2)
                imprimir(id,opc);
            context.getExternalContext().redirect("recepcion.xhtml?faces-redirect=true");
        }
        catch (IOException ex) {
            Logger.getLogger(FolioMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    //2a posicion
    public void editarRegitro(Folios o,Integer trejas,Integer tlimon,Integer iAgronomo,int opc)
    { 
        try
        {
            FolioController controller=new FolioController();
            if(o.isIsdejo())
                o.setDejo(1);
            else
                o.setDejo(0);
            if(iAgronomo != null)
                controller.updateRecord(o.getNo_rejas(),o.getTipo_rejas(),
                        o.getTipo_limon(),o.getId_agronomo(),o.getDejo(),
                        o.getObservaciones(),o.getId(),opc);
            else
                controller.updateRecordSinAgr(o.getNo_rejas(),o.getTipo_rejas(),
                        o.getTipo_limon(),o.getDejo(),o.getObservaciones(),o.getId(),opc);
            context.getExternalContext().redirect("ListaActivo2.xhtml?faces-redirect=true");
        }
        catch (IOException ex) {
            Logger.getLogger(FolioMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    //3a opcion mobile
    public void editarRegitroMobile3(
            Integer peso_bruto,Integer peso_tara,
            Integer japon,Integer segundas,Integer terceras,Integer torreon,Integer coleada,
            Integer id,int opc,int usuario)
    {      
        try
        {
            FolioController controller=new FolioController();
            System.out.println("id:::"+ id);
            if(id != null)
            {
                if(opc==3)
                {
                    if((peso_bruto-peso_tara)>0)
                    {
                        controller.updateRecord(
                                peso_bruto,peso_tara,(peso_bruto-peso_tara),
                                japon,segundas,terceras,torreon,coleada,
                                id,opc,usuario);
                        imprimir(id,3);
                    }
                }
                else
                {
                    controller.updateRecord(
                        peso_bruto,peso_tara,(peso_bruto-peso_tara),
                        japon,segundas,terceras,torreon,coleada,
                        id,opc,usuario);
                }
                context.getExternalContext().redirect("confirmacion.xhtml?faces-redirect=true");
            }
        }
        catch (IOException ex) {
            Logger.getLogger(FolioMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    //3a Opcion
    public void editarRegitro(Folios o)
    {
        try
        {
            FolioController controller=new FolioController();
            controller.updateRecord(o.getSegundas(),o.getTerceras(),o.getTorreon(),o.getColeada(),o.getJapon(),o.getId());
            context.getExternalContext().redirect("ListaActivo3.xhtml?faces-redirect=true");
        }
        catch (IOException ex) {
            Logger.getLogger(FolioMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void borrarActivo(Folios activoAborrar)
    {
        try
        {
            FolioController controller=new FolioController();
            controller.deleteRecord(activoAborrar.getId().intValue());
            System.out.println("seleccion Borrar "+activoAborrar.toString()); 
            context.getExternalContext().redirect("ListaActivo.xhtml?faces-redirect=true");
            context.addMessage(null, new FacesMessage("Borrar", "Registro  borrado con exito :  "+activoAborrar.getId()));
        }
        catch (IOException ex) {
            Logger.getLogger(FolioMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void rechazarActivo2(Folios activoAborrar)
    {
        try
        {
            FolioController controller=new FolioController();
            controller.rechazarActivo(activoAborrar.getId().intValue());
            System.out.println("seleccion Borrar "+activoAborrar.toString()); 
            context.getExternalContext().redirect("ListaActivo2.xhtml?faces-redirect=true");
            context.addMessage(null, new FacesMessage("Borrar", "Registro  borrado con exito :  "+activoAborrar.getId()));
        }
        catch (IOException ex) {
            Logger.getLogger(FolioMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void rechazarActivo3(Folios activoAborrar)
    {
        try
        {
            FolioController controller=new FolioController();
            controller.rechazarActivo(activoAborrar.getId().intValue());
            System.out.println("seleccion Borrar "+activoAborrar.toString()); 
            context.getExternalContext().redirect("ListaActivo3.xhtml?faces-redirect=true");
            context.addMessage(null, new FacesMessage("Borrar", "Registro  borrado con exito :  "+activoAborrar.getId()));
        }
        catch (IOException ex) {
            Logger.getLogger(FolioMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void rechazar(Integer id,int opcion)
    {
        try
        {
            FolioController controller=new FolioController();
            controller.rechazar(id,opcion);
            System.out.println("seleccion Borrar "+id+"-->"+opcion); 
            if(opcion==0)
                context.getExternalContext().redirect("recepcion.xhtml?faces-redirect=true");
            else
                context.getExternalContext().redirect("confirmacion.xhtml?faces-redirect=true");
        }
        catch (IOException ex) {
            Logger.getLogger(FolioMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void imprimir(Integer i,int opcion)
    {
        System.out.println("imprimir "+i);
        FolioController controller=new FolioController();
        String fechaModificacion= controller.getFecha();
        Folios folio = controller.getOne(i);
        controller.updateRecord(fechaModificacion,opcion,folio.getId());
        imprimePallet(
                folio.getId(),
                folio.getFolio(),
                folio.getCodigo(),
                folio.getFecha(),
                folio.getProductor(),
                folio.getPeso_bruto(),
                folio.getPeso_tara(),
                folio.getPeso_neto(),
                folio.getNo_rejas(),
                folio.getTipos_rejas(),
                folio.getTipos_limones(),
                (folio.getAgronomo()!=null?folio.getAgronomo():""),
                folio.getObservaciones(),
                folio.getDejo(),
                folio.getJapon(),
                folio.getSegundas(),
                folio.getTerceras(),
                folio.getTorreon(),
                folio.getColeada(),
                fechaModificacion,
                opcion);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "NOTA", "Se mando a imprimir Pallet ["+i+"]"));

    }
    
    public void imprimePallet(
            Integer Id,
            String Folio,
            String Codigo,
            String Fecha,
            String Productor,
            Integer bruto,
            Integer tara,
            Integer neto,
            
            Integer rejas,
            String  trejas,
            String  tlimon,
            String  agronomo,
            String  observaciones,
            int     dejo,
            int Japon,
            int Segundas,
            int Terceras,
            int Torreon,
            int Coleada,
            String fechaModificacion,
            int opcion)
    {
        try 
        {
            listarImpresoras();
            ByteArrayOutputStream documentoBytes=null;
            documentoBytes= crearDocumentoiText(Folio,Codigo,Fecha,Productor,bruto,tara,neto,
                            rejas,trejas,tlimon,agronomo,observaciones,dejo,
                            Japon,Segundas,Terceras,Torreon,Coleada,fechaModificacion,opcion);
            imprimir(documentoBytes);
        }
        catch (IOException | PrinterException ex) 
        {
            ex.printStackTrace();
        }
        
    }
    private  void imprimir(ByteArrayOutputStream documentoBytes) throws IOException, PrinterException
    {
        ByteArrayInputStream bais = new ByteArrayInputStream(documentoBytes.toByteArray());
        PDDocument document = PDDocument.load(bais);
        PrintService myPrintService = findPrintService(firstImpresora());
        PrinterJob printerJob = PrinterJob.getPrinterJob();
        printerJob.setPageable(new PDFPageable(document));
        printerJob.setPrintService(myPrintService);
        printerJob.print();

    }
    private PrintService findPrintService(String printerName) {
        PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, null);
        for (PrintService printService : printServices) {
            System.out.println(printService.getName());

            if (printService.getName().trim().equals(printerName)) {
                return printService;
            }
        }
        return null;
    }
    private String  firstImpresora() {
        PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, null);
        System.out.println("Lista de impresoras disponibles");
        for (PrintService printService : printServices) {
            return printService.getName();
        }
        return ""; 
    }
    private Table creaTabla(Integer rejas,
            String  trejas,
            String  tlimon,
            String  agronomo,
            String  observaciones,
            Image dejo)
    {
        Table table1 = new Table(3);
        table1.setBorder(Border.NO_BORDER);
        table1.setWidth(500F);
        
        table1.addCell("No. Rejas");
        table1.addCell("Tamaño rejas");
        table1.addCell("Tamaño limón");
        table1.addCell(""+rejas);
        table1.addCell(""+trejas);
        table1.addCell(""+tlimon);
        
        table1.addCell("Agronomo");
        table1.addCell("Observaciones");
        table1.addCell("Dejo rejas");
        table1.addCell(""+agronomo);
        table1.addCell(""+observaciones);
        table1.addCell(dejo);  
        
        return table1;
    }
    private Table creaTabla(
            int Japon,
            int Segundas,
            int Terceras,
            int Torreon,
            int Coleada)
    {
        Table table1 = new Table(3);
        table1.setBorder(Border.NO_BORDER);
        table1.setWidth(500F);

        table1.addCell("Japon");
        table1.addCell("Segundas");
        table1.addCell("Terceras");
        table1.addCell(""+Japon);
        table1.addCell(""+Segundas);
        table1.addCell(""+Terceras);

        table1.addCell("Torreon");
        table1.addCell("Coleada");
        table1.addCell(" ");
        table1.addCell(""+Torreon);
        table1.addCell(""+Coleada);
        table1.addCell(" ");  
        
        return table1; 
    }
    private Table creaTabla(String c1,String c2,String c3,
            String c4,String c5,String c6,
    String c7,String c8,String c9,
    String c10,String c11,String c12,
    Image img1,String fecha,Image img2,String dato) 
    { 
        Table table1 = new Table(3);
        table1.setBorder(Border.NO_BORDER);
        table1.setWidth(500F); 
        table1.addCell(dato);
        table1.addCell("Citricos Cadillos SA de CV");
        table1.addCell(""); 
        table1.addCell(img1);
        table1.addCell(fecha);
        table1.addCell(img2); 
        table1.addCell(c1);
        table1.addCell(c2);
        table1.addCell(c3); 
        table1.addCell(c4);
        table1.addCell(c5);
        table1.addCell(c6);  
        table1.addCell(c7);
        table1.addCell(c8);
        table1.addCell(c9);
        table1.addCell(c10);
        table1.addCell(c11);
        table1.addCell(c12);
        
        return table1;
    }
    public static void main(String[] args)
    {
        String cad="Mon 04 de February del 2019 08:02.24";
        FolioMB mb=new FolioMB();
        System.out.println("CAD:: "+cad);
        cad =  mb.changeMonthForKey(cad);
        System.out.println("CAD:: "+cad);
        cad =  mb.changeDayForKey(cad);
        System.out.println("CAD:: "+cad);
    }
    private String changeDayForKey(String cadena)
    {
        Map md=new HashMap();
        md.put("Mon","Lunes");
        md.put("Tue","Martes");
        md.put("Wed","Miercoles");
        md.put("Thu","Jueves");
        md.put("Fri","Viernes");
        md.put("Sat","Sabado");
        md.put("Sun","Domingo");
        int size= md.size();
        Iterator itKey =  md.keySet().iterator();
        for(int i=0;itKey.hasNext() && i<size;i++)
        {
            String key = (String)itKey.next();
            int container =  cadena.indexOf(key);
            if(container >= 0)
            {
                String value=  (String)md.get(key);
                cadena=cadena.replaceAll(key,value);
                return cadena;
            }
        }
        return cadena;
    }
    private String changeMonthForKey(String cadena)
    {
        Map md=new HashMap();
        md.put("January","Enero");
        md.put("February","Febrero");
        md.put("March","Marzo");
        md.put("April","Abril");
        md.put("May","Mayo");
        md.put("June","Junio");
        md.put("July","Julio");
        md.put("August","Agosto");
        md.put("September","Septiembre");
        md.put("October","Octubre");
        md.put("November","Noviembre");
        md.put("December","Diciembre");
        int size= md.size();
        Iterator itKey =  md.keySet().iterator();
        for(int i=0;itKey.hasNext() && i<size;i++)
        {
            String key = (String)itKey.next();
            int container =  cadena.indexOf(key);
            if(container >= 0)
            {
                String value=  (String)md.get(key);
                cadena = cadena.replaceAll(key,value);
                return cadena;
            }
        }
        return cadena;
    }
    private ByteArrayOutputStream crearDocumentoiText( 
            String Folio,
            String Codigo,
            String Fecha,
            String Productor,
            Integer bruto,
            Integer tara,
            Integer neto,
            Integer rejas,
            String  trejas,
            String  tlimon,
            String  agronomo,
            String  observaciones,
            int     dejo,
            int Japon,
            int Segundas,
            int Terceras,
            int Torreon,
            int Coleada,
            String fechaModificacion,
            int     opcion) 
    {
        ByteArrayOutputStream documentoBytes = null;
        try 
        {
            documentoBytes = new ByteArrayOutputStream();
            PdfWriter pdfWriter = new PdfWriter(documentoBytes);
            PdfDocument pdfDoc = new PdfDocument(pdfWriter);
            Document documento = new Document(pdfDoc, PageSize.LETTER); 
            ServletContext ctx = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext(); 
            String realPath = ctx.getRealPath("/");
            String imageFiless = realPath+"citricos.png"; 
            ImageData imagen2=ImageDataFactory.create(getImageQRImage(Folio,"png",130,130)); 
            
            ImageData imagen1 = ImageDataFactory.create(imageFiless);
            Image img1=new Image(imagen1);
            img1.setHeight(90);
            img1.setWidth(130);
            Image img2=new Image(imagen2);
            img2.setHeight(130);
            img2.setWidth(130);
            String dato ="";
            if(opcion == 1) dato  =  "Bascula"; 
            if(opcion == 2) dato  =  "Recepción"; 
            if(opcion == 3) dato  =  "Confirmación"; 
            fechaModificacion= changeMonthForKey(fechaModificacion);
            fechaModificacion= changeDayForKey(fechaModificacion);
            documento.add(new Paragraph("Fecha de Impresión: "+fechaModificacion));
            documento.add(creaTabla("Folio",Folio,"       "+Codigo,
                    "Productor",Productor,"       *",
                    "Peso Bruto","Peso Tara","Peso neto",
                    ""+bruto,""+tara,""+neto,
                    img1,"Fecha["+Fecha+"]",img2,dato));
            if(opcion == 2 || opcion == 3)
            { 
                String checkin_png = realPath+"checkIn.png"; 
                String checkout_png = realPath+"checkOut.png";
                System.out.println("realPath>>"+realPath);
                ImageData checkIn_img = ImageDataFactory.create(checkin_png);
                ImageData checkOut_img = ImageDataFactory.create(checkout_png);
                Image checkIn_imagen=new Image(checkIn_img);
                Image checkOut_imagen=new Image(checkOut_img);
                checkIn_imagen.setHeight(20);
                checkIn_imagen.setWidth(20);
                checkOut_imagen.setHeight(20);
                checkOut_imagen.setWidth(20);
                Image imagen = (dejo==1)? checkIn_imagen : checkOut_imagen ; 
                documento.add(new Paragraph("   "));
                documento.add(creaTabla(
                        rejas,trejas,tlimon,
                        agronomo,observaciones,imagen));
            }
            if(opcion == 3)
            { 
                documento.add(new Paragraph("   "));
                documento.add(creaTabla(Japon,Segundas,Terceras,Torreon,Coleada));
            }
            documento.close();
        } 
        catch (MalformedURLException ex) 
        {
            Logger.getLogger(FolioMB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return documentoBytes;
    }
    private static byte[] getImageQRImage(String text, String ext, int width, int height) 
    {
    	byte[] pngData = null;
        try {
                QRCodeWriter qrCodeWriter = new QRCodeWriter();
            BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, width, height);
            ByteArrayOutputStream pngOutputStream = new ByteArrayOutputStream();
            MatrixToImageWriter.writeToStream(bitMatrix, ext, pngOutputStream);
            pngData = pngOutputStream.toByteArray();  
        } catch(WriterException wex) {
                System.out.println(wex.getMessage());
        } catch(IOException ioe) {
                System.out.println(ioe.getMessage());
        } 
         return pngData;
    } 
    private void listarImpresoras() {
        PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, null);
        System.out.println("Lista de impresoras disponibles");

        for (PrintService printService : printServices) {
            System.out.println("\t" + printService.getName());
        }
    }
    public Folios getActivoSeleccionado() {
        return activoSeleccionado;
    }

    public void setActivoSeleccionado(Folios activoSeleccionado) {
        this.activoSeleccionado = activoSeleccionado;
    }

    public Folios getActivoEditar() {
        return activoEditar;
    }

    public void setActivoEditar(Folios activoEditar) {
        this.activoEditar = activoEditar;
    }

    public Folios getActivoAnexar() {
        return activoAnexar;
    }

    public void setActivoAnexar(Folios activoAnexar) {
        this.activoAnexar = activoAnexar;
    }

    public Folios getActivoBorrado() {
        return activoBorrado;
    }

    public void setActivoBorrado(Folios activoBorrado) {
        this.activoBorrado = activoBorrado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Productor> getProductores() 
    {
        return productores;
    }

    public void setProductores(List<Productor> productores) {
        this.productores = productores;
    }
    
    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Integer getId_productor() {
        return id_productor;
    }

    public void setId_productor(Integer id_productor) {
        this.id_productor = id_productor;
    }

    public Integer getPeso_bruto() {
        return peso_bruto;
    }

    public void setPeso_bruto(Integer peso_bruto) {
        this.peso_bruto = peso_bruto;
    }

    public Integer getPeso_tara() {
        return peso_tara;
    }

    public void setPeso_tara(Integer peso_tara) {
        this.peso_tara = peso_tara;
    }

    public Integer getPeso_neto() {
        return peso_neto;
    }

    public void setPeso_neto(Integer peso_neto) {
        this.peso_neto = peso_neto;
    }

    public Integer getNo_rejas() {
        return no_rejas;
    }

    public void setNo_rejas(Integer no_rejas) {
        this.no_rejas = no_rejas;
    }

    public Integer getTipo_rejas() {
        return tipo_rejas;
    }

    public void setTipo_rejas(Integer tipo_rejas) {
        this.tipo_rejas = tipo_rejas;
    }

    public Integer getTipo_limon() {
        return tipo_limon;
    }

    public void setTipo_limon(Integer tipo_limon) {
        this.tipo_limon = tipo_limon;
    }

    public Integer getId_agronomo() {
        return id_agronomo;
    }

    public void setId_agronomo(Integer id_agronomo) {
        this.id_agronomo = id_agronomo;
    }

    public Integer getDejo() {
        return dejo;
    }

    public void setDejo(Integer dejo) {
        this.dejo = dejo;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Integer getSegundas() {
        return segundas;
    }

    public void setSegundas(Integer segundas) {
        this.segundas = segundas;
    }

    public Integer getTerceras() {
        return terceras;
    }

    public void setTerceras(Integer terceras) {
        this.terceras = terceras;
    }

    public Integer getTorreon() {
        return torreon;
    }

    public void setTorreon(Integer torreon) {
        this.torreon = torreon;
    }

    public Integer getColeada() {
        return coleada;
    }

    public void setColeada(Integer coleada) {
        this.coleada = coleada;
    }

    public Integer getJapon() {
        return japon;
    }

    public void setJapon(Integer japon) {
        this.japon = japon;
    }

    public String getProductor() {
        return productor;
    }

    public void setProductor(String productor) {
        this.productor = productor;
    }

    public String getAgronomo() {
        return agronomo;
    }

    public void setAgronomo(String agronomo) {
        this.agronomo = agronomo;
    }

    public String getTipos_rejas() {
        return tipos_rejas;
    }

    public void setTipos_rejas(String tipos_rejas) {
        this.tipos_rejas = tipos_rejas;
    }

    public String getTipos_limones() {
        return tipos_limones;
    }

    public List<Tarima> getAgronomos() {
        
        return agronomos;
    }

    public void setAgronomos(List<Tarima> agronomos) {
        this.agronomos = agronomos;
    }

    public List<Tarima> getTamanos_limon() {
        
        return tamanos_limon;
    }

    public void setTamanos_limon(List<Tarima> tamanos_limon) {
        this.tamanos_limon = tamanos_limon;
    }

    public List<Tarima> getTamanos_reja() {
        return tamanos_reja;
    }

    public void setTamanos_reja(List<Tarima> tamanos_reja) {
        this.tamanos_reja = tamanos_reja;
    }

    public void setTipos_limones(String tipos_limones) {
        this.tipos_limones = tipos_limones;
    }
    
    public boolean isSkip() {
        return skip;
    }
 
    public void setSkip(boolean skip) {
        this.skip = skip;
    }

    public boolean isIsdejo() {
        return isdejo;
    }

    public void setIsdejo(boolean isdejo) {
        this.isdejo = isdejo;
    }

    public List<Folios> getRegistros_basculas() {
        FolioController controller=new FolioController();
        this.registros_basculas= controller.getAll(0);
        return this.registros_basculas;
    }
    public void setRegistros_basculas(List<Folios> registros_basculas) 
    {
        this.registros_basculas = registros_basculas;
    }

    public List<Folios> getRegistros_recepcion() 
    {
        FolioController controller=new FolioController();
        this.registros_recepcion= controller.getAll(1);
        return registros_recepcion;
    }

    public void setRegistros_recepcion(List<Folios> registros_recepcion) 
    {
        this.registros_recepcion = registros_recepcion;
    }

    public List<Folios> getRegistros_confirmacion() 
    {
        FolioController controller=new FolioController();
        this.registros_confirmacion= controller.getAll(2);
        return registros_confirmacion;
    }

    public List<Folios> getCorridas() 
    {
        FolioController controller=new FolioController();
        this.corridas= controller.getAll();
        return this.corridas;
    }

    public void setCorridas(List<Folios> corridas) {
        this.corridas = corridas;
    }

    public void setRegistros_confirmacion(List<Folios> registros_confirmacion) 
    {
        this.registros_confirmacion = registros_confirmacion;
    }

    public String getUrl() {
        url = (FacesContext.getCurrentInstance()).getExternalContext().getRequestParameterMap().get("url");
        url =url +"?faces-redirect=true";
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMensaje() 
    {
        mensaje = (FacesContext.getCurrentInstance()).getExternalContext().getRequestParameterMap().get("mmm");
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    
}
