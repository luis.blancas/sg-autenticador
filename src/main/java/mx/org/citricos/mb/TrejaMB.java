/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.org.citricos.mb; 
import java.io.IOException;
import java.io.Serializable;
import java.util.List; 
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct; 
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean; 
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext; 
import mx.org.citicos.controller.TrejaController; 
import mx.org.citricos.entity.Tarima; 

/**
 *
 * @author luis Adrian
 */ 
@ManagedBean (name = "trejasMB")
@RequestScoped
public class TrejaMB implements Serializable
{
    private SessionMB  sessionMB =new SessionMB();
    TrejaController controller=new TrejaController();
    private final FacesContext context = FacesContext.getCurrentInstance();
    private List<Tarima> tarimas; 
    private Tarima tarimaSeleccionado;
    private Tarima tarimaEditar;
    private Tarima tarimaAnexar;
    private Tarima tarimaBorrado; 
    
    private Integer id;
    private String  descripcion; 
    
    @PostConstruct
    public void init() {
        tarimaSeleccionado=new Tarima(); 
    }
    public List<Tarima> getTarimas() {
        
        this.tarimas= controller.getAll();
        return tarimas;
    } 
    public void seleccionEditar(Tarima editar )
    {
        this.tarimaEditar=new Tarima();
        this.tarimaEditar.setId(editar.getId()); 
        this.tarimaEditar.setNombre(editar.getNombre()); 
        System.out.println("seleccion Editar "+editar.toString());
    }
    public void seleccionBorrado(Tarima borrado )
    {
        this.tarimaBorrado=new Tarima();
        this.tarimaBorrado.setId(borrado.getId());
        this.tarimaBorrado.setNombre(borrado.getNombre());
        System.out.println("seleccionBorrado "+borrado.toString());
    } 
    public void editartarima(Integer id,Tarima o)
    {
        try
        { 
            System.out.println("Editar id    :"+id);
            System.out.println("Editar Nombre:"+o.getNombre());
            controller.updateRecordUsr(o.getNombre(),id,sessionMB.getUsuariofinal().getId());
            context.getExternalContext().redirect("ListarTarima.xhtml?faces-redirect=true");
        }
        catch (IOException ex) {
            Logger.getLogger(TrejaMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void anexartarima()
    {
        try
        { 
            System.out.println("Anexar Nombre:"+this.descripcion);
            controller.insertRecordUsr(descripcion,sessionMB.getUsuariofinal().getId());
            context.getExternalContext().redirect("ListarTarima.xhtml?faces-redirect=true");
        }
        catch (IOException ex) 
        {
            Logger.getLogger(TrejaMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void borrartarima(Tarima tarimaAborrar)
    {
        try
        { 
            controller.deleteRecord(tarimaAborrar.getId(),sessionMB.getUsuariofinal().getId());
            System.out.println("seleccion Borrar "+tarimaAborrar.toString()); 
            this.tarimas= controller.getAll();
            context.getExternalContext().redirect("ListarTarima.xhtml?faces-redirect=true");
            context.addMessage(null, new FacesMessage("Borrar", "Registro  borrado con exito :  "+tarimaAborrar.getNombre()));
        }
        catch (IOException ex) {
            Logger.getLogger(TrejaMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /*-------------------------------------------------------------------------------------*/

    public TrejaController getController() {
        return controller;
    }

    public void setController(TrejaController controller) {
        this.controller = controller;
    }

    public Tarima getTarimaSeleccionado() {
        return tarimaSeleccionado;
    }

    public void setTarimaSeleccionado(Tarima tarimaSeleccionado) {
        this.tarimaSeleccionado = tarimaSeleccionado;
    }

    public Tarima getTarimaEditar() {
        return tarimaEditar;
    }

    public void setTarimaEditar(Tarima tarimaEditar) {
        this.tarimaEditar = tarimaEditar;
    }

    public Tarima getTarimaAnexar() {
        return tarimaAnexar;
    }

    public void setTarimaAnexar(Tarima tarimaAnexar) {
        this.tarimaAnexar = tarimaAnexar;
    }

    public Tarima getTarimaBorrado() {
        return tarimaBorrado;
    }

    public void setTarimaBorrado(Tarima tarimaBorrado) {
        this.tarimaBorrado = tarimaBorrado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
}
